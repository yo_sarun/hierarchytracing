function show_swc(V, data, factor, axes_obj)
%SHOW_SWC Summary of this function goes here
%   Detailed explanation goes here
% keyboard;
edges = data(:,[1 7]);
data(:,3:4) = data(:,3:4)./factor;
edges = edges(all(edges>0,2),:);
G = sparse(edges(:,2), edges(:,1), 1, size(data,1), size(data,1));
[~, ~, pred] = bfs(G,1);
leafIdx = find(leafnode(data));
% keyboard;
axes(axes_obj), imshow(max(V,[],3),[]);
hold on
for i = leafIdx
   idx = path_from_pred(pred,i);
   plot(data(idx,3), data(idx,4), 'c', 'Linewidth', 2);
end
hold off
end

