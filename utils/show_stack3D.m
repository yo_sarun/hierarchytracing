function show_stack3D(V, cmap, nslice)
%SHOW_STACK3D show image stack in slices
%   Input
%   V       3D image stack
%   cmap    colour map
%   nslice  number of slices


width = 20;     % Width in inches
height = 20;    % Height in inches

pos = get(gcf, 'Position');
% set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperPosition', [0 0 30 30]);

%# coordinates
% V = V(1:2:end, 1:2:end, :);
[X,Y] = meshgrid(1:size(V,2), 1:size(V,1));
Z = ones(size(V,1),size(V,2));

%# plot each slice as a texture-mapped surface (stacked along the Z-dimension)
for k=1:ceil(size(V,3)/nslice):size(V,3)
    idx = k:min(k+ceil(size(V,3)/nslice)-1,size(V,3));
    surface('XData',X-0.5, 'YData',Y-0.5, 'ZData',Z.*k, ...
        'CData',mean(V(:,:,idx),3), 'CDataMapping','direct', ...
        'EdgeColor','none', 'FaceColor','texturemap')
end
colormap(cmap)
% view(3), 
view([-10 20]), 
box on, axis tight square
set(gca, 'YDir','reverse', 'ZLim',[0 size(V,3)+1])
box off
axis off
end