function [data, h] = fix_rootnode( data, V, s, savefile )
%FIX_ROOTNODE rearrange nodes such that rootnode is the first one
%	  Input
%	  data          n-by-7 matrix in swc format of neuron graph
%	  V				3D image stack or 
%     s             soma coordinate
%	  savefile		save filename
%
%     Output
%     data          new n-by-7 matrix with rootnode being soma
%     h             figure number

%% Find Spanning Tree
N = size(data,1);
E = [data(:,1) data(:,7)];
E = E(all(E>0,2),:);
DG = sparse(E(:,1),E(:,2), ones(size(E,1),1), N, N);
UG = DG + DG';
ST = kruskal_mst(UG);

%% Find starting point
ds = bsxfun(@minus, data(:,3:5), s);
dlen = sqrt(sum(ds.^2,2));
[~,root] = min(dlen);

%% Rearrange points not to violate 
visited = false(1,N);
new_data = cell(1,N);
cur = 1;
while any(~visited)
    if visited(root)
        root = find(~visit, 1);
    end
    % Rearrange points not to violate 
    [order,pred] = bfs_traverse(ST,root);
    data_tmp = data(order,:);
    data_tmp(:,7) = pred(order);
    newIdx = zeros(size(order));
    for i = 1:size(order,2)
        newIdx(order(i)) = i;
    end
    data_tmp(:,1) = newIdx(data_tmp(:,1));
    data_tmp(1,7) = -1;
    data_tmp(2:end,7) = newIdx(data_tmp(2:end,7));
    new_data{cur} = data_tmp;
    cur = cur+1;
    visited(order) = true;
end
new_data = new_data(~cellfun(@(x)isempty(x),new_data));
data = vertcat(new_data{:});

%% show image
if nargout == 2
    h = show_neuron(data, V);
    % hold on
    % plot(center(1),center(2),'+','LineWidth',2,'MarkerSize',10);
    % hold off
end

%%
if nargin == 4
    fileID = fopen([savefile '.swc'],'w');
    fprintf(fileID,'# Neurolucida to SWC conversion from L-Measure.\n');
    fprintf(fileID,'%.0f %d %.3f %.3f %.3f %.4f %.0f\n',data');
    fclose(fileID);
end
end

