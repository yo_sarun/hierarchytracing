function [attr1, score1, attr2, score2, attrs, result] = rank_feature( features, Y, type, t1, t2, varargin )
%RANK_FEATURE Summary of this function goes here
%   Detailed explanation goes here
%FEATURE_STAT plot histogram of neuron feature vector and convert neuron
%feature into matrix
%   Input
%   features    Neuron feature vector in n-by-1 struct array, each attribute
%               must be a scalar or 1-by-m array (or row vector)
%   Y           label of neuron feature vector in n-by-1 matrix
%   type        type name of neuron in 1D cell array of string
%   varargin    utility parameters
%
%   Output
%   X           Neuron feature in n-by-d matrix

% plot statistics
attrs = fieldnames(features);
result = zeros(1,length(attrs));
notnum = false(1,length(attrs));
use = true(1, length(attrs));
for n = 1:length(attrs)
    str = attrs{n};
    if numel(features(1).(str)) ~= 1
%         disp(['Non-numeric attribute: ' str])
        notnum(n) = true;
    end
    if any(strcmp(str, {'heightandwidth'}))
        use(n) = false;
    end
    data = cat(3,features.(str));
    data1 = data(:,:,Y==t1);
    data2 = data(:,:,Y==t2);
    r = zeros(size(data,1), size(data,2));
    for i = 1:size(data,1)
        for j = 1:size(data,2)
%             if min(data1(i,j,:)) > max(data2(i,j,:)) || ...
%                     min(data2(i,j,:)) > max(data1(i,j,:))
%                 continue
%             end
% keyboard;
            r(i,j) = max((range(data1(i,j,:)) + range(data2(i,j,:)) - ...
                    range(cat(3, data1(i,j,:), data2(i,j,:)))) / ...
                    (range(data1(i,j,:)) + range(data2(i,j,:))), 0);
            
        end
    end
    if any(r(:)<0)
        keyboard;
    end
    result(n) = mean(r(:));
end
[C1,I1] = min(result(~notnum&use));
idx1 = find(~notnum&use,I1); 
disp(['Type ' num2str(t1) ' and type ' num2str(t2) ...
        ' best feature is ' attrs{idx1(I1)} ...
        ' by ' num2str(C1*100) '%']);
[C2,I2] = min(result(notnum&use));
idx2 = find(notnum&use,I2); 
disp(['Type ' type{t1} ' and type ' type{t2} ...
        ' best non-numeric feature is ' attrs{idx2(I2)} ...
        ' by ' num2str(C2*100) '%']);
attr1 = attrs{idx1(I1)};
score1 = num2str(C1*100);
attr2 = attrs{idx2(I2)};
score2 = num2str(C2*100);
end

