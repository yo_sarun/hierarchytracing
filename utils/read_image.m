function V = read_image(imgdir, varargin)
%READ_IMAGE read image stack. RGB image will be converted to grayscale.
%Image slice is normalized between [0,255];
%   Input
%   imgdir - directory name containing image stack
%   issort - binary variable whether to sort image name
%
%   Output
%   V - 3D image stack
%

% process input
options = struct('sort',true,'scale',1);
optionNames = fieldnames(options);

for pair = reshape(varargin,2,[]) % pair is {propName;propValue}
   inpName = lower(pair{1}); % make case insensitive
   if any(strcmp(inpName,optionNames))
      options.(inpName) = pair{2};
   else
      error('%s is not a recognized parameter name',inpName)
   end
end

currentpath = cd;
cd(imgdir)
im_names = dir('*.tif');
if isempty(im_names)
    im_names = dir('*.TIF');
end
FNUM = length(im_names);

if options.sort
    filenames = regexp(sprintf('%030s\n',im_names.name),'\n','split');
    filenames = filenames(~cellfun(@isempty,filenames));
    [~, IX] = sort(filenames);
    if any(arrayfun(@(x)length(x.name), im_names) > 30)
        error('ERROR: file name is too long');
    end
else
    IX = 1:FNUM;
end

disp('Reading image...');
for fcount = 1:FNUM,
    disp([imgdir im_names(IX(fcount)).name]);
    X = imread(im_names(IX(fcount)).name,'tif');
    if ndims(X) == 3
        X = rgb2gray(X);
    end
    X = single(X);
    X = X-min(X(:)); X = X/max(X(:)); X = uint8(round(X*255.0));
    if ~exist('V', 'var')
        s = size(imresize(X,options.scale));
        V = zeros(s(1),s(2),FNUM);
    end
    V(:,:,fcount)=imresize(X,options.scale);
end
cd(currentpath)
end

