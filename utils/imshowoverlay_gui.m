function str = imshowoverlay_gui( V, seed, axes_obj, k )
%IMSHOWOVERLAY_GUI Summary of this function goes here
%   Detailed explanation goes here
dep = round(1+(size(V,3)-1)*k);
axes(axes_obj), imshow(V(:,:,dep), []);
[py, px] = find(seed(:,:,dep));
hold on;
plot(px,py,'b+')
hold off;
str = ['Slice: ' int2str(dep) '/' int2str(size(V,3))];
end

