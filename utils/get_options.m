function [ options ] = get_options( defaultoptions, options )
%GET_OPTIONS extract field from utility options
%   input
%	defaultoptions		default options
%	options				user-given options
%
%	Output
%	options				user-given options filled with default option
%

if(~exist('options','var')), 
    options = defaultoptions; 
else
    tags = fieldnames(defaultoptions);
    for i=1:length(tags)
         if(~isfield(options,tags{i})),  
             options.(tags{i})=defaultoptions.(tags{i}); 
         end
    end
    if(length(tags)~=length(fieldnames(options))), 
        warning('find_snake_topo:unknownoption','unknown options found');
    end
end

end

