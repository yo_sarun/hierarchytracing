function plot_feature(input)
%PLOT_FEATURE plot feature. Change plot depends on type of feature.

if isscalar(input)
    disp(input);
elseif isvector(input)
    compassplot3(input)
elseif ismatrix(input)
    color_roseplot(input)
else
    error('Unknown type of input');
end
end

