function imgdisp( handles )
%IMGDISP Summary of this function goes here
%   Detailed explanation goes here
val = get(handles.img_slider,'Value');
idx = get(handles.trace_popup, 'Value');
myCallback = handles.myCallback{idx};

if handles.select_soma == 1
    [str,h] = feval(@imshow_gui, handles.V, handles.axes1, val);
    set(h, 'ButtonDownFcn', @somaClickCallback);
    set(handles.slider_text, 'String', str);
    dep = size(handles.V,3);
    set(handles.img_slider, 'SliderStep', [1/dep , 10/dep]);
    drawnow;
    return
end
if ~isempty(myCallback)
    f = functions(myCallback{1});
    if strcmp(f.function, 'SNK_show') || strcmp(f.function, 'show_swc')
        feval(myCallback{1}, myCallback{2:end});
    else
        str = feval(myCallback{1}, myCallback{2:end}, val);
        set(handles.slider_text, 'String', str);
        dep = size(myCallback{2},3);
        set(handles.img_slider, 'SliderStep', [1/dep , 10/dep]);
    end
    drawnow;
end
end

