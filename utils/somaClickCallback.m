function somaClickCallback(objectHandle, eventData)
handles = guidata(objectHandle);
if handles.select_soma == 1
    handles.featupdate = 1;
    p = get(gca,'CurrentPoint' );
    val = get(handles.img_slider,'Value');
    coordinates = [p(1,1:2) round(1+(size(handles.V,3)-1)*val)];
    handles.coordinates = coordinates;
    set(handles.soma_coord_text, 'String', num2str(coordinates,'(%.0f,%.0f,%.0f)'));

    if (~isfield(handles, 'v') && ...
            isfield(handles, 'analyze_swc_pathname') && ...
            isfield(handles, 'analyze_swc_filename'))
        data = read_swc_file([handles.analyze_swc_pathname handles.analyze_swc_filename]);
        data = fix_rootnode(data, handles.V, handles.coordinates);
        handles.data = data;

        issoma = get(handles.soma_checkbox, 'Value');

        v = get_feature_vector(handles.data, handles.V, ...
                'soma', handles.coordinates(1:2), 'issoma', issoma);
        handles.v = v;
        handles.featupdate = 0;
        set(handles.feature_menu, 'String', fieldnames(handles.v));
    end
    
    handles.select_soma = 0;
    
    set(handles.status, 'String', 'Current Status: Idle');
    handles.isbusy = 0;
    guidata(gcbo, handles);
    imgdisp(handles)
end
