function [data, V, h] = mhd2swc(datadir, groupnum, datanum, savedir, display)
% MHD2SWC convert mdh file from VascuSynth dataset to swc file from 
% DIADEM dataset
% URL:http://www.mathworks.com/matlabcentral/fileexchange/27983-3d-slicer/content/imStacks/metaImageInfo.m
% URL:http://www.mathworks.com/matlabcentral/fileexchange/10922-matlabbgl
%   Input
%   datadir         directory containing dataset
%   groupnum        group number
%   datanum         data number
%   savedir         directory for saving swc file
%   display         binary variable (1 for display neuron, default 0)
%
%   Output
%   data            data in swc format
%   V               3D image stack
%   h               figure number

if nargin < 4
    savedir = '';
end
if nargin < 5
    display = 0;
end

% read image stack
directory = [datadir 'Group' num2str(groupnum) '/data' num2str(datanum)];
filename = ['/testVascuSynth',num2str(datanum),'_101_101_101_uchar.mhd'];
info = metaImageInfo([directory filename]);
V = metaImageRead(info);

% read in tree structure information
mat_file = ['/treeStructure_',num2str(datanum),'.mat'];
load([directory mat_file])

% change into swc format
type_key = {'root', 'bif', 'terminal'};
type_value = [1, 1, 2];
type_map = containers.Map(type_key, type_value);

n = length(node);
type = arrayfun(@(x)type_map(x.type), node)';
coord = vertcat(node.coord);
rootnode = arrayfun(@(x)isempty(x.parent), node);
parent = zeros(n,1);
parent(rootnode) = -1;
parent(~rootnode) = [node.parent];
radius = zeros(n,1);
radius(rootnode) = 0;
radius(~rootnode) = [node.parent_radius];

data = [(1:n)', type, coord, radius, parent];

% find graph
N = size(data,1);
E = [data(:,1) data(:,7)];
E = E(all(E>0,2),:);
DG = sparse(E(:,1),E(:,2), ones(size(E,1),1), N, N);
UG = DG + DG';

% Rearrange points not to violate swc contraints
[order,pred] = bfs_traverse(UG,find(data(:,7)==-1,1));
data = data(order,:);
data(:,7) = pred(order);
newIdx = zeros(size(order));
for i = 1:size(order,2)
    newIdx(order(i)) = i;
end
data(:,1) = newIdx(data(:,1));
data(1,7) = -1;
data(2:end,7) = newIdx(data(2:end,7));

% write into file
swcfile = ['vsynthG',num2str(groupnum),'D',num2str(datanum),'.swc'];
fid = fopen([savedir swcfile], 'wt');
fprintf(fid, '%d %d %.3f %.3f %.3f %.4f %d\n', data');
fclose(fid);

if display
    % locate root, bifurcation and terminal indices
    root_index = arrayfun(@(x)strcmp(x.type,'root'), node);
    bif_index = arrayfun(@(x)strcmp(x.type,'bif'), node);
%     term_inde x = find(arrayfun(@(x)strcmp(x.type,'terminal'), node));

    % visualize volume image
    h = figure(1); clf, isosurface(V, 20);
    axis([0 101 0 101 0 101])
    alpha(.4)

    % visualize bifurcation locations
    root_coord = coord(root_index,:);
    bif_coord = coord(bif_index,:);
    hold on
    scatter3(root_coord(:,1), ...
            root_coord(:,2), ...
            root_coord(:,3), ...
            80, 'b', 'filled')
    scatter3(bif_coord(:,1), ...
            bif_coord(:,2), ...
            bif_coord(:,3), ...
            80, 'r', 'filled')
    hold off
end
end