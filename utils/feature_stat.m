function X = feature_stat(features, Y, type, varargin)
%FEATURE_STAT plot histogram of neuron feature vector and convert neuron
%feature into matrix
%   Input
%   features    Neuron feature vector in n-by-1 struct array, each attribute
%               must be a scalar or 1-by-m array (or row vector)
%   Y           label of neuron feature vector in n-by-1 matrix
%   type        type name of neuron in 1D cell array of string
%   varargin    utility parameters
%
%   Output
%   X           Neuron feature in n-by-d matrix

if size(features,1) ~= size(Y,1)
    error('ERROR: size of feature vector and label mismatched')
end

labels = unique(Y);         % get label names

% process input
options = struct('bin',10);
optionNames = fieldnames(options);

for pair = reshape(varargin,2,[]) % pair is {propName;propValue}
   inpName = lower(pair{1}); % make case insensitive
   if any(strcmp(inpName,optionNames))
      options.(inpName) = pair{2};
   else
      error('%s is not a recognized parameter name',inpName)
   end
end

% convert 1D array struct to matrix and get attribute names
n = length(features);
val = struct2cell(features);
val = cellfun(@(x)reshape(x,1,numel(x)),val,'UniformOutput',false);
val = [val{:}];
d = numel(val)/n;
X = reshape(val,d,n)';

% plot statistics
attrs = fieldnames(features);
for attr = attrs'
    str = attr{1};
    if numel(features(1).(str)) ~= 1
        disp(['Cannot display ' str])
        continue
    end
    h = figure;
    x = [features.(attr{1})];
    bins = linspace(min(x)-1,max(x)+1,options.bin); % pick my own bin locations

    y = cell(length(labels),1);
    for label = labels'
        y{label} = hist(x(Y==label), bins);
    end

%     subplot(2,2,1); 
    bar(bins, cell2mat(y)', 'stacked');
    legend(type)
	title(str);
    
    
%     subplot(2,2,2); boxplotfun( x, Y, type, str );
%     subplot(2,2,3); errorplot(x, Y, type, str);
%     subplot(2,2,4); scatterboxplot(x, Y, type, str)
    
    set(h, 'Position', [300, 300, 1024, 720]);
    set(gcf, 'PaperUnits', 'centimeters');
    set(gcf, 'PaperPosition', [0 0 45 30]);
end

end

