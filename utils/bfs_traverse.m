function [order, pred] = bfs_traverse( G, n )
%BFS_TRAVERSE traverse the graph using breadth-first-search
%   Input
%	G		graph in sparse matrix
%	n		start index
%
%	Output
%	order	order of index that were visited
%	pred	predecessor of node
%
[~,dt,pred] = bfs(G,n);
[b,ix] = sort(dt);
order = ix(b>0)';
end

