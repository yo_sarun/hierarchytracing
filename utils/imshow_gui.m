function [str, h] = imshow_gui( V, axes_obj, k )
%IMSHOW_GUI Summary of this function goes here
%   Detailed explanation goes here
dep = round(1+(size(V,3)-1)*k);
axes(axes_obj), h = imshow(V(:,:,dep), []);
str = ['Slice: ' int2str(dep) '/' int2str(size(V,3))];
end

