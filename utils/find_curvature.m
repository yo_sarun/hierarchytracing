function [ len, curve ] = find_curvature( points, display)
%FIND_CURVATURE find curvature of points
%
%     Inputs
%     points      list of points
%     display     boolean to display value or not
%     
%     Outputs
%     len         length of first derivative (len between adjacent points)
%     curve       curvature
%     
if nargin < 2
    display = 0;
end
points = points';
s = size(points);
if s(1) == 2
    points = [points; ones(1,s(2))];
end
c = cscvn(points);

if display > 0
    fnplt(c, 'g',1.5);
    legend({'Control Polygon' 'Quadratic Spline Curve' ...
            'Cubic Spline Curve' 'Interpolating Spline Curve'}, ...
            'location','SE');
    hold on;
    plot3(points(1,:), points(2,:), points(3,:));
    hold off;
end
interv = fnbrk(c,'interval');
t = linspace(interv(1), interv(2),s(2)*2);
dc = fnder(c); dct = fnval(dc,t); ddct = fnval(fnder(dc),t);
kappa = sqrt(sum(cross(dct,ddct).^2)./(sum(dct.^2)).^3);
curve = sum(kappa)/(s(2)*2);

ds = points(:,2:end) - points(:,1:end-1);
len = sqrt(sum(ds.^2,1));
end

