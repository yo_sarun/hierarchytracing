function [ is_inside ] = is_inside_boundary( snks1, snks2, blksize, gap )
%IS_INSIDE_BOUNDARY check whether snks1 and snks2 is within 20 pixels
% 
%     Inputs
%     snks1       snakes 1
%     snks2       snakes 2
%     blksize     block size
%     options     utility options
%     
%     Outputs
%     is_inside   boolean whether snks1 and snks2 are within 20 pixels
%

% if~ismember(nargin,3:4)
%     error('Invalid inputs to is_inside_boundary!')    
% end

%% Get options parameters
if nargin < 4
    gap = 20;
end
% defaultoptions = struct('Gap', 20);
% 
% if ~exist('options','var')
%     options = defaultoptions;
% else
%     options = get_options(defaultoptions, options);
% end
% 
% gap = options.Gap;

%%
is_inside = false;
if isempty(snks1) || isempty(snks2)
    return
end
% points1 = round(vertcat(snks1.vert));
% points2 = round(vertcat(snks2.vert));
% if isempty(points1) || isempty(points2)
%     return
% end
% 
% p1min = max([min(points1)-gap;ones(1,3)]);
% p1max = min([max(points1)+gap;blksize([2 1 3])]);
% p2min = max([min(points2);ones(1,3)]);
% p2max = min([max(points2);blksize([2 1 3])]);
% 
% if all((p1min >= p2min & p1min <= p2max) | (p1max >= p2min & p1max <= p2max) | ...
%         (p2min >= p1min & p2min <= p1max) | (p2max >= p1min & p2max <= p1max))
%     is_inside = true;
% end
if isempty(snks1.vert) || isempty(vertcat(snks2.vert))
    return
end

ds = bsxfun(@minus, vertcat(snks2.vert), snks1.vert(1,:));
if any(sqrt(sum(ds.^2,2)) < gap)
    is_inside = true;
    return
end

ds = bsxfun(@minus, vertcat(snks2.vert), snks1.vert(end,:));
if any(sqrt(sum(ds.^2,2)) < gap)
    is_inside = true;
    return
end

end
