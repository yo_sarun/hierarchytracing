% Test tracing snake at block level

clear all
close all
clc

addpath('./AMT')
addpath('./frangi/')
addpath('./utils')
addpath('./DRF')
addpath('./snake')
addpath('./block')
addpath('./topology')
addpath('./ba_interp3')
addpath(genpath('./toolbox_fast_marching'))
addpath(genpath('./UGM'))
addpath(genpath('./matlab_bgl'))

% load './Yo/demoVars.mat' 'smooth'
% load 'snkDRF_predict_new.mat'
% load 'blkDRF_predict_new11.mat'
% load './Yo/nCF_1.mat' smooth V
% load './resultCF/snkDRF_nCF_1.mat'
% load 'blkDRF_n72413_h0_11.mat'
load '../tracefiles/preprocessimage/n72413_h0.mat'
load '../tracefiles/snake/snkDRF_n72413_h0.mat'

disp('======================================')
disp('Open Snake Curve with DRF at Block level')

% warning('on','all')
warning('off','all')
set(0,'DefaultFigureWindowStyle','docked')

%% Set BLK_propagate parameters
% r = 5;
% c = 7;
% fcount = 5;
% r = 7;
% c = 1;
% fcount = 1;
% r = 1;
% c = 7;
% fcount = 1;
% r = 21;
% c = 6;
% fcount = 6;
% r = 18;
% c = 15;
% fcount = 5;
% r = 3;
% c = 15;
% fcount = 1;
% r = 12;
% c = 9;
% fcount = 6;
% r = 6;
% c = 3;
% fcount = 1;
% r = 0;
% c = 3;
% fcount = 1;
r = 21;
c = 6;
fcount = 10;
% row = (r*32)+1;
% col = (c*32)+1;
% U = smooth(row:row+95,col:col+95,:);

nRows = 3;
nCols = 3;
nDeps = 1;

%% pad image and find GVF energy of superblock
row = (r*blksize(1))+1;
col = (c*blksize(2))+1;
rowt = min(row+blksize(1)*nRows-1, size(V,1));
colt = min(col+blksize(2)*nCols-1, size(V,2));

U = smooth(row:rowt, col:colt, :);

mu = .2;
GVF_ITER = 10;
h = fspecial('gaussian',[5 5],5);
f = imfilter(double(U),h);
Fexts = AM_GVF(f, mu, GVF_ITER, 1);
Fext = cell(1,3);
for i = 1:3
    Fext{i} = double(Fexts(:,:,:,i));
end

%% get snake edge Struct
disp('Generate Edge Struct for Snake Level ...')
tic;
blockStruct = BLK_getStruct(nRows, nCols, nDeps);
toc;

%%
options.FrangiScaleRange = [1,8];
options.Fcount = fcount;
% options.Iter_thr = 10;
tic;
[bsnk, btube, S, C] = ...
        BLK_propagate_topo( U, allsnk, blockStruct...
        , blksize, tubes, Fext ...
        , Ss, Cs , r ,c, level, options);
toc;
