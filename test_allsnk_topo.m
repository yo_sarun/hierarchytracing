% Trace snake in voxel level
function [ptitle, myCallback, allsnk, tubes, blksize, Ss, Cs, level, V] ...
        = test_allsnk_topo(handles, options)
loadname = [handles.analyze_pathname handles.analyze_filename];
imgname = handles.analyze_filename;
A = load(loadname, 'V', 'blksize', 'cellsize');
B = load('paramsDRF2.mat', 'result2');

V = A.V;
blksize = A.blksize;
cellsize = A.cellsize;
result2 = B.result2;
clearvars A B



addpath('./AMT')
addpath('./frangi')
addpath('./utils')
addpath('./DRF')
addpath('./snake')
addpath('./block')
addpath('./topology')
addpath('./ba_interp3')
addpath('./swc');
addpath(genpath('./toolbox_fast_marching'))
addpath(genpath('./UGM'))
addpath(genpath('./matlab_bgl'))

if ~handles.script
    handles.jTextArea.append(['Trace Open Snake at Pixel level' char(10)]);
    handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength);
else
    disp('Trace Open Snake at Pixel level')
end

%% initialise
level = 1;
allsnk = cell(cellsize);
tubes = cell(cellsize);
Ss = zeros(cellsize);Cs = cell(cellsize);

%% compute GVF
C = regexp(imgname,'\.','split');
imgname = C{1};
GVFfile = [handles.imggvf imgname '_GVF.mat'];
if ~(exist(GVFfile, 'file') == 2)
    mu = .2;
    GVF_ITER = 10;
    h = fspecial('gaussian',[5 5],5);
    matObj = matfile(loadname);
    f = imfilter(double(matObj.smooth),h);
    Fexts = AM_GVF(f, mu, GVF_ITER, 1);
    mkdir(handles.imggvf);
    save(GVFfile, 'Fexts', '-v7.3');
end

%% Deform snakes in each block
edgeStruct_default = BLK_getEdgeStruct([32 32 size(V,3)]);
blksizeN = blksize(1);
blksizeM = blksize(2);
num = numel(allsnk);
parfor a = 1:num
    [i,j] = ind2sub(cellsize,a);
    row = ((i-1)*32)+1;
    col = ((j-1)*32)+1;
    rowt = min(row+blksizeN-1, size(V,1));
    colt = min(col+blksizeM-1, size(V,2));

    matObj = matfile(loadname);
    U = matObj.smooth(row:rowt,col:colt,:);
    seed = matObj.seeds(row:rowt,col:colt,:);
    
    if all(size(U) == [32 32 size(U,3)])
        edgeStruct = edgeStruct_default;
    else
        edgeStruct = BLK_getEdgeStruct(size(U));
    end
    
    % get GVF
    GVFobj = matfile(GVFfile);
    Fext = cell(1,3);
    for n = 1:3
        Fext{n} = double(GVFobj.Fexts(row:rowt,col:colt,:,n));
    end
    
    opt = options;
    if ~isfield(opt, 'LengthThreshold')
        opt.LengthThreshold = size(U,1)/10;
    end
    % find snakes in the block
    [allsnk{a}, tubes{a}, ~, Ss(a), Cs{a}] = ...
            find_snake_topo(U, seed, Fext, edgeStruct, result2, opt);
end

%% Save result
snkdir = handles.snkdir;
savename = [snkdir 'snkDRF_' imgname 's.mat'];
if mkdir(snkdir)
    save(savename, 'allsnk', 'tubes', 'blksize', 'Ss', 'Cs', 'level', '-v7.3');
    if ~handles.script
        handles.jTextArea.append(['Snake file saved to ' savename char(10)]);
        handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength);
    else
        disp(['Snake file saved to ' savename])
    end
else
    error('ERROR: cannot save snake tracing result');
end

%% show overlaid image
if handles.script
    h = figure;
    handles.axes1 = cla;
end
SNK_show(allsnk, blksize, 'jet', 1, V, 'pixel level', handles.axes1);
ptitle = {'pixel level'};
myCallback = {@SNK_show, allsnk, blksize, 'jet', 1, V, 'pixel level', handles.axes1};
if handles.script
    saveas(h, [handles.savedir imgname '_pixel.png'], 'png');
end
end
