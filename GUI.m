function varargout = GUI(varargin)
% GUI MATLAB code for GUI.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI

% Last Modified by GUIDE v2.5 18-Sep-2014 21:27:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   [] , ...
                   'gui_WindowScrollWheelFcn', @figure1_WindowScrollWheelFcn);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI is made visible.
function GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI (see VARARGIN)
% Choose default command line output for GUI
addpath('Emmanuel')
addpath('Eve')
addpath('Samantha')
addpath('Yo')
addpath('export_fig')
addpath('uicomponent')
addpath(genpath('lib'))
addpath(genpath('utils'))
addpath(genpath('./matlab_bgl'))

handles.imgdir = './preprocess_output/';
handles.imggvf = './gvf/';
handles.snkdir = './snk/';
handles.swcdir = './myswc/';
handles.featdir = './feat/';
handles.output = hObject;

set(0,'DefaultFigureWindowStyle','normal')
warning('off','all')

handles.script = false;
handles.isbusy = 0;
handles.pretitle = {};
handles.tracetitle = {};
handles.preCallback = {};
handles.traceCallback = {};
handles.myCallback = {[]};
handles.select_soma = 0;
handles.featupdate = 0;
set(handles.axes1, 'Visible', 'off');

handles.jTextArea = javax.swing.JTextArea;
handles.jTextArea.setEditable(0);
jContainer = javax.swing.JScrollPane(handles.jTextArea);
hContainer = uicomponent(jContainer,'pos',[20,10,330,140]);
handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength); % scroll to end

% Update handles structure
guidata(hObject, handles);
% UIWAIT makes GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function select_file_btn_Callback(hObject, eventdata, handles)
if handles.isbusy
    return
end
[filename, pathname] = uigetfiles;%('.', 'Pick first image of the image stack');
if filename == 0
    return
end
handles.filename = filename;
handles.pathname = pathname;
set(handles.file_text, 'String', filename);
% Update handles structure
guidata(hObject, handles);


function preprocess_btn_Callback(hObject, eventdata, handles)
if handles.isbusy
    return
end
if ~isfield(handles, 'filename')
    errordlg('No image is selected');
    return
end

set(handles.status, 'String', 'Current Status: Busy....');
handles.isbusy = 1;
guidata(hObject, handles);

set(handles.axes1, 'Visible', 'on');
filename = regexp(handles.filename,'\.','split');
handles.file_extension = char(filename(end));

ptype = get(handles.preprocess_type, 'Value');
try
    switch ptype
        case 1
            [imgdir, savefile, ptitles, callback] = preprocess(handles);
        case 2
            [imgdir, savefile, ptitles, callback] = preprocessCCmotor2(handles);
        case 3
            [imgdir, savefile, ptitles, callback] = preprocessDIADEM_CF(handles);
        case 4
            [imgdir, savefile, ptitles, callback] = preprocessDIADEM_NL(handles);
        case 5
            [imgdir, savefile, ptitles, callback] = preprocessDIADEM_OP(handles);
        case 6
            [imgdir, savefile, ptitles, callback] = preprocessVascu2(handles);
    end
    handles.pretitle = ptitles;
    set(handles.trace_popup, 'String', [handles.pretitle; handles.tracetitle]);
    set(handles.trace_popup,'Value',length(handles.pretitle));
    handles.preCallback = callback;
    handles.myCallback = [handles.preCallback; handles.traceCallback ];

    handles.analyze_pathname = imgdir;
    handles.analyze_filename = savefile;
    set(handles.trace_file_text, 'String', ['Current File: ' savefile]);
    imgdisp(handles);
catch
    handles.jTextArea.append(['Error occur' char(10)]);
end
set(handles.feature_menu, 'String', {'-'});
try
    handles = rmfield(handles,'v');
catch
end
% Update handles structure
set(handles.status, 'String', 'Current Status: Idle');
handles.isbusy = 0;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function preprocess_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to preprocess_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function trace_file_btn_Callback(hObject, eventdata, handles)
if handles.isbusy
    return
end
[filename, pathname] = uigetfile([handles.imgdir '*.mat'], 'Pick .mat file to trace');
if filename == 0
    return
end
handles.analyze_filename = filename;
handles.analyze_pathname = pathname;
set(handles.trace_file_text, 'String', ['Current File: ' filename]);
set(handles.feature_menu, 'String', {'-'});
try
    handles = rmfield(handles,'v');
catch
end
load([handles.analyze_pathname handles.analyze_filename], 'V');
handles.pretitle = {'Original'};
set(handles.trace_popup, 'String', [handles.pretitle; handles.tracetitle]);
set(handles.trace_popup,'Value',length(handles.pretitle));
handles.preCallback = {{@imshow_gui, V, handles.axes1}};
handles.myCallback = [handles.preCallback; handles.traceCallback ];
imgdisp(handles);
% Update handles structure
guidata(hObject, handles);


function trace_btn_Callback(hObject, eventdata, handles)
if handles.isbusy
    return
end
if ~(isfield(handles, 'analyze_pathname') && isfield(handles, 'analyze_filename'))
    errordlg('Preprocess image first');
    return
end

set(handles.status, 'String', 'Current Status: Busy....');
handles.isbusy = 1;
guidata(hObject, handles);
pause(1/8);

try
    % Open parallel
%     if ~matlabpool('size')
%         try
%             matlabpool open
%         catch
%             print 'a'
%             warning('NO PARALLEL: matlabpool is not available');
%         end
%     end
    if isfield(handles, 'options')
        opt = handles.options;
    else
        opt = [];
    end
    set(handles.axes1, 'Visible', 'on');
    [ptitles1, myCallback1, allsnk, tubes, blksize, Ss, Cs, level, V] = ...
            test_allsnk_topo(handles, opt);

    [swcdir, swcfile, ptitles2, myCallback2] = test_propagate_all_topo(...
            handles, V, allsnk, tubes, blksize, Ss, Cs, level, opt);
    % close parallel
%     if matlabpool('size')
%         try
%             matlabpool close
%         catch
%         end
%     end
    handles.analyze_swc_filename = swcfile;
    handles.analyze_swc_pathname = swcdir;
    set(handles.analyze_swc_file_text, 'String', ['Current File: ' swcfile]);
    handles.tracetitle = [ptitles1; ptitles2];
    set(handles.trace_popup, 'String', [handles.pretitle; handles.tracetitle]);
    set(handles.trace_popup,'Value',length([handles.pretitle; handles.tracetitle]));
    handles.traceCallback = [{myCallback1}; myCallback2];
    handles.myCallback = [handles.preCallback; handles.traceCallback];
catch e
    if strcmp(e.identifier, 'MATLAB:nonStrucReference')
        handles.jTextArea.append(['Error: No snake in image' char(10)]);
    else
        handles.jTextArea.append(['Error occur' char(10)]);
    end
end
set(handles.feature_menu, 'String', {'-'});
try
    handles = rmfield(handles,'v');
catch
end
set(handles.status, 'String', 'Current Status: Idle');
handles.isbusy = 0;
guidata(hObject, handles);


function snk_file_btn_Callback(hObject, eventdata, handles)
[filename, pathname] = uigetfile('.', 'Pick snake file to trace');
handles.snk_filename = filename;
handles.snk_pathname = pathname;
set(handles.snk_file_text, 'String', ['Current File: ' filename]);
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function trace_output_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trace_output (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function file_extension_CreateFcn(hObject, eventdata, handles)
% hObject    handle to file_extension (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function output_box_CreateFcn(hObject, eventdata, handles)
% hObject    handle to output_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in trace_popup.
function trace_popup_Callback(hObject, eventdata, handles)
% hObject    handle to trace_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns trace_popup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from trace_popup
if handles.isbusy
    return
end
imgdisp(handles);

% --- Executes during object creation, after setting all properties.
function trace_popup_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trace_popup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in select_soma_btn.
function select_soma_btn_Callback(hObject, eventdata, handles)
% hObject    handle to select_soma_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.isbusy
    return
end
if ~(isfield(handles, 'analyze_pathname') && ...
        isfield(handles, 'analyze_filename'))
    errordlg('Preprocess image or swc file not found');
    return
end

set(handles.status, 'String', 'Current Status: Busy....');
handles.isbusy = 1;
handles.select_soma = 1;
set(handles.axes1, 'Visible', 'on');
load([handles.analyze_pathname handles.analyze_filename], 'V');
handles.V = V;
imgdisp(handles)
set(handles.feature_menu, 'String', {'-'});
try
    handles = rmfield(handles,'v');
catch
end
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function feature_menu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to feature_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in analyze_btn.
function analyze_btn_Callback(hObject, eventdata, handles)
% hObject    handle to analyze_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.isbusy
    return
end
if ~(isfield(handles, 'analyze_pathname') && ...
        isfield(handles, 'analyze_filename') && ...
        isfield(handles, 'analyze_swc_pathname') && ...
        isfield(handles, 'analyze_swc_filename') && ...
        isfield(handles, 'coordinates'))
    errordlg('Need preprocess image, trace result, and soma first');
    return
end

load 'cmap.mat'

set(handles.status, 'String', 'Current Status: Busy...');
handles.isbusy = 1;

if ~isfield(handles, 'v') || handles.featupdate == 1
    data = read_swc_file([handles.analyze_swc_pathname handles.analyze_swc_filename]);
    data = fix_rootnode(data, handles.V, handles.coordinates);
    handles.data = data;

    issoma = get(handles.soma_checkbox, 'Value');
    
    v = get_feature_vector(handles.data, handles.V, ...
            'soma', handles.coordinates(1:2), 'issoma', issoma);
    handles.v = v;

    set(handles.feature_menu, 'String', fieldnames(handles.v));
    handles.featupdate = 0;
end

feature_list = get(handles.feature_menu, 'String');
feature_value = feature_list{get(handles.feature_menu, 'Value')};
feature = handles.v.(feature_value);

if isscalar(feature)
    handles.jTextArea.append([feature_value ': ' num2str(feature) char(10)]);
    handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength);
elseif isvector(feature)
    compassplot3(feature)
elseif ismatrix(feature)
    color_roseplot(feature)
else
    error('Unknown type of input');
end
set(handles.status, 'String', 'Current Status: Idle');
handles.isbusy = 0;
guidata(hObject, handles);

% --- Executes on button press in get_swc_btn.
function get_swc_btn_Callback(hObject, eventdata, handles)
% hObject    handle to get_swc_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.isbusy
    return
end
if ~(isfield(handles, 'analyze_pathname') && isfield(handles, 'analyze_filename'))
    errordlg('Preprocess image first');
    return
end
[filename, pathname] = uigetfile([handles.swcdir '*.swc'], 'Pick SWC file to analyze');
if filename == 0
    return
end
handles.analyze_swc_filename = filename;
handles.analyze_swc_pathname = pathname;
set(handles.analyze_swc_file_text, 'String', ['Current File: ' filename]);
set(handles.feature_menu, 'String', {'-'});
try
    handles = rmfield(handles,'v');
catch
end

load([handles.analyze_pathname handles.analyze_filename], 'V', 'sample_factor');
data = read_swc_file([pathname filename]);
handles.tracetitle = {'trace'};
set(handles.trace_popup, 'String', [handles.pretitle; handles.tracetitle]);
set(handles.trace_popup,'Value',length([handles.pretitle; handles.tracetitle]));
handles.traceCallback = {{@show_swc, V, data, sample_factor, handles.axes1}};
handles.myCallback = [handles.preCallback; handles.traceCallback];
imgdisp(handles);
% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in param_btn.
function param_btn_Callback(hObject, eventdata, handles)
% hObject    handle to param_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.isbusy
    return
end
prompt = {'Frangi Scale Range:','Alpha:','Beta:','Tau:','Converge Iteration:','Max Iteration:','Snake Length Threshold:'};
dlg_title = 'Parameters';
num_lines = 1;
def = {'[1 8]','1','1','.5','7','100',''};
param_input = inputdlg(prompt,dlg_title,num_lines,def);
handles.options = [];
if ~isempty(param_input)
    f = {'FrangiScaleRange', 'Alpha', 'Beta', 'Tau', ...
            'Iter_thr', 'Snake_iter', 'LengthThreshold'};
    for i = 1:length(f)
        if ~isempty(param_input{i})
            handles.options.(f{i}) = str2num(param_input{i});
        end
    end
end
guidata(hObject, handles);


% --- Executes on selection change in preprocess_type.
function preprocess_type_Callback(hObject, eventdata, handles)
% hObject    handle to preprocess_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns preprocess_type contents as cell array
%        contents{get(hObject,'Value')} returns selected item from preprocess_type



% --- Executes on slider movement.
function img_slider_Callback(hObject, eventdata, handles)
% hObject    handle to img_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
idx = get(handles.trace_popup, 'Value');
myCallback = handles.myCallback{idx};
if handles.select_soma == 0 && ~isempty(myCallback)
    f = functions(myCallback{1});
    if strcmp(f.function, 'SNK_show') || strcmp(f.function, 'show_swc')
        return
    end
end
imgdisp(handles);

 
% --- Executes during object creation, after setting all properties.
function img_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to img_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function output_box_Callback(hObject, eventdata, handles)
% hObject    handle to output_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of output_box as text
%        str2double(get(hObject,'String')) returns contents of output_box as a double


% --- Executes on scroll wheel click while the figure is in focus.
function figure1_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)
count = eventdata.VerticalScrollCount;
try
    style = get(gco,'Style');
    if strcmp(style, 'slider')
        data = guidata(hObject);
        step = get(data.img_slider, 'SliderStep');
        val = get(data.img_slider, 'Value') + count*step(1);
        if val < 0
            val = 0;
        end
        if val > 1
            val = 1;
        end
        set(data.img_slider, 'Value', val);
        
        idx = get(handles.trace_popup, 'Value');
        myCallback = handles.myCallback{idx};
        if handles.select_soma == 0 && ~isempty(myCallback)
            f = functions(myCallback{1});
            if strcmp(f.function, 'SNK_show') || strcmp(f.function, 'show_swc')
                return
            end
        end
        imgdisp(data);
    end
    guidata(hObject, handles);
catch
end


% --- Executes on selection change in feature_menu.
function feature_menu_Callback(hObject, eventdata, handles)
% hObject    handle to feature_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns feature_menu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from feature_menu


% --- Executes on button press in save_feat_btn.
function save_feat_btn_Callback(hObject, eventdata, handles)
% hObject    handle to save_feat_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~(isfield(handles, 'v') && isfield(handles, 'analyze_filename'))
    errordlg('Need to select soma first');
    return
end
imgname = handles.analyze_filename;
C = regexp(imgname,'\.','split');
imgname = C{1};
mkdir(handles.featdir);
[filename, pathname] = uiputfile([handles.featdir imgname '_feat.mat'], 'Save feature to MAT file');
if filename == 0
    return
end
feature = handles.v;
save([pathname filename], 'feature');


% --- Executes during object creation, after setting all properties.
function analyze_btn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to analyze_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function get_swc_btn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to get_swc_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in soma_checkbox.
function soma_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to soma_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of soma_checkbox
handles.featupdate = 1;
guidata(gcbo, handles);


% --- Executes on button press in save_img_btn.
function save_img_btn_Callback(hObject, eventdata, handles)
% hObject    handle to save_img_btn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
mkdir(handles.featdir);
[filename, pathname] = uiputfile([handles.featdir 'screenshot.png'], 'Save Image to PNG file');
if filename == 0
    return
end
figure_handle = isolate_axes(handles.axes1);
export_fig(figure_handle, [pathname filename]);