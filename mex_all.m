addpath('./snake')
addpath('./topology')
addpath(genpath('./UGM'))

TP_mex
SNK_mex
cd('./UGM')
mexAll
cd('../')
cd('./toolbox_fast_marching')
compile_mex
cd('../')
cd('./DRF')
make
cd('../')
cd('./frangi')
files = dir('*.c');
for i = 1:length(files)
    mex(files(i).name)
end
cd('../')
cd('./ba_interp3')
mex -O ba_interp3.cpp
cd('../')
cd('./AMT')
files = dir('*.c');
for i = 1:length(files)
    mex(files(i).name)
end
cd('../')
disp('MEX Done')