% Preprocessing step for image region of drosophila dataset

clearvars -except datapath datapaths filename filenames imgdir imgname snkdir swcdir sample_factor
close all
clc

addpath('./AMT');
addpath('./CSF');
addpath('./utils');
addpath('./PreprocessLib');
addpath('./oof3response');
addpath(genpath('./CurveLab-2.1.3'));

% set(0,'DefaultFigureWindowStyle','normal')
set(0,'DefaultFigureWindowStyle','docked')

%% Read Image
disp('Reading images ...')
currentpath = cd;
% [filename, datapath] = uigetfiles;
% filename = 'img72413_h0.tif';
% datapath = '/Users/Yo/Desktop/CRFSNK_predict/';

cd(datapath)
InfoImage=imfinfo(filename);
FNUM = length(InfoImage);
V=zeros(InfoImage(1).Height,InfoImage(1).Width,FNUM,'uint8');
for i=1:FNUM
   V(:,:,i)=imread(filename,'Index',i,'Info',InfoImage);
end

C = regexp(filename,'\.','split');
C = C(~cellfun(@(x)isempty(x), C));
savefile = ['n', C{1}, '.mat'];
cd(currentpath)
figure(1)
imshow3D(V)

%% parameters
sigCurv=20;

S_csf = 2;

BG_median_thr = 30;

cellsize = [ceil(size(V,1)/32) ceil(size(V,2)/32)];
blksize = [32 32 FNUM];

thr_val = 50;
region_size = 200;

r_oof = 6;
opts.responsetype=5;
OOF_thr = 5;


%% Curvelet Preprocessing
curvelets=zeros(size(V));
for i=1:FNUM  
    disp(['Curvelets #',num2str(i),' started']);
    [curvelets(:,:,i)]=NfdctDemo(V(:,:,i),sigCurv);
end
figure(2)
imshow3D(curvelets)

%% LoG
V_csf = CSF_cos(uint8(curvelets),S_csf);
figure(3)
imshow3D(V_csf)


%% heuristic: BG subtraction
V_bg_sub_cell = cell(cellsize);
V_bg_sub_reg_cell = cell(cellsize);
disp('BG subtraction... Start')
for a = 1:prod(cellsize)
    [i,j] = ind2sub(cellsize,a);
    row = (i-1)*32+1;
    col = (j-1)*32+1;
    rowt = min(row+blksize(1)-1, size(V,1));
    colt = min(col+blksize(1)-1, size(V,2));
    for fcount = 1:FNUM,
        block = double(V_csf(row:rowt,col:colt,fcount));
        bg = median(block(:));
        new_block = block-bg;
        new_block(new_block < BG_median_thr) = 0;
        V_bg_sub_cell{a}(:,:,fcount) = new_block;
    end
end
disp('BG subtraction... Finish')

V_bg_sub = zeros(size(V));
for a = 1:prod(cellsize)
    [i,j] = ind2sub(cellsize,a);
    row = (i-1)*32+1;
    col = (j-1)*32+1;
    rowt = min(row+blksize(1)-1, size(V,1));
    colt = min(col+blksize(1)-1, size(V,2));
    level = graythresh(V_bg_sub_cell{a});
    V_bg_sub(row:rowt,col:colt,:) = V_bg_sub_cell{a};
end

figure(4);
imshow3D(V_bg_sub);
drawnow;
%% Small Region Filter
% h = fspecial('gaussian',[5 5],5);
% f = imfilter(double(V_bg_sub),h);
V_thr = Threshold(V_bg_sub,thr_val);
V_reg = EliminateSmallReg(V_bg_sub,region_size);
figure(6)
imshow3D(V_reg);
% keyboard

%% OOF
disp('OOF started');
s = size(V_reg);
newE = zeros(s+(r_oof+3)*2);
newE(r_oof+4:r_oof+3+s(1), r_oof+4:r_oof+3+s(2), r_oof+4:r_oof+3+s(3)) = V_reg;
Eoof_block = oof3response(newE,1:r_oof,opts);
disp('OOF finished');
%% detect seed points by ridge points
figure(7)
imshow3D(Eoof_block)
figure(8)
imshow3D(Eoof_block>OOF_thr)
[fx,fy,~] = AM_gradient(Eoof_block);
seedsx = fx(:,1:end-1,:)>0&fx(:,2:end,:)<0&Eoof_block(:,1:end-1,:) > OOF_thr;
seedsy = fy(1:end-1,:,:)>0&fy(2:end,:,:)<0&Eoof_block(1:end-1,:,:) > OOF_thr;
seeds = seedsx(1:end-1,:,:)&seedsy(:,1:end-1,:);

figure(9)
imshow3D(seeds)
seeds = padarray(seeds,[1 1],'symmetric','post');


%% Save filtered image and seed points
smooth_cell = cellfun(@(x)single(x),V_bg_sub_cell,'UniformOutput',false);
smooth = single(V_bg_sub);
if mkdir(imgdir)
    save ([imgdir savefile], 'V', 'smooth', 'seeds', 'blksize', 'cellsize', '-v7.3');
    disp('Save');
else
    error('ERROR: cannot save preprocessed image stack');
end

%% Show seed points
for fcount = 1:FNUM
    figure(10)
    fcount
    imshow(V_bg_sub(:,:,fcount),[])
    [py, px] = find(seeds(:,:,fcount));
    hold on;
    plot(px,py,'b+')
    hold off;
end
