% Compute multi-scale DRF at every scale and display

close all
clear all
clc

addpath('./vol3d')
addpath('./AMT')
addpath('./frangi/')
addpath('./utils')
addpath('./DRF')
addpath('./snake')
addpath('./block')
addpath('./topology')
addpath('./ba_interp3')
addpath('./oof3response')
addpath(genpath('./toolbox_fast_marching'))
addpath(genpath('./UGM'))

load './paramsDRF2.mat'

filename = 'n72413_h0';
loadname = ['./Yo/' filename '.mat'];
load(loadname, 'V', 'smooth', 'cellsize')

%% DRF at pixel level with no interaction
% get DRF graphical moodel
snakefile = ['snkDRF_' filename '.mat'];
load(snakefile, 'tubes', 'blksize');

tube = zeros(size(V));
for a = 1:numel(tubes)
    [i,j] = ind2sub(cellsize,a);
    disp([i,j])
    row = ((i-1)*32)+1;
    col = ((j-1)*32)+1;
    rowt = min(row+blksize(1)-1, size(V,1));
    colt = min(col+blksize(1)-1, size(V,2));

    U = smooth(row:rowt,col:colt,:);

    edgeStruct = BLK_getEdgeStruct(size(U));


    % Use theta trained from Diadem
    nodes = reshape(U, numel(U), 1);
    n1 = edgeStruct.edgeEnds(:,1);
    n2 = edgeStruct.edgeEnds(:,2);
    edges = abs(nodes(n1)-nodes(n2));

    % Inference using Graph-Cut
    [~,pot] = DRF_infer_GC( result, double(nodes), double(edges), edgeStruct);
    
    row = ((i-1)*blksize(1));
    col = ((j-1)*blksize(2));
    rowt = min(row+blksize(1), size(V,1));
    colt = min(col+blksize(2), size(V,2));
    tube(row+1:rowt, col+1:colt, :) = reshape(pot(:,2), size(U));
end
figure(1)
imshow3D(tube)

%%
blk_names = flipud(dir(['blkDRF_' filename '*.mat']));
level = length(blk_names);

blkprob = cell(1,level+1);
P = cell(1,level+1);

P{1} = zeros(size(V));
for i = 1:size(tubes,1)
    for j = 1:size(tubes,2)
        row = ((i-1)*blksize(1));
        col = ((j-1)*blksize(2));
        rowt = min(row+blksize(1), size(V,1));
        colt = min(col+blksize(2), size(V,2));
        P{1}(row+1:rowt, col+1:colt, :) = tubes{i,j};
    end
end
bsize = ceil(size(tubes)/3);
blkprob{1} = zeros(size(tubes));
for a = 1:prod(bsize)
    [i,j] = ind2sub(bsize,a);
    i = (i-1)*3;
    j = (j-1)*3;
    nRows = 3;
    if i+3 > size(tubes,1)
        nRows = size(tubes,1)-i;
    end
    nCols = 3;
    if j+3 > size(tubes,2)
        nCols = size(tubes,2)-j;
    end
    blockStruct = BLK_getStruct(nRows, nCols, 1);
    row = (i*blksize(1))+1;
    col = (j*blksize(2))+1;
    rowt = min(row+blksize(1)*nRows-1, size(V,1));
    colt = min(col+blksize(2)*nCols-1, size(V,2));
    im = smooth(row:rowt, col:colt, :);

    % Load snakes, Fext, Egvf, Tube
    inittube = zeros(size(im));
    for r = 1:blockStruct.nRows
        for c = 1:blockStruct.nCols
            row = ((r-1)*blksize(1));
            col = ((c-1)*blksize(2));
            rowt = min(row+blksize(1), size(im,1));
            colt = min(col+blksize(2), size(im,2));
            inittube(row+1:rowt, col+1:colt, :) = tubes{r+i,c+j};
        end
    end

    % combine DRF snake-level and pixel-level
%     disp('Initialise Probability Map ...')
    update_opts.FrangiScaleRange = [1,8];
    [~, ~, ~, prob] = BLK_update( blockStruct, blksize, im, inittube, update_opts);
    blkprob{1}(i+1:i+nRows,j+1:j+nCols) = prob;
end

figure(2)
imshow3D(P{1})

%%

for n = 1:level
    load(blk_names(n).name,'tubes', 'blksize');
	P{n+1} = zeros(size(V));
    for i = 1:size(tubes,1)
        for j = 1:size(tubes,2)
            row = ((i-1)*blksize(1));
            col = ((j-1)*blksize(2));
            rowt = min(row+blksize(1), size(V,1));
            colt = min(col+blksize(2), size(V,2));
            P{n+1}(row+1:rowt, col+1:colt, :) = tubes{i,j};
        end
    end

    bsize = ceil(size(tubes)/3);
    blkprob{n+1} = zeros(size(tubes));
    for a = 1:prod(bsize)
        [i,j] = ind2sub(bsize,a);
        i = (i-1)*3;
        j = (j-1)*3;
        nRows = 3;
        if i+3 > size(tubes,1)
            nRows = size(tubes,1)-i;
        end
        nCols = 3;
        if j+3 > size(tubes,2)
            nCols = size(tubes,2)-j;
        end
        blockStruct = BLK_getStruct(nRows, nCols, 1);
        row = (i*blksize(1))+1;
        col = (j*blksize(2))+1;
        rowt = min(row+blksize(1)*nRows-1, size(V,1));
        colt = min(col+blksize(2)*nCols-1, size(V,2));
        im = smooth(row:rowt, col:colt, :);

        % Load snakes, Fext, Egvf, Tube
        inittube = zeros(size(im));
        for r = 1:blockStruct.nRows
            for c = 1:blockStruct.nCols
                row = ((r-1)*blksize(1));
                col = ((c-1)*blksize(2));
                rowt = min(row+blksize(1), size(im,1));
                colt = min(col+blksize(2), size(im,2));
                inittube(row+1:rowt, col+1:colt, :) = tubes{r+i,c+j};
            end
        end

        % combine DRF snake-level and pixel-level
        disp('Initialise Probability Map ...')
        update_opts.FrangiScaleRange = [1,8];
        [~, ~, ~, prob] = BLK_update( blockStruct, blksize, im, inittube, update_opts);
        blkprob{n+1}(i+1:i+nRows,j+1:j+nCols) = prob;
    end
    
    figure(n+2)
    imshow3D(P{n+1})
end

% %%
% figure
% load mri.mat
% D = uint8(P{n+1}*255);
% h = vol3d('cdata',D,'texture','3D');
% view(3);  
% axis tight;  daspect([1 1 .4])
% alphamap('rampup');
% alphamap(.06 .* alphamap);

%%
% 
% h = figure
% imshow(V(:,:,4))
% % saveas(h, ['sample_', filename, '_0.png']);
% h = figure
% imshow(V(865:992,897:960,1))
% % saveas(h, ['sample_', filename, '_00.png']);
% h = figure;
% imshow(tube(865:992,897:960,1)); colormap jet; colorbar;
% % saveas(h, ['sample_', filename, '_01.png']);
% blksize = [32 32 23];
% imgtmp = cell(1,level+1);
% for n = 1:level+1
%     h = figure(n+10);
%     imgtmp{n} = P{n}(865:992,897:960,1);
%     imshow(imgtmp{n});  colormap jet; colorbar;
%     hold on;
%     for i = 1:ceil(size(imgtmp{n},1)/blksize(1))-1
%         plot([1,size(V,1)],blksize(1)*[i,i],'Color','k','LineWidth',2)
%     end
%     for i = 1:ceil(size(imgtmp{n},2)/blksize(2))-1
%         plot(blksize(2)*[i,i],[1,size(V,2)-1],'Color','k','LineWidth',2)
%     end
%     hold off;
%     blksize = blksize .* [3 3 1];
% %     saveas(h, ['sample_', filename, '_', sprintf('%02d',n+1),'.png']);
% end


%%

h = figure(10)
imshow(V(:,:,1))
saveas(h, ['sample_', filename, '_0.png']);
h = figure(11)
imshow(V(865:992,897:960,1))
saveas(h, ['sample_', filename, '_00.png']);

imgtmp = cell(1,level+1);
for n = 1:level+1
    imgtmp{n} = P{n}(865:992,897:960,1);
end


blksize = [32 32 23];
s = size(imgtmp{1});
for n = 1:level
    fig = figure(n);
    imshow(imgtmp{n+1});

    dif = abs(imgtmp{n} - imgtmp{n+1});
    red = cat(3, ones(s), zeros(s), zeros(s));
    hold on
    h = imshow(red);
    hold off
    set(h, 'AlphaData', dif./0.22);
    
    hold on;
    for i = 1:ceil(size(imgtmp{n},1)/blksize(1))-1
        plot([1,size(V,1)],blksize(1)*[i,i],'Color','b','LineWidth',3)
    end
    for i = 1:ceil(size(imgtmp{n},2)/blksize(2))-1
        plot(blksize(2)*[i,i]+1,[1,size(V,2)-1],'Color','b','LineWidth',3)
    end
    hold off;
    blksize = blksize .* [3 3 1];
    saveas(h, ['sample_', filename, '_', sprintf('%02d',n),'.png']);
end

h = figure(level+1);
imshow(imgtmp{level+1});
saveas(h, ['sample_', filename, '_', sprintf('%02d',level+1),'.png']);

%%
h = figure
imshow(V(780:900,440:640,1))
saveas(h, ['sample2_', filename, '_0.png']);
h = figure;
imshow(tube(780:900,440:640,1));
saveas(h, ['sample2_', filename, '_1.png']);
h = figure;
imshow(P{5}(780:900,440:640,1));
saveas(h, ['sample2_', filename, '_2.png']);