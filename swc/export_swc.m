function data = export_swc( opensnk, savefile, V, factor )
%EXPORT_SWC create neuron file or .swc file from 1D array of snake struct
%	  Input
%	  opensnk		1D array of snake struct
%	  savefile		save filename
%     V             3D image stack
%	  factor		for resizing trace when downsample the image stack
%
%	  Ouput 
%	  data			2D array in .swc format
%

if nargin <= 3
    factor = 1;
end

%%
nsnks = size(opensnk,2);
p_snk = arrayfun(@(x)size(x.vert,1),opensnk);
p_snk = [0 cumsum(p_snk)];
points = vertcat(opensnk.vert);
edge = zeros(0,2);
for i = 1:nsnks
    tmp = [p_snk(i)+2:p_snk(i+1); p_snk(i)+1:p_snk(i+1)-1]';
    n1 = i;
    for j = 1:length(opensnk(i).snkh)
        n2 = opensnk(i).snkh(j);
        ds_h = bsxfun(@minus, opensnk(n2).vert, opensnk(n1).vert(1,:));
        [~,pos] = min(sqrt(sum(ds_h.^2,2)));
%         if dis > 1
%             warning('distance between snakes is %d', dis);
%         end
        tmp = [tmp;p_snk(n1)+1, p_snk(n2)+pos];
    end
    for j = 1:length(opensnk(i).snkt)
        n2 = opensnk(i).snkt(j);
        ds_t = bsxfun(@minus, opensnk(n2).vert, opensnk(n1).vert(end,:));
        [~,pos] = min(sqrt(sum(ds_t.^2,2)));
%         if dis > 1
%             warning('distance between snakes is %d', dis);
%         end
        tmp = [tmp;p_snk(i+1), p_snk(opensnk(i).snkt(j))+pos];
    end
    edge = [edge; tmp];
end
%%
DG = sparse( edge(:,1), edge(:,2), 1, p_snk(end), p_snk(end) );
UG = DG + DG';
% ST = graphminspantree(UG, 'Method', 'Kruskal');
ST = kruskal_mst(UG);

%% Rearrange points not to violate
N = p_snk(end);
visited = false(1,N);
new_data = cell(1,N);
cur = 1;
while any(~visited)
%     [order,pred] = graphtraverse(ST,find(~visited,1),'Method','BFS','Directed',false);
    [order, pred] = bfs_traverse(ST, find(~visited,1));
    data = zeros(size(order,2), 7);
    data(:,1) = order;
    data(:,2) = 2;
    data(:,3:5) = points(order,:);
    data(:,3:4) = data(:,3:4) .* factor;
    data(:,6) = 1;
    data(:,7) = pred(order);

    newIdx = zeros(size(order));
    newIdx(order) = 1:size(order,2);
    newIdx = newIdx + sum(visited);
    data(:,1) = newIdx(data(:,1));
    data(1,7) = -1;
    data(2:end,7) = newIdx(data(2:end,7));
    new_data{cur} = data;
    cur = cur+1;
    visited(order) = true;
end
data = vertcat(new_data{:});


%% show image
if ~exist('V', 'var')
    V = zeros(1024,1024,1);
end
colmap = jet(sum(data(:,7)==-1));
h = figure;
imshow(mean(V,3),[]);
hold on
n = 0;
for i = 1:size(data,1)
    if data(i,7) > 0
        idx = [i, data(i,7)];
        plot(data(idx,3)./factor, data(idx,4)./factor, 'Color', colmap(n,:), 'Linewidth', 2)
    else
        n = n+1;
    end
end
hold off

if ~isempty(savefile)
    saveas(h, [savefile,'.png'], 'png');

    fileID = fopen(savefile,'w');
    fprintf(fileID,'# Neurolucida to SWC conversion from L-Measure.\n');
    fprintf(fileID,'%.0f %d %.3f %.3f %.3f %.4f %.0f\n',data');
    fclose(fileID);
end
end

