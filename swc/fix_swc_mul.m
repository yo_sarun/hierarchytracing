% Fix neuron file or .swc file while keep all components

%% Read SWC file
% load '../Yo/nOP_7.mat' 'V'

% loadfile = './OP/nOP_7.swc';
% imgfile = 'choOP_7.png';
% loadfile = 'myCF_1_old.swc';
% loadfile = '../result/n06.swc';
% savefile = 'hohoNC_1.swc';
% loadfile = '../my72413_h0.swc';
% savefile = 'new72413_h0.swc';

% factor = 4;

disp('Reading SWC file ...')
fid=fopen(loadfile, 'rt'); 
% this is error message for reading the file
if fid == -1 
    error('File could not be opened, check name or path.')
end

data = [];
tline = fgetl(fid);
while ischar(tline) 
    % reads a line of data from file.
    vnum = sscanf(tline, '%d %d %f %f %f %f %d');
    data = cat(1,data,vnum');
    tline = fgetl(fid);
end
fclose(fid);

%% Find Spanning Tree
N = size(data,1);
E = [data(:,1) data(:,7)];
E = E(all(E>0,2),:);
DG = sparse(E(:,1),E(:,2), ones(size(E,1),1), N, N);
UG = tril(DG + DG');
ST = graphminspantree(UG, 'Method', 'Kruskal');

%% Rearrange points not to violate 
visited = false(1,N);
new_data = cell(1,N);
cur = 1;
while any(~visited)
    [order,pred] = graphtraverse(ST,find(~visited,1),'Method','BFS','Directed',false);
    
    data_tmp = data(order,:);
    data_tmp(:,7) = pred(order);
    newIdx = zeros(size(order));
    for i = 1:size(order,2)
        newIdx(order(i)) = i + sum(visited);
    end
    data_tmp(:,1) = newIdx(data_tmp(:,1));
    data_tmp(1,7) = -1;
    data_tmp(2:end,7) = newIdx(data_tmp(2:end,7));
    new_data{cur} = data_tmp;
    cur = cur+1;
    visited(~isnan(pred)) = true;
end
new_data = new_data(~cellfun(@(x)isempty(x),new_data));
data = vertcat(new_data{:});
%% show image
if ~exist('V', 'var')
    V = zeros(1024,1024,1);
end
colmap = jet(sum(data(:,7)==-1));
h = figure;
imshow(mean(V,3),[]);
hold on
n = 0;
for i = 1:size(data,1)
    if data(i,7) > 0
        idx = [i, data(i,7)];
        plot(data(idx,3)./factor, data(idx,4)./factor, 'Color', colmap(n,:), 'Linewidth', 2)
    else
        n = n+1;
    end
end
hold off

saveas(h, imgfile, 'png');
%%

% fileID = fopen(savefile,'w');
% fprintf(fileID,'# Neurolucida to SWC conversion from L-Measure.\n');
% fprintf(fileID,'%.0f %d %.3f %.3f %.3f %.4f %.0f\n',data')
% fclose(fileID);
