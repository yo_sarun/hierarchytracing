% Fix neuron file or .swc file for drosophila dataset

%% Read Image
disp('Reading images ...')
currentpath = cd;
% filename = 'img72413_h0.tif';
filename = 'img72513_h0.tif';
datapath = '/Users/Yo/Desktop/CRFSNK_predict/';

cd(datapath)
InfoImage=imfinfo(filename);
FNUM = length(InfoImage);
V=zeros(InfoImage(1).Height,InfoImage(1).Width,FNUM,'uint8');
for i=1:FNUM
   V(:,:,i)=imread(filename,'Index',i,'Info',InfoImage);
end
cd(currentpath)

%% fix nreg72413_h0.swc
loadfile = 'nreg72413_h0.swc';
savefile = 'cho72413_h0.swc';
imgfile = 'cho72413_h0.png';
factor = 1;
% fix_swc_royCF;
fix_swc_mul;

%% fix nreg72513_h0.swc
loadfile = 'nreg72513_h0.swc';
savefile = 'cho72513_h0.swc';
imgfile = 'cho72513_h0.png';
factor = 1;
% fix_swc_royCF;
fix_swc_mul;

%% fix r72413_h0.swc
loadfile = 'r72413_h0.swc';
savefile = 'roy72413_h0.swc';
imgfile = 'roy72413_h0.png';
factor = 1;
% fix_swc_royCF;
fix_swc_mul;

%% fix r72513_h0.swc
loadfile = 'r72513_h0.swc';
savefile = 'roy72513_h0.swc';
imgfile = 'roy72513_h0.png';
factor = 1;
% fix_swc_royCF;
fix_swc_mul;

%% fix reg72413_h0_1vs_S1.swc
loadfile = 'reg72413_h0_1vs_S1.swc';
savefile = 'bas72413_h0.swc';
imgfile = 'bas72413_h0.png';
factor = 1;
% fix_swc_royCF;
fix_swc_mul;

%% fix reg72513_h0_1vs_S1.swc
loadfile = 'reg72513_h0_1vs_S1.swc';
savefile = 'bas72513_h0.swc';
imgfile = 'bas72513_h0.png';
factor = 1;
% fix_swc_royCF;
fix_swc_mul;

%% fix ori72413_h0.swc
loadfile = 'ori72413_h0.swc';
savefile = 'orig72413_h0.swc';
imgfile = 'orig72413_h0.png';
factor = 1;
fix_swc_royCF;

%% fix ori72513_h0.swc
loadfile = 'ori72513_h0.swc';
savefile = 'orig72513_h0.swc';
imgfile = 'orig72513_h0.png';
factor = 1;
fix_swc_royCF;