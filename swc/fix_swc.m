function fix_swc( loadfile, savefile, V, factor )
%FIX_SWC find only one neuron from the given .swc and save into file
%	  Input
%	  loadfile		load filename
%	  savefile		save filename
%	  V				3D image stack
%	  factor		for resizing trace when downsample the image stack
%

if nargin < 4
    factor = 1;
end
%% Read SWC file
% loadfile = 'basCF_1.swc';
% savefile = 'yoCF_1.swc';
data = read_swc_file(loadfile);
% disp('Reading SWC file ...')
% fid=fopen(loadfile, 'rt'); 
% % this is error message for reading the file
% if fid == -1 
%     error('File could not be opened, check name or path.')
% end
% 
% data = [];
% tline = fgetl(fid);
% while ischar(tline) 
%     % reads a line of data from file.
%     vnum = sscanf(tline, '%d %d %f %f %f %f %d');
%     data = cat(1,data,vnum');
%     tline = fgetl(fid);
% end
% fclose(fid);

%% Find Spanning Tree
N = size(data,1);
E = [data(:,1) data(:,7)];
E = E(all(E>0,2),:);
DG = sparse(E(:,1),E(:,2), ones(size(E,1),1), N, N);
UG = DG + DG';
ST = kruskal_mst(UG);
% [ST,pred] = graphminspantree(UG);
[ci,sizes] = components(ST);
[~,comp] = max(sizes);

%% Rearrange points not to violate 
[order,pred] = bfs_traverse(ST,find(ci==comp,1));
% [order,pred] = graphtraverse(ST,find(pred==0),'Method','BFS','Directed',false);
data = data(order,:);
data(:,7) = pred(order);
newIdx = zeros(size(order));
for i = 1:size(order,2)
    newIdx(order(i)) = i;
end
data(:,1) = newIdx(data(:,1));
data(1,7) = -1;
data(2:end,7) = newIdx(data(2:end,7));

%% show image
if nargin == 3 || exist('V', 'var')
    h = figure;
    imshow(mean(V,3),[]);
    hold on
    for i = 1:size(data,1)
        if data(i,7) > 0
            idx = [i, data(i,7)];
%             plot(data(idx,3), data(idx,4), 'r', 'Linewidth', 3)
            plot(data(idx,3)./factor, data(idx,4)./factor, 'c', 'Linewidth', 2)
        end
    end
    hold off
    saveas(h, [savefile,'.png'], 'png');
end
%%
fileID = fopen(savefile,'w');
fprintf(fileID,'# Neurolucida to SWC conversion from L-Measure.\n');
fprintf(fileID,'%.0f %d %.3f %.3f %.3f %.4f %.0f\n',data');
fclose(fileID);
% end

