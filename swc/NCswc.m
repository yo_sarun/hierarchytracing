% Fix neuron file or .swc file for NC/NL dataset from DIADEM Challenge

%%
offset = [73,507,-5;...
        526,484,11;...
        952,462,-21;...
        924,3,-19;...
        468,-14,-1;...
        0,0,0];
offset = bsxfun(@plus,offset,[1 15 22]);
factor = 1;

%%
V_final = zeros(1000,1500);
load('/Users/Yo/Desktop/CRFSNK_predict/Yo/n01.mat')
V_final(offset(1,2):offset(1,2)+511, offset(1,1):offset(1,1)+511) = mean(V,3);
load('/Users/Yo/Desktop/CRFSNK_predict/Yo/n02.mat')
V_final(offset(2,2):offset(2,2)+511, offset(2,1):offset(2,1)+511) = mean(V,3);
load('/Users/Yo/Desktop/CRFSNK_predict/Yo/n03.mat')
V_final(offset(3,2):offset(3,2)+511, offset(3,1):offset(3,1)+511) = mean(V,3);
load('/Users/Yo/Desktop/CRFSNK_predict/Yo/n04.mat')
V_final(offset(4,2):offset(4,2)+511, offset(4,1):offset(4,1)+511) = mean(V,3);
load('/Users/Yo/Desktop/CRFSNK_predict/Yo/n05.mat')
V_final(offset(5,2):offset(5,2)+511, offset(5,1):offset(5,1)+511) = mean(V,3);
load('/Users/Yo/Desktop/CRFSNK_predict/Yo/n06.mat')
V_final(offset(6,2):offset(6,2)+511, offset(6,1):offset(6,1)+511) = mean(V,3);

%%
h = figure
imshow(min(V_final*10,255),[])
saveas(h, 'NC_1.png', 'png');
im_names = dir('./NC/*.swc');
N = length(im_names);
colmap = jet(N+5);
for i = 1:N,
    n = i
    loadfile = ['./NC/' im_names(i).name];
    fix_swc_mul_NC;
end
saveas(h, 'oriNC_1.png', 'png');