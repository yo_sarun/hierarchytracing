% Fix neuron files or .swc files from roysam algorithm
%% 
load '../Yo/roynCF_1.mat' 'V'
loadfile = 'royCF_1.swc';
savefile = 'rCF_1.swc';
imgfile = 'rCF_1.png';
factor = 1;
% fix_swc_royCF;
fix_swc_mul

%% 
load '../Yo/roynCF_2.mat' 'V'
loadfile = 'royCF_2.swc';
savefile = 'rCF_2.swc';
imgfile = 'rCF_2.png';
factor = 1;
% fix_swc_royCF;
fix_swc_mul;