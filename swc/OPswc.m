% Fix neuron file or .swc file for OP dataset from DIADEM Challenge

factor = 1;

%% ground truth
load '../Yo/nOP_7.mat' 'V'
loadfile = './OP/OP_7.swc';
imgfile = 'oriOP_7.png';
fix_swc_mul;

load '../Yo/nOP_8.mat' 'V'
loadfile = './OP/OP_8.swc';
imgfile = 'oriOP_8.png';
fix_swc_mul;

load '../Yo/nOP_9.mat' 'V'
loadfile = './OP/OP_9.swc';
imgfile = 'oriOP_9.png';
fix_swc_mul;


%% roy
load '../Yo/nOP_7.mat' 'V'
loadfile = './OP/rOP_7.swc';
imgfile = 'royOP_7.png';
fix_swc_mul;

load '../Yo/nOP_8.mat' 'V'
loadfile = './OP/rOP_8.swc';
imgfile = 'royOP_8.png';
fix_swc_mul;

load '../Yo/nOP_9.mat' 'V'
loadfile = './OP/rOP_9.swc';
imgfile = 'royOP_9.png';
fix_swc_mul;


%% NTC
load '../Yo/nOP_7.mat' 'V'
loadfile = './OP/nOP_7.swc';
imgfile = 'choOP_7.png';
fix_swc_mul;

load '../Yo/nOP_8.mat' 'V'
loadfile = './OP/nOP_8.swc';
imgfile = 'choOP_8.png';
fix_swc_mul;

load '../Yo/nOP_9.mat' 'V'
loadfile = './OP/nOP_9.swc';
imgfile = 'choOP_9.png';
fix_swc_mul;


%% Principal Curve
load '../Yo/nOP_7.mat' 'V'
loadfile = './OP/Qual_OF_Test_7vs_S1.swc';
imgfile = 'basOP_7.png';
fix_swc_mul;

load '../Yo/nOP_8.mat' 'V'
loadfile = './OP/Qual_OF_Test_8vs_S1.swc';
imgfile = 'basOP_8.png';
fix_swc_mul;

load '../Yo/nOP_9.mat' 'V'
loadfile = './OP/Qual_OF_Test_9vs_S1.swc';
imgfile = 'basOP_9.png';
fix_swc_mul;


%% Our method
load '../Yo/nOP_7.mat' 'V'
loadfile = './OP/myOP_7.swc';
imgfile = 'myOP_7.png';
fix_swc_mul;

load '../Yo/nOP_8.mat' 'V'
loadfile = './OP/myOP_8.swc';
imgfile = 'myOP_8.png';
fix_swc_mul;

load '../Yo/nOP_9.mat' 'V'
loadfile = './OP/myOP_9.swc';
imgfile = 'myOP_9.png';
fix_swc_mul;