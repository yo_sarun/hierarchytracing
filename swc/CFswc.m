% Fix neuron file or .swc file for CF dataset from DIADEM Challenge

factor = 4;

%% ground truth
load '../Yo/nCF_1.mat' 'V'
loadfile = './CF/CF_1.swc';
imgfile = 'oriCF_1.png';
fix_swc_mul;

load '../Yo/nCF_2.mat' 'V'
loadfile = './CF/CF_2.swc';
imgfile = 'oriCF_2.png';
fix_swc_mul;

%% our method
load '../Yo/nCF_1.mat' 'V'
loadfile = './CF/yoyonCF_1.swc';
imgfile = 'mynCF_1.png';
fix_swc_mul;

load '../Yo/nCF_2.mat' 'V'
loadfile = './CF/myCF_2.swc';
imgfile = 'myCF_2.png';
fix_swc_mul;
