% Render tracing result in 3D

clearvars -except datapath datapaths filenames imgdir imgname snkdir swcdir sample_factor
close all;
clc;
addpath(genpath('./matlab_bgl'))
addpath('./bresenham_line3d/')
addpath('./utils/')
zfactor = 3;
%% Read SWC file
% load './Yo/n72413_h0.mat'
% loadfile = 'my72413_h0.swc';
load([imgdir imgname '.mat'])
loadfile = [swcdir 'my' imgname '.swc'];
disp('Reading SWC file ...')
fid=fopen(loadfile, 'rt'); 
% this is error message for reading the file
if fid == -1 
    error('File could not be opened, check name or path.')
end

data = [];
tline = fgetl(fid);
while ischar(tline) 
    % reads a line of data from file.
    vnum = sscanf(tline, '%d %d %f %f %f %f %d');
    data = cat(1,data,vnum');
    tline = fgetl(fid);
end
fclose(fid);

%% Find Spanning Tree
N = size(data,1);
E = [data(:,1) data(:,7)];
E = E(all(E>0,2),:);
DG = sparse(E(:,1),E(:,2), ones(size(E,1),1), N, N);
UG = DG + DG';
ST = kruskal_mst(UG);
%% Rearrange points not to violate 
visited = false(1,N);
new_data = cell(1,N);
cur = 1;
while any(~visited)
    find(~visited,1)
    [order,pred] = bfs_traverse(ST,find(~visited,1));
    
    data_tmp = data(order,:);
    data_tmp(:,7) = pred(order);
    newIdx = zeros(size(order));
    for i = 1:size(order,2)
        newIdx(order(i)) = i + sum(visited);
    end
    data_tmp(:,1) = newIdx(data_tmp(:,1));
    data_tmp(1,7) = -1;
    data_tmp(2:end,7) = newIdx(data_tmp(2:end,7));
    new_data{cur} = data_tmp;
    cur = cur+1;
    visited(order) = true;
end
new_data = new_data(~cellfun(@(x)isempty(x),new_data));
data = vertcat(new_data{:});
%% show image
s = size(V);
s(3) = s(3)*zfactor;
V_l = zeros(s);
n = 1;

for i = 1:size(data,1)
    if data(i,7) > 0
%         keyboard;
        idx = [i, data(i,7)];
        points = data(idx,3:5);
        points(:,3) = points(:,3).*zfactor;
        points(points<0) = 1;
        points(points(:,1)>s(2),1) = s(2); 
        points(points(:,2)>s(1),2) = s(1); 
        points(points(:,3)>s(3),3) = s(3);
        [X,Y,Z] = bresenham_line3d(floor(min(points)), ceil(max(points)));
%         keyboard;
        idx = sub2ind(s,Y,X,Z);
        V_l(idx) = 255;
%         plot(data(idx,3)./4, data(idx,4)./4, 'Color', colmap(n,:), 'Linewidth', 3)
    else
        n = n+1;
    end
end
%%
I = imdilate(V_l, ones(3, 3, 3));
% h = fspecial('gaussian',[5 5 5],5);
% f = imfilter(double(I),h);
f = smooth3(I,'gaussian',[5 5 5], 5);
f = downsample(permute(f,[2 1 3]),4);
f = downsample(permute(f,[2 1 3]),4);
f = padarray(f,[1 1 1]);
f = flipdim(f, 1);
figure(1)
imshow3D(f)

%%
[ x, y, z ] = meshgrid(1:size(f,2), 1:size(f,1), 1:size(f,3));

[T, p, col] = isosurface(x, y, z, f, 20, z);

h = figure(2)
clf
pa = patch('Faces',T,'Vertices',p,'FaceVertexCData',col, 'FaceColor','interp', 'EdgeColor', 'none');
view(-11, 40)
% view(10,-80)
colormap(jet(256))
set(gca, 'DataAspectRatioMode', 'manual')
set(gca, 'DataAspectRatio', [1 1 1])
set(pa, 'FaceLighting', 'phong')
light( 'Position', [0 0 1000])
set(gca,'xtick',[])
set(gca,'xticklabel',[])
set(gca,'ytick',[])
set(gca,'yticklabel',[])
set(gca,'ztick',[])
set(gca,'zticklabel',[])
box on
saveas(h, [swcdir imgname '_3D.png'], 'png');