% Preprocessing step for vascusynth dataset

% read volume image into MATLAB:
% use following toolbox:
% http://www.mathworks.com/matlabcentral/fileexchange/27983-3d-slicer
here = cd;
cd('/Users/Yo/Desktop/March_2013_VascuSynth_Dataset/Group1/data9')
info = metaImageInfo('testVascuSynth9_101_101_101_uchar.mhd');
V = metaImageRead(info);

% read in tree structure information
load treeStructure_9.mat
cd(here)

% locate root, bifurcation and terminal indices
b = struct2cell(node);
root_index = find(sum(squeeze(strcmp(b,'root'))));
bif_index = find(sum(squeeze(strcmp(b,'bif'))));
term_index = find(sum(squeeze(strcmp(b,'terminal'))));

% visualize volume image
isosurface(V, 20)
axis([0 101 0 101 0 101])
alpha(.4)
 
% visualize bifurcation locations
coord = [node(bif_index(:)).coord];
coord = reshape(coord,3, length(coord)/3);
x = coord(1,:);
y = coord(2,:);
z = coord(3,:);
hold on
scatter3(x, y, z, 80, 'r', 'filled')
