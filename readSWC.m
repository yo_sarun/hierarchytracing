% Learn DRF parameters at voxel level

clear all;
close all;
clc;

addpath('./AMT');
addpath('./DRF');
addpath('./CSF');
addpath('./block');
addpath('./utils');
addpath('./oof3response')
addpath('./PreprocessLib/');
addpath(genpath('./UGM'));
addpath(genpath('./CurveLab-2.1.3'));


% directory = '//Users/Yo/Desktop/train_data/';
directory = 'C:/Neuron/train_data/';
folder = 'OP_1';

%% Read SWC file
disp('Reading SWC file ...')
fid=fopen([directory 'Gold_Standard/' folder '.swc'], 'rt'); 
% this is error message for reading the file
if fid == -1 
    error('File could not be opened, check name or path.')
end

data = [];
tline = fgetl(fid);
while ischar(tline) 
    % reads a line of data from file.
    vnum = sscanf(tline, '%d %d %f %f %f %f %d');
    data = cat(1,data,vnum');
    tline = fgetl(fid);
end
data(:,5) = data(:,5)+1;
fclose(fid);

%% Read Image Stack
disp('Reading image stack ...')
f = [directory 'Image_Stacks/' folder '/'];
im_names = dir([f '*.tif']);
FNUM = length(im_names);

for fcount = 1:FNUM,
    V(:,:,fcount) = imread([f im_names(fcount).name],'tif');
end

%% Get label
disp('--------------------------------------------------')
disp('Get binary image ...')
s = size(V);
bimg = zeros(s);
for i = 1:size(data,1)
    a = max(floor(data(i,3:5) - data(i,6)), ones(1,3));
    b = min(ceil(data(i,3:5) + data(i,6)), permute(s, [2 1 3])');
    [pX,pY,pZ] = meshgrid(a(1):b(1),a(2):b(2),a(3):b(3));
    blk = numel(pX);
    Ni = [reshape(pX,blk,1), reshape(pY,blk,1), reshape(pZ,blk,1)];
    dist = sqrt(sum((Ni-repmat(data(i,3:5),blk,1)).^2,2));
    points = Ni(dist <= data(i,6)+1,:);
    ind = sub2ind(size(V), points(:,2), points(:,1), points(:,3));
    bimg(ind) = 1;
end

figure(1);
imshow3D(bimg), drawnow;

%% Curvelet Preprocessing
curvelets=zeros(size(V));
sigCurv=20;
for i=1:FNUM  
	disp(['Curvelets #',num2str(i),' started']);
	[curvelets(:,:,i)]=NfdctDemo(V(:,:,i),sigCurv);
end
figure(11)
imshow3D(curvelets)
%% LoG
S_csf = 2;
BG_median_thr = 30;
V_csf = CSF_cos(uint8(curvelets),S_csf);
V_csf(V_csf < BG_median_thr) = 0;
figure(12)
imshow3D(V_csf)

%%
% E = double(V_csf)./255;
E = double(V_csf);

%% init DRF parameters
disp('--------------------------------------------------')
disp('Train DRF parameters ...')
sample = E(:,:,38);
bin = bimg(:,:,38);
[ba,bi] = DRF_init_diadem( sample, bin, 1);
theta = double([ba;bi]);

%% learn DRF parameters
disp('--------------------------------------------------')
disp('Learn DRF parameters ...')
edgeStruct = BLK_getEdgeStruct([size(sample) 1]);

%% convert image to sameple feature vectors
nodes = reshape(sample, numel(sample), 1);
label = reshape(bin, numel(bin), 1);

n1 = edgeStruct.edgeEnds(:,1);
n2 = edgeStruct.edgeEnds(:,2);
edges = abs(nodes(n1)-nodes(n2));
ising = double(label(n1)==label(n2));
ising(ising==0) = -1;

%% Use saddle point approximation with graphcut to approximate likelihood
myobjfun = @(x) DRF_objfunGC(x, nodes, edges, ising, label, edgeStruct);
[result,fval,exitflag,output,lambda,grad,hessian]=fmincon(myobjfun,theta,...
        [],[],[],[],[-Inf;-Inf;0;-Inf],[0;Inf;Inf;0]);
disp('Done')
%% use fminunc to approximate likelihood
result2 = fminunc(myobjfun,theta);

%% save DRF parameters
disp('Save')
save 'paramsDRF2.mat' result result2
%% Show sample
disp('--------------------------------------------------')
disp('Show sample ...')
s = size(sample);
[ydcode1,pot1] = DRF_infer_GC( result, nodes, edges, edgeStruct );
tube1 = double(reshape(pot1(:,2), s(1), s(2)));

[ydcode2,pot2] = DRF_infer_GC( result2, nodes, edges, edgeStruct );
tube2 = double(reshape(pot2(:,2), s(1), s(2)));

[ydcode3,pot3] = DRF_infer_GC( theta, nodes, edges, edgeStruct );
tube3 = double(reshape(pot3(:,2), s(1), s(2)));

figure(15);
imshow3D(logical(reshape(ydcode1,s)-1));
figure(16);
imshow3D(tube1);
figure(17);
imshow3D(logical(reshape(ydcode2,s)-1));
figure(18);
imshow3D(tube2);
figure(19);
imshow3D(logical(reshape(ydcode3,s)-1));
figure(20);
imshow3D(tube3);

%%
% disp('--------------------------------------------------')
% disp('Show result ...')
% 
% % clearvars -except E result result2
% 
% s = size(E);
% edgeStruct = BLK_getEdgeStruct(s);
% 
% nodes = reshape(E, s(1)*s(2)*s(3), 1);%, reshape(U, s(1)*s(2)*s(3), 1)]; 
% n1 = edgeStruct.edgeEnds(:,1);
% n2 = edgeStruct.edgeEnds(:,2);
% edges = abs(nodes(n1)-nodes(n2));
% 
% [ydcode1,pot1] = DRF_infer_GC( result, nodes, edges, edgeStruct );
% tube1 = double(reshape(pot1(:,2), s(1), s(2), s(3)));
% 
% [ydcode2,pot2] = DRF_infer_GC( result2, nodes, edges, edgeStruct );
% tube2 = double(reshape(pot2(:,2), s(1), s(2), s(3)));
% 
% [ydcode3,pot3] = DRF_infer_GC( theta, nodes, edges, edgeStruct );
% tube3 = double(reshape(pot3(:,2), s(1), s(2), s(3)));
% 
% figure(15);
% imshow3D(logical(reshape(ydcode1,size(E))-1));
% figure(16);
% imshow3D(tube1);
% figure(17);
% imshow3D(logical(reshape(ydcode2,size(E))-1));
% figure(18);
% imshow3D(tube2);
% figure(19);
% imshow3D(logical(reshape(ydcode3,size(E))-1));
% figure(20);
% imshow3D(tube3);
