# README #

This code is the implementation of THREE-DIMENSIONAL NEURITE TRACING UNDER GLOBALLY VARYING CONTRAST.

### What is this repository for? ###

* Neuron tracing using multiple stretch open active contours with attraction model.

### How do I get set up? ###

* Code is implemented in Matlab 2013a in both mac and windows platform (need to recompile mex files for linux)
* run mex_all.m to compile all c-mex files.
* script.m is the main script.
* It use parallel toolbox.
* Data can be download from DIADEM Challenge website (http://diademchallenge.org)

### Files ###

* script.m - main script
* preprocess*.m - preprocess image stacks
* readSWC.m - learn DRF at voxel level parameter
* test_allsnk_topo.m - trace snake inside a block
* test_propagate_all_topo.m - trace snake between blocks (superblock)


### Citation ###
S. Gulyanon, N. Sharifai, S. Bleykhman, E. Kelly, M.D. Kim, A. Chiba, and G. Tsechpenakis, "Three-dimensional neurite tracing under globally varying contrast," Int'l Symposium on Biomedical Imaging: from Nano to Macro (ISBI), Brooklyn, NY, 2015.