addpath('/Users/gavriil/Documents/GABRIEL/RESEARCH_IUPUI/NeuronD3dDt/code/frangi')


[filename, datapath] = uigetfiles;
cd(datapath)
im_names = dir('*.jpg');
FNUM = length(im_names);
for fcount = 1:FNUM,
    fcount
    Xp = imread(im_names(fcount).name,'jpg');
    Xp = double(rgb2gray(Xp))/255;
    Xp = Xp-min(Xp(:)); Xp = Xp/max(Xp(:));
    X = smoothn(Xp,[1,1],'Gaussian');
    [mu,mask]=kmeansfun(round(255*X),5);
    B = double(mask<=2);
    %imshow(B),pause
    [S,PTS_end,PTS_jct,Hs,Hr,Hi,ellipse_axis_ratio,ellipse_orient] = letterimgproc(codepath,X,B);
    
    inme = im_names(fcount).name;
    inme = inme(1:length(inme)-4);
    
    eval(['S_' inme '=S;'])
    eval(['PTS_end_' inme '=PTS_end;'])
    eval(['PTS_jct_' inme '=PTS_jct;'])
    eval(['Hs_' inme '=Hs;'])
    eval(['Hr_' inme '=Hr;'])
    eval(['Hi_' inme '=Hi;'])
    eval(['ellipse_axis_ratio_' inme '=ellipse_axis_ratio;'])
    eval(['ellipse_orient_' inme '=ellipse_orient;'])
    
end