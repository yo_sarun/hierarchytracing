function [ev]=getEV(point,I)%,tube)
%
% This function FRANGIFILTER3D uses the eigenvectors of the Hessian to
% compute the likeliness of an image region to vessels, according
% to the method described by Frangi
%
% [J,Scale,Vx,Vy,Vz] = FrangiFilter3D(I, Options)
%
% inputs,
%   I : The input image volume (vessel volume)
%   Options : Struct with input options,
%       .FrangiScaleRange : The range of sigmas used, default [1 8]
%       .FrangiScaleRatio : Step size between sigmas, default 2
%       .FrangiAlpha : Frangi vesselness constant, treshold on Lambda2/Lambda3
%					   determines if its a line(vessel) or a plane like structure
%					   default .5;
%       .FrangiBeta  : Frangi vesselness constant, which determines the deviation
%					   from a blob like structure, default .5;
%       .FrangiC     : Frangi vesselness constant which gives
%					   the threshold between eigenvalues of noise and 
%					   vessel structure. A thumb rule is dividing the 
%					   the greyvalues of the vessels by 4 till 6, default 500;
%       .BlackWhite : Detect black ridges (default) set to true, for
%                       white ridges set to false.
%       .verbose : Show debug information, default true
%
% outputs,
%   J : The vessel enhanced image (pixel is the maximum found in all scales)
%   Scale : Matrix with the scales on which the maximum intensity 
%           of every pixel is found
%   Vx,Vy,Vz: Matrices with the direction of the smallest eigenvector, pointing
%				in the direction of the line/vessel.
%
% Literature, 
%	Manniesing et al. "Multiscale Vessel Enhancing Diffusion in 
%		CT Angiography Noise Filtering"
%
% Example,
%   % compile needed mex file
%   mex eig3volume.c
%
%   load('ExampleVolumeStent');
%   
%   % Frangi Filter the stent volume
%   options.BlackWhite=false;
%   options.FrangiScaleRange=[1 1];
%   Vfiltered=FrangiFilter3D(V,options);
%
%   % Show maximum intensity plots of input and result
%   figure, 
%   subplot(2,2,1), imshow(squeeze(max(V,[],2)),[])
%   subplot(2,2,2), imshow(squeeze(max(Vfiltered,[],2)),[])
%   subplot(2,2,3), imshow(V(:,:,100),[])
%   subplot(2,2,4), imshow(Vfiltered(:,:,100),[])
%
% Written by D.Kroon University of Twente (May 2009)


% Use single or double for calculations
if(~isa(I,'double')), I=single(I); end

% I is the result of imgaussian
% keyboard;
% tic;
p = floor(point);
pmin = max(ones(1,3),p-2);
s = size(I);
pmax = min(s([2 1 3]),p+3);
offset = point-pmin+ones(1,3);

yoyo = I(pmin(2):pmax(2),pmin(1):pmax(1),pmin(3):pmax(3));

[Dxx, Dyy, Dzz, Dxy, Dxz, Dyz] = Hessian3DLocal(yoyo);
[~,~,~,Vx,Vy,Vz]=eig3volume( ...
        single(Dxx),single(Dxy),single(Dxz), ...
        single(Dyy),single(Dyz),single(Dzz));

ev = zeros(1,3);
ev(1) = interp3(Vy,offset(1),offset(2),offset(3),'*linear');
ev(2) = interp3(Vx,offset(1),offset(2),offset(3),'*linear');
ev(3) = interp3(Vz,offset(1),offset(2),offset(3),'*linear');
% ev = ev([2 1 3]);
% toc;

% tic;
% [Dxx, Dyy, Dzz, Dxy, Dxz, Dyz] = Hessian3D(tube,4);
% [Lambda1,Lambda2,Lambda3,Vx,Vy,Vz]=eig3volume( ...
%         single(Dxx),single(Dxy),single(Dxz), ...
%         single(Dyy),single(Dyz),single(Dzz));
% evt = zeros(1,3);
% evt(1) = interp3(Vx,point(1),point(2),point(3),'*linear');
% evt(2) = interp3(Vy,point(1),point(2),point(3),'*linear');
% evt(3) = interp3(Vz,point(1),point(2),point(3),'*linear');
% toc;
% all(ev==evt)
end

