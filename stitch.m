% Stitch images and tracing snakes together for NC/NL image stack

addpath('./AMT')
addpath('./snake')
addpath('./utils')
addpath(genpath('./matlab_bgl'))

for i = 1:6
    str = sprintf('blkDRF_n%02d_1.mat', i);
    load([snkdir str])
    snks{i} = allsnk{1};
end

offset = [73,507,-5;...
        526,484,11;...
        952,462,-21;...
        924,3,-19;...
        468,-14,-1;...
        0,0,0];
%%
nsnks = cumsum([0 cellfun(@length,snks)]);
for n = 1:length(snks)
    for i = 1:length(snks{n})
        snks{n}(i).vert = bsxfun(@plus,snks{n}(i).vert,offset(n,:));
        snks{n}(i).label = snks{n}(i).label + nsnks(n);
        snks{n}(i).snkh = snks{n}(i).snkh + nsnks(n);
        snks{n}(i).snkt = snks{n}(i).snkt + nsnks(n);
    end
    for m = 1:n-1
        m
        for i = 1:length(snks{n})
            for j = 1:length(snks{m})
                if ~is_inside_boundary( snks{n}(i), snks{m}(j), [10000 10000 50000] )
                    continue
                end
                if SNK_collide(snks{n}(i), snks{m}(j), 1, 2)
                    snks{n}(i).snkh = [snks{n}(i).snkh snks{m}(j).label];
                elseif SNK_collide(snks{n}(i), snks{m}(j), 2, 2)
                    snks{n}(i).snkt = [snks{n}(i).snkt snks{m}(j).label];
                end
                if SNK_collide(snks{m}(j), snks{n}(i), 1, 2)
                    snks{m}(j).snkh = [snks{m}(j).snkh snks{n}(i).label];
                elseif SNK_collide(snks{m}(j), snks{n}(i), 2, 2)
                    snks{m}(j).snkt = [snks{m}(j).snkt snks{n}(i).label];
                end
            end
        end
    end
end

opensnk = [snks{:}];
%%
offset = bsxfun(@plus,offset,[1 15 22]);
%%
V_final = zeros(1000,1500);

for i = 1:6
    str = sprintf('n%02d.mat', i);
    load([imgdir str])
    V_final(offset(i,2):offset(i,2)+511, offset(i,1):offset(i,1)+511) = mean(V,3);
end

h = figure
V_final = min(V_final*10,255);
imshow(V_final,[])
%%

for i = 1:size(opensnk,2)
    opensnk(i).vert = bsxfun(@plus,opensnk(i).vert,[1 15 22]);
end
%%
h = figure(level), SNK_show({opensnk}, blksize, 'jet', 1, V_final, '');
saveas(h, [swcdir 'myNC_1.png'], 'png');