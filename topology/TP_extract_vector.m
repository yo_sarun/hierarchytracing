function [ Vi, Vj ] = TP_extract_vector(opensnk, blksize, i, j, fig)
%TP_EXTRACT_VECTOR extract the attraction model vector
%	Input
%	opensnk		1D array of snake struct
%	blksize		block size i.e. [nRows nCols nDeps]
%	i			index of snake 1
%	j			index of snake 1
%	fig			figure number for debugging
%
%	Output
%	Vi			Attraction model of snake i
%	Vj			Attraction model of snake j
%
Vi = zeros(2,3);
Vj = zeros(2,3);

%% generate potential map
if fig == i || fig == j
    stretch_opts.Figure = 2;
    stretch_opts.Angle = [0.866, 0.707, 0.5, 0];
else
    stretch_opts.Figure = 0;
end
start_points = round(vertcat(opensnk(i).vert(:,[2,1,3]))');
[stretch_i, ~] = TP_get_stretch_potential(opensnk(i), blksize, stretch_opts);
end_points = round(vertcat(opensnk(j).vert(:,[2,1,3]))');
[stretch_j, ~] = TP_get_stretch_potential(opensnk(j), blksize, stretch_opts);
potential_map = (stretch_i + stretch_j)./2;
% potential_map = min(stretch_i, stretch_j);

%% find path between snake i and j
options.nb_iter_max = Inf;
% options.end_points = end_points([2 1 3],:);
[D,~,Q] = perform_fast_marching(1./potential_map, start_points, options);

ind_i = sub2ind(blksize,start_points(1,:),start_points(2,:),start_points(3,:));
ind_j = sub2ind(blksize,end_points(1,:),end_points(2,:),end_points(3,:));
[geodist,p_j] = min(D(ind_j));
closest_points = Q(ind_j);
p_i = closest_points(p_j);
if ~isfinite(geodist) 
    warning(['Extract vector: geodist undefined ' num2str(i) ' and ' num2str(j)])
    return
end

Ni = size(opensnk(i).vert, 1);
Nj = size(opensnk(j).vert, 1);
arr_i = [ind_i(1), ind_i(end)];
arr_j = [ind_j(1), ind_j(end)];
if ~ismember(ind_i(p_i), arr_i) && ~ismember(ind_j(p_j), arr_j)
    warning(['Extract vector: body-to-body path ' num2str(i) ' and ' num2str(j)])
    return
end

if geodist == 0
    warning(['Extract vector: snake collide ' num2str(i) ' and ' num2str(j)])
    Vi = -1;
    return
end


%% get accumulate direction and weight
direction = (end_points(:,p_j) - start_points(:,p_i))';
if norm(direction) ~= 0
    direction = direction([2 1 3])./norm(direction);
end

Wij = opensnk(i).E * opensnk(j).E / (1+geodist^2);
Wij = max(0, Wij); % Wij must be greater than zero
if Wij == 0
%     keyboard;
    warning('Wij == 0');
    return
end

if p_i == 1
    Vi(1,:) = Wij .* direction;
elseif p_i == Ni
    Vi(2,:) = Wij .* direction;
end
if p_j == 1
    Vj(1,:) = Vj(1,:) + Wij .* -direction;
elseif p_j == Nj
    Vj(2,:) = Vj(2,:) + Wij .* -direction;
end
% Vsnk(closest_points,:) = Vsnk(closest_points,:) + Wij .* direction;

if fig == i || fig == j%fig ~= 0
    Wij
    Vi
    Vj
    [D,S,~] = perform_fast_marching(1./potential_map, start_points(:,p_i), options);
    gpath = compute_geodesic(D,end_points(:,p_j));
    if size(gpath,2) == 1
        opts.method = 'discrete';
        gpath = compute_geodesic(D,end_points(:,p_j), opts);
    end

    set(0,'DefaultFigureWindowStyle','docked')
    figure(j+10)
    plot_fast_marching_3d(D,S,gpath,start_points([2 1 3],:),end_points([2 1 3],:));

    figure(j+20)
    subplot(2,2,1); imagesc(stretch_i(:,:,2)); colormap gray;
    subplot(2,2,2); imagesc(stretch_j(:,:,2)); colormap gray;
    subplot(2,2,3); imagesc(potential_map(:,:,2)); colormap gray;
    subplot(2,2,4);
    TP_show_path(gpath, start_points, end_points, potential_map(:,:,1));
%     keyboard;
    close all;
end
end

