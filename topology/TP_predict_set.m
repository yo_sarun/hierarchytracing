function [ opensnk, snkcode, model ] = TP_predict_set( opensnk, blksize, snkcode, S, C, level, options )
%TP_PREDICT predict attraction model between set-of-snake
%	Input
%	opensnk			1D array of snake struct
%	blksize			block size i.e. [nRows nCols nDeps]
%	snkcode			binary 2D array telling which snake-end converges
%	S				number of snake segment
%	C				snake segment number
%	level			resolution scale
%	options			utility options
%
%	Output
%	opensnk			updated 1D array of snake struct
%	model			attraction model i.e. 1D cell array of attraction model struct
%	snkcode			updated binary 2D array telling which snake-end converges
%

%% Get options parameters
defaultoptions = struct('Collide_thr', 6, 'Confidence_thr', 10, 'Figure', 0, 'Debug', false);

if ~exist('options','var')
    options = defaultoptions;
else
    options = get_options(defaultoptions, options);
end

collide_thr = options.Collide_thr;
thr = options.Confidence_thr;
% fig = options.Figure;

%%
nsnk = size(opensnk,2);
model = struct('confidenceH', num2cell(zeros(1,nsnk)), ...
        'confidenceT', 0, ...
        'predict_directionH', zeros(1,3), ...
        'predict_directionT', zeros(1,3));
V = zeros(2,3,nsnk);
done = false(nsnk,2);
for i = 1:nsnk
    if opensnk(i).converge
        continue
    end
    model_opts.Collide_thr = options.Collide_thr;
    model_opts.Debug = options.Debug;
    model_opts.C_SNAKE = C(i);
    if nargout < 3
        model_opts.Model = false;
    end
    [ V(:,:,i), opensnk(i), done(i,:) ] = ...
            TP_model_set( opensnk(i), opensnk, snkcode(i,:), blksize, C, level, done(i,:), model_opts );
end

for i = 1:nsnk
    if opensnk(i).converge
        continue
    end
    
    % reset collide counter if no collision occurs
    if ~done(i,1) && opensnk(i).collideh < collide_thr
        opensnk(i).collideh = 0;
    elseif opensnk(i).collideh >= collide_thr
       snkcode(i,1) = 0;
    end
    if ~done(i,2) && opensnk(i).collidet < collide_thr
        opensnk(i).collidet = 0;
    elseif opensnk(i).collidet >= collide_thr
        snkcode(i,2) = 0;
    end
    
    if nargout == 3
        % normalise the weight and direction
        model(i).confidenceH = min(norm(V(1,:,i))/thr,1);
        if norm(V(1,:,i)) ~= 0
            model(i).predict_directionH = V(1,:,i) ./ norm(V(1,:,i));
        else
            model(i).predict_directionH = V(1,:,i);
        end

        model(i).confidenceT = min(norm(V(2,:,i))/thr,1);
        if norm(V(2,:,i)) ~= 0
            model(i).predict_directionT = V(2,:,i) ./ norm(V(2,:,i));
        else
            model(i).predict_directionT = V(2,:,i);
        end
    end
end

end

