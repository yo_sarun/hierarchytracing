function TP_show_path( gpath, start_points, end_points, I )
%TP_SHOW_PATH show shortest path over potential map
% 
%     Inputs
%     gpath           shortest path
%     start_points    source points
%     end_points      sink points
%     I               potential map

imagesc(I), colormap gray;
hold on;
plot(start_points(2,:),start_points(1,:),'-o','Color','b');
plot(end_points(2,:),end_points(1,:),'-o','Color','r');
plot(gpath(2,:),gpath(1,:),'-','Color','g');
hold off;
title('Geodesic Path')
drawnow;
end

