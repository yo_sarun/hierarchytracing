function [ opensnk, model, snkcode ] = TP_predict( opensnk, blksize, snkcode, options )
%TP_PREDICT predict attraction model between snakes
%	Input
%	opensnk			1D array of snake struct
%	blksize			block size i.e. [nRows nCols nDeps]
%	snkcode			binary 2D array telling which snake-end converges
%	options			utility options
%
%	Output
%	opensnk			updated 1D array of snake struct
%	model			attraction model i.e. 1D cell array of attraction model struct
%	snkcode			updated binary 2D array telling which snake-end converges
%

% Get options parameters
defaultoptions = struct('Collide_thr', 10, 'Confidence_thr', 0.1, 'Figure', 0);
if ~exist('options','var')
    options = defaultoptions;
else
    options = get_options(defaultoptions, options);
end

collide_thr = options.Collide_thr;
thr = options.Confidence_thr;
fig = options.Figure;

%%
nsnk = size(opensnk,2);
model = struct('confidenceH', num2cell(zeros(1,nsnk)), ...
        'confidenceT', 0, ...
        'predict_directionH', zeros(1,3), ...
        'predict_directionT', zeros(1,3));

%% Association potential (Bias)
V = zeros(2,3,nsnk);
% for i = 1:nsnk
%     V(1,:,i) = 0.1 * thr * (opensnk(i).vert(1,:) - opensnk(i).vert(2,:));
%     V(2,:,i) = 0.1 * thr * (opensnk(i).vert(end,:) - opensnk(i).vert(end-1,:));
% end

%% Interaction potential (Observation from snakes' configuration)
done = false(nsnk,2);
for i = 1:nsnk
    if isempty(opensnk(i).vert)
        disp('snake i is empty')
        continue
    end
    
    for j = i+1:nsnk
        if i == j  || isempty(opensnk(j).vert)
            disp('snake j is empty')
            continue
        end
        if((~any(snkcode(i,:)) || opensnk(i).converge) && (~any(snkcode(j,:)) || opensnk(j).converge))
            disp('both snakes are converge')
            continue
        end
        if ismember(j,[opensnk(i).snkh opensnk(i).snkt]) && ismember(i,[opensnk(j).snkh opensnk(j).snkt])
            disp('already collide')
            continue
        end
        % check collision
        is_collide = false(2,2);
        for k = 1:2
            if snkcode(i,k) && ~opensnk(i).converge
                is_collide(1,k) = SNK_collide(opensnk(i), opensnk(j), k);
                if is_collide(1,k)
                    if k == 1 
                        if ~done(i,k)
                            opensnk(i).collideh = opensnk(i).collideh + 1;
                        end
                        if opensnk(i).collideh > collide_thr
                            disp(['snake ' num2str(i) ' collide at head at snake ' num2str(j)]);
                            opensnk(i).snkh = [opensnk(i).snkh j];
                            snkcode(i,1) = 0;
                        end
                    elseif k == 2
                        if ~done(i,k)
                            opensnk(i).collidet = opensnk(i).collidet + 1;
                        end
                        if opensnk(i).collidet > collide_thr
                            disp(['snake ' num2str(i) ' collide at tail at snake ' num2str(j)])
                            opensnk(i).snkt = [opensnk(i).snkt j];
                            snkcode(i,2) = 0;
                        end
                    end
                    done(i,k) = true;
                end
            else
                if k == 1
                    is_collide(2,k) = ismember(j,opensnk(i).snkh);
                else
                    is_collide(2,k) = ismember(j,opensnk(i).snkt);
                end
            end
            if snkcode(j,k) && ~opensnk(j).converge
                is_collide(2,k) = SNK_collide(opensnk(j), opensnk(i), k);
                if is_collide(2,k)
                    if k == 1
                        if ~done(j,k)
                            opensnk(j).collideh = opensnk(j).collideh + 1;
                        end
                        if opensnk(j).collideh > collide_thr
                            disp(['snake ' num2str(j) ' collide at head at snake ' num2str(i)])
                            opensnk(j).snkh = [opensnk(j).snkh i];
                            snkcode(j,1) = 0;
                        end
                    elseif k == 2
                        if ~done(j,k)
                            opensnk(j).collidet = opensnk(j).collidet + 1;
                        end
                        if opensnk(j).collidet > collide_thr
                            disp(['snake ' num2str(j) ' collide at tail at snake ' num2str(i)])
                            opensnk(j).snkt = [opensnk(j).snkt i];
                            snkcode(j,2) = 0;
                        end
                    end
                    done(j,k) = true;
                end
            else
                if k == 1
                    is_collide(2,k) = ismember(i,opensnk(j).snkh);
                else
                    is_collide(2,k) = ismember(i,opensnk(j).snkt);
                end
            end
        end
        if i == 2 && j == 6
            disp(is_collide)
        end
        % if snakes collide, E_model = 0
        if any(is_collide(:))
            disp(['collision occurs/already occur ' num2str(i) ' and ' num2str(j)])
            continue
        end

        % generate potential map and extract vector from it
        [Vi, Vj] = TP_extract_vector(opensnk, blksize, i, j, fig);
        if Vi == -1
            opensnk(i).snkh = [opensnk(i).snkh j];
            opensnk(j).snkh = [opensnk(j).snkh i];
        end
        V(:,:,i) = V(:,:,i) + Vi;
        V(:,:,j) = V(:,:,j) + Vj;
    end
end

for i = 1:nsnk
    if opensnk(i).converge
        continue
    end
    % reset collide counter if no collision occurs
    if ~done(i,1) && opensnk(i).collideh <= collide_thr
        opensnk(i).collideh = 0;
    end
    if ~done(i,2) && opensnk(i).collidet <= collide_thr
        opensnk(i).collidet = 0;
    end
    
	% normalise the weight and direction
	model(i).confidenceH = min(norm(V(1,:,i))/thr,1);
    if norm(V(1,:,i)) ~= 0
        model(i).predict_directionH = V(1,:,i) ./ norm(V(1,:,i));
    else
        model(i).predict_directionH = V(1,:,i);
    end

    model(i).confidenceT = min(norm(V(2,:,i))/thr,1);
    if norm(V(2,:,i)) ~= 0
        model(i).predict_directionT = V(2,:,i) ./ norm(V(2,:,i));
    else
        model(i).predict_directionT = V(2,:,i);
    end
end
% keyboard;
end

