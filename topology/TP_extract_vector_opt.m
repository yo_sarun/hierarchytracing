function Vi = TP_extract_vector_opt(snk1, snk2, snkcode, blksize)
%TP_EXTRACT_VECTOR extract the attraction model vector
%	Input
%	opensnk		1D array of snake struct
%	blksize		block size i.e. [nRows nCols nDeps]
%	i			index of snake 1
%	j			index of snake 1
%	fig			figure number for debugging
%
%	Output
%	Vi			Attraction model of snake i
%	Vj			Attraction model of snake j
%

Vi = zeros(2,3);
% Vj = zeros(2,3);
    
% % Get options parameters
% defaultoptions = struct('SnakeDebug', 1, 'Figure', 0, 'Debug', false);
% if ~exist('options','var')
%     options = defaultoptions;
% else
%     options = get_options(defaultoptions, options);
% end

%%
for k = 1:2
    if ~snkcode(k)
        continue
    end
%     if options.Debug && (options.SnakeDebug == snk1.label || options.SnakeDebug == snk2.label)
%         stretch_opts.Debug = options.Debug;
%         stretch_opts.Figure = options.Figure;
%         stretch_opts.Angle = [0.866, 0.707, 0.5];
%         offset = zeros(1,3);
%     else
    stretch_opts.Figure = 0;
    if k == 1
        p1 = snk1.vert(1,:);
    else
        p1 = snk1.vert(end,:);
    end
    [offset, blksize] = TP_find_boundary(p1, snk2, blksize );
    stretch_opts.Offset = offset;
%     end

    % get start_points and end_points
    start_points = (p1 - offset)';
    start_points = start_points([2 1 3],:);
    cond_i = any(start_points<1) ...
            | start_points(1,:)>blksize(1) ...
            | start_points(2,:)>blksize(2) ...
            | start_points(3,:)>blksize(3);
    start_points = start_points(:,~cond_i);
    if isempty(start_points)
        continue
    end
    
    end_points = bsxfun(@minus, round(snk2.vert), offset)';
    end_points = end_points([2 1 3],:);
    cond_j = any(end_points<1) ...
            | end_points(1,:)>blksize(1) ...
            | end_points(2,:)>blksize(2) ...
            | end_points(3,:)>blksize(3);
    
    end_points = end_points(:,~cond_j);

    if isempty(end_points)
        continue
    end
    
    % generate potential map
    stretch_i = TP_get_stretch_potential_opt(snk1, blksize, offset);%stretch_opts);
    stretch_j = TP_get_stretch_potential_opt(snk2, blksize, offset);%stretch_opts);
    potential_map = (stretch_i + stretch_j)./2;

    % find path between snake i and j
    fast_march_opts.nb_iter_max = Inf;
    fast_march_opts.end_points = end_points;
    [D,~,Q] = perform_fast_marching(1./potential_map, start_points, fast_march_opts);

%     ind_i = sub2ind(blksize,start_points(1,:),start_points(2,:),start_points(3,:));
    ind_j = sub2ind(blksize,end_points(1,:),end_points(2,:),end_points(3,:));
    [geodist,p_j] = min(D(ind_j));
    closest_points = Q(ind_j);
    p_i = closest_points(p_j);
    
    % check validity of the shortest path
    if ~isfinite(geodist) || geodist == 0
    %     warning('Extract vector: geodist undefined %d and %d', snk1.label, snk2.label)
        continue
    end
    % if geodist == 0
    % %     warning('Extract vector: snake collide %d and %d', snk1.label, snk2.label)
    %     return
    % end

    % get accumulate direction and weight
    direction = (end_points(:,p_j) - start_points(:,p_i))';
    if norm(direction) ~= 0
        direction = direction([2 1 3])./norm(direction);
    end

    E1 = min(0, snk1.E);
    E2 = min(0, snk2.E);
    Wij = max(0, (E1 * E2) / (1+geodist^2)); % Wij must be greater than zero
    if Wij == 0
    %     warning('Wij == 0');
        continue
    end

%     if ind_i(1) == ind_i(p_i)
    Vi(k,:) = Wij .* direction;
%     elseif ind_i(end) == ind_i(p_i)
%         Vi(2,:) = Wij .* direction;
%     end
%     if ind_j(1) == ind_j(p_j)
%         Vj(1,:) = Vj(1,:) + Wij .* -direction;
%     elseif ind_j(end) == ind_j(p_j)
%         Vj(2,:) = Vj(2,:) + Wij .* -direction;
%     end
end

% %% debug
% if options.Debug && (options.SnakeDebug == snk1.label || options.SnakeDebug == snk2.label)
%     Wij
%     Vi
%     Vj
%     [D,S,~] = perform_fast_marching(1./potential_map, start_points(:,p_i), options);
%     gpath = compute_geodesic(D,end_points(:,p_j));
%     if size(gpath,2) == 1
%         geodesic_opts.method = 'discrete';
%         gpath = compute_geodesic(D,end_points(:,p_j), geodesic_opts);
%     end
% 
%     figure(snk2.label+10)
%     plot_fast_marching_3d(D,S,gpath,start_points([2 1 3],:),end_points([2 1 3],:));
% 
%     figure(snk2.label+20)
%     subplot(2,2,1); imagesc(stretch_i(:,:,options.Figure)); colormap gray;
%     subplot(2,2,2); imagesc(stretch_j(:,:,options.Figure)); colormap gray;
%     subplot(2,2,3); imagesc(potential_map(:,:,options.Figure)); colormap gray;
%     subplot(2,2,4);
%     TP_show_path(gpath, start_points, end_points, potential_map(:,:,options.Figure));
%     pause(1);
%     close all;
% end

end

