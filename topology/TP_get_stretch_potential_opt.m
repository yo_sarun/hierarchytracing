function [ result, mask, snkind ] = TP_get_stretch_potential_opt( snk, blksize, offset)%, options )
%TP_GET_POTENTIAL  generate the stretch map of set-of-snake snk
%   input
%	snk			1D array of snake struct
%	blksize		block size i.e. [nRows nCols nDeps]
%	options		utility options
%
%	Output
%	result		stretch map
%	mask		2D binary image where is 1 if angel to snake is less than pi/2
%	snkind		3D array indicate which snake is closest to the voxel
%

% Get options parameters
options = struct('Infinite', 1000, 'Offset', offset ...
        , 'Angle', 0.707, 'Figure', 0, 'Debug', false, 'useMEX', 1);
% if ~exist('options','var')
%     options = defaultoptions;
% else
%     options = get_options(defaultoptions, options);
% end
angle = options.Angle;

%% Initialise output variables and parameters
result = options.Infinite*ones(blksize);
mask = zeros(blksize);
nsnk = size(snk,2);
snkind = zeros(nsnk*2,1);
if isempty(snk) || any(blksize<=0)
    return
end

%% preprocess snake points, evolve directions, and snake index
snkpoints = zeros(nsnk*2,3);
snkdir = zeros(nsnk*2,3);
for n = 1:nsnk
    if snk(n).converge || isempty(snk(n).vert) || size(snk(n).vert,2) < 3
        continue
    end
    % head-end
    snkpoints(2*n-1,:) = snk(n).vert(1,:) - options.Offset;
    if any(snkpoints(2*n-1,:)>0) ...
            && snkpoints(2*n-1,1)<=blksize(2) ...
            && snkpoints(2*n-1,2)<=blksize(1) ...
            && snkpoints(2*n-1,3)<=blksize(3)
        snkpoints(2*n-1,:) = round(snkpoints(2*n-1,:));
        snkdir(2*n-1,:) = snk(n).vert(1,:)-snk(n).vert(2,:);
        snkdir(2*n-1,:) = snkdir(2*n-1,:)./norm(snkdir(2*n-1,:));
        snkind(2*n-1) = 2*snk(n).label-1;
    end
    % tail-end
    snkpoints(2*n,:) = snk(n).vert(end,:) - options.Offset;
    if all(snk(n).vert(end,:)>0) ...
            && snk(n).vert(end,1)<=blksize(2) ...
            && snk(n).vert(end,2)<=blksize(1) ...
            && snk(n).vert(end,3)<=blksize(3)
        snkpoints(2*n,:) = round(snkpoints(2*n,:));
        snkdir(2*n,:) = snk(n).vert(end,:)-snk(n).vert(end-1,:);
        snkdir(2*n,:) = snkdir(2*n,:)./norm(snkdir(2*n,:));
        snkind(2*n) = 2*snk(n).label;
    end
end

% remove short or empty snake
snkpoints(snkind == 0, :) = [];
snkdir(snkind == 0, :) = [];
snkind(snkind == 0) = [];
if isempty(snkind)
    return
end

[x,y,z] = meshgrid(1:blksize(2),1:blksize(1),1:blksize(3));
point = [reshape(x,numel(x),1), reshape(y,numel(y),1), reshape(z,numel(z),1)];

%% Debug: test different value of angle
if options.Debug
    figure
    n = length(angle);
    fig = options.Figure;
    for i = 1:length(angle)
        v1 = bsxfun(@minus, point, round(snk.vert(1,:)));
        r1 = sqrt(sum(v1.^2,2));
        r1(r1 == 0) = 0.01;     % avoid zero division
        v1unit = bsxfun(@rdivide, v1, r1);
        n1 = snk.vert(1,:)-snk.vert(2,:);
        n1 = n1/norm(n1);
        cos_theta1 = v1unit*n1';
        cos_theta1 = cos_theta1-angle(i);
        cos_theta1 = cos_theta1 / (1-angle(i));
        n1mag = (r1.^2)./exp(cos_theta1);
        mask(n1mag > 0) = 1;
        n1mag(n1mag < 0) = 0;
        n1mag(cos_theta1<=0) = options.Infinite;

        vN = bsxfun(@minus, point, round(snk.vert(end,:)));
        rN = sqrt(sum(vN.^2,2));
        rN(rN == 0) = 0.01;     % avoid zero division
        vNunit = bsxfun(@rdivide, vN, rN);
        nN = snk.vert(end,:)-snk.vert(end-1,:);
        nN = nN/norm(nN);
        cos_thetaN = vNunit*nN';
        cos_thetaN = cos_thetaN-angle(i);
        cos_thetaN = cos_thetaN / (1-angle(i));
        nNmag = (rN.^2)./exp(cos_thetaN);
        mask(nNmag > 0 & nNmag > n1mag) = 2;
        nNmag(nNmag < 0) = 0;
        nNmag(cos_thetaN<=0) = options.Infinite;
        % reshape to block size
        result = reshape(n1mag,blksize);
        result(nNmag < n1mag) = nNmag(nNmag < n1mag);
        
        n1mag = reshape(n1mag, blksize);
        n1mag(cos_theta1<=0) = max(n1mag(:));
        subplot(3,n,i); imagesc(n1mag(:,:,fig)); colormap gray;
        start_points = snk.vert';
        hold on;
        plot(start_points(1,:),start_points(2,:),'-o','Color','b');
        hold off;
        title('head-end')
        nNmag = reshape(nNmag, blksize);
        nNmag(cos_thetaN<=0) = max(nNmag(:));
        subplot(3,n,n+i); imagesc(nNmag(:,:,fig)); colormap gray;
        hold on;
        plot(start_points(1,:),start_points(2,:),'-o','Color','b');
        hold off;
        title('tail-end')
        subplot(3,n,2*n+i); imagesc(result(:,:,fig)); colormap gray;
        hold on;
        plot(start_points(1,:),start_points(2,:),'-o','Color','b');
        hold off;
        title(['stretch map, Angle = ' num2str(angle(i))])
    end
    angle = defaultoptions.Angle;
end


if options.useMEX
    result = TP_get_stretch_potential_opt_c(snkpoints, snkdir, point, angle, int32(blksize), options.Infinite);
else
    % generate stretch map
    for i = 1:size(snkpoints,1)
        v = bsxfun(@minus, point, snkpoints(i,:));
        r = sqrt(sum(v.^2,2));
        v_unit = bsxfun(@rdivide, v, r);
        cos_theta = v_unit*snkdir(i,:)';
        cos_theta = cos_theta - angle;
        cos_theta = cos_theta / (1-angle);
    %     mag = cos_theta./exp(r./sigma);
        mag = (r.^2)./exp(cos_theta);
        mag(cos_theta<=0) = options.Infinite;

        mask(result(:) > mag) = snkind(i);
        result(result(:) > mag) = mag(result(:) > mag);
    end
end
end

