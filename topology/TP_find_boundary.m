function [ offset, s ] = TP_find_boundary( p1, snk2, blksize, gap )
%TP_FIND_BOUNDARY find offset and intersect area of snk1 and snk2
% 
%     Inputs
%     snk1        snake 1
%     snk2        snake 2
%     blksize     block size
%     gap         gap aroudn snake 1 and snake 2
%     
%     Outputs
%     offset      offset for converting to intersect area coordinate
%     s           size of intersect area
%

% if isempty(snk2)
%     return
% end

if nargin < 4
    gap = 20;
end

% offset = 0;
% s = blksize;
point1 = round(p1);
points2 = round(vertcat(snk2.vert));
% if isempty(points2)
%     return
% end

% keyboard;
[pmin,pmax] = TP_find_boundary_c(point1, points2, gap, blksize);

% p1min = max([min([points1; inf(1,3)])-gap;ones(1,3)]);
% p1max = min([max([points1; -inf(1,3)])+gap;blksize([2 1 3])]);
% p1min = max([point1-gap;ones(1,3)]);
% p1max = min([point1+gap;blksize([2 1 3])]);
% p2min = max([min([points2; inf(1,3)])-gap;ones(1,3)]);
% p2max = min([max([points2; -inf(1,3)])+gap;blksize([2 1 3])]);
% 
% pmin = max(p1min,p2min);
% pmax = min(p1max,p2max);

% if (sum(abs(pmin-pmin2)) + sum(abs(pmax-pmax2))) > 6
%     keyboard;
% %     error('wrong TP_find_boundary')
% end

offset = pmin - ones(1,3);
s = pmax([2 1 3])-pmin([2 1 3]) + ones(1,3);

end

