function [ result, mask ] = TP_get_stretch_potential( snk, blksize, options )
%TP_GET_POTENTIAL generate the stretch map of snk
%   input
%	snk			a snake struct
%	blksize		block size i.e. [nRows nCols nDeps]
%	options		utility options
%
%	Output
%	result		stretch map
%	mask		2D binary image where is 1 if angel to snake is less than pi/2
%

% Get options parameters
defaultoptions = struct('Infinite', 1000, 'Angle', 0.707, 'Figure', 0);
if ~exist('options','var')
    options = defaultoptions;
else
    options = get_options(defaultoptions, options);
end
infinite = options.Infinite;
angle = options.Angle;
fig = options.Figure;

%% check input snk and generate map
mask = zeros(blksize);
if size(snk.vert,2) < 3
    result = zeros(blksize);
    return
end
[x,y,z] = meshgrid(1:blksize(2),1:blksize(1),1:blksize(3));
point = [reshape(x,numel(x),1), reshape(y,numel(y),1), reshape(z,numel(z),1)];

%% Test different value of angle
if fig ~= 0
    figure
    n = length(angle);
    for i = 1:length(angle)
        v1 = bsxfun(@minus, point, round(snk.vert(1,:)));
        r1 = sqrt(sum(v1.^2,2));
        r1(r1 == 0) = 0.01;     % avoid zero division
        v1unit = bsxfun(@rdivide, v1, r1);
        n1 = snk.vert(1,:)-snk.vert(2,:);
        n1 = n1/norm(n1);
        cos_theta1 = v1unit*n1';
        cos_theta1 = cos_theta1-angle(i);
        cos_theta1 = cos_theta1 / (1-angle(i));
        n1mag = (r1.^2)./exp(cos_theta1);
        mask(n1mag > 0) = 1;
        n1mag(n1mag < 0) = 0;
        n1mag(cos_theta1<=0) = infinite;

        vN = bsxfun(@minus, point, round(snk.vert(end,:)));
        rN = sqrt(sum(vN.^2,2));
        rN(rN == 0) = 0.01;     % avoid zero division
        vNunit = bsxfun(@rdivide, vN, rN);
        nN = snk.vert(end,:)-snk.vert(end-1,:);
        nN = nN/norm(nN);
        cos_thetaN = vNunit*nN';
        cos_thetaN = cos_thetaN-angle(i);
        cos_thetaN = cos_thetaN / (1-angle(i));
        nNmag = (rN.^2)./exp(cos_thetaN);
        mask(nNmag > 0 & nNmag > n1mag) = 2;
        nNmag(nNmag < 0) = 0;
        nNmag(cos_thetaN<=0) = infinite;
        % reshape to block size
        result = reshape(n1mag,blksize);
        result(nNmag < n1mag) = nNmag(nNmag < n1mag);
        
        n1mag = reshape(n1mag, blksize);
        n1mag(cos_theta1<=0) = max(n1mag(:));
        subplot(3,n,i); imagesc(n1mag(:,:,fig)); colormap gray;
        start_points = snk.vert';
        hold on;
        plot(start_points(1,:),start_points(2,:),'-o','Color','b');
        hold off;
        title('head-end')
        nNmag = reshape(nNmag, blksize);
        nNmag(cos_thetaN<=0) = max(nNmag(:));
        subplot(3,n,n+i); imagesc(nNmag(:,:,fig)); colormap gray;
        hold on;
        plot(start_points(1,:),start_points(2,:),'-o','Color','b');
        hold off;
        title('tail-end')
        subplot(3,n,2*n+i); imagesc(result(:,:,fig)); colormap gray;
        hold on;
        plot(start_points(1,:),start_points(2,:),'-o','Color','b');
        hold off;
        title(['stretch map, Angle = ' num2str(angle(i))])
    end
    angle = defaultoptions.Angle;
end


%% generate potential for head end
v1 = bsxfun(@minus, point, round(snk.vert(1,:)));
r1 = sqrt(sum(v1.^2,2));
r1(r1 == 0) = 0.01;     % avoid zero division
v1unit = bsxfun(@rdivide, v1, r1);
n1 = snk.vert(1,:)-snk.vert(2,:);
n1 = n1/norm(n1);
cos_theta1 = v1unit*n1';
cos_theta1 = cos_theta1-angle;
cos_theta1 = cos_theta1 / (1-angle);
% cos_theta1 = max(cos_theta1,thr);
% n1mag = cos_theta1./exp(r1./sigma);
n1mag = (r1.^2)./exp(cos_theta1);
mask(n1mag > 0) = 1;
n1mag(n1mag < 0) = 0;
n1mag(cos_theta1<=0) = infinite;

%% generate potential for tail end
vN = bsxfun(@minus, point, round(snk.vert(end,:)));
rN = sqrt(sum(vN.^2,2));
rN(rN == 0) = 0.01;     % avoid zero division
vNunit = bsxfun(@rdivide, vN, rN);
nN = snk.vert(end,:)-snk.vert(end-1,:);
nN = nN/norm(nN);
cos_thetaN = vNunit*nN';
cos_thetaN = cos_thetaN-angle;
cos_thetaN = cos_thetaN / (1-angle);
% cos_thetaN = max(cos_thetaN,thr);
% nNmag = cos_thetaN./exp(rN./sigma);
nNmag = (rN.^2)./exp(cos_thetaN);
mask(nNmag > 0 & nNmag > n1mag) = 2;
nNmag(nNmag < 0) = 0;
nNmag(cos_thetaN<=0) = infinite;
% reshape to block size
result = reshape(n1mag,blksize);
result(nNmag < n1mag) = nNmag(nNmag < n1mag);

end

