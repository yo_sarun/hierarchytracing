function Vi = TP_extract_vector_set(snk1, snk2, snkcode, C, blksize, options)
%TP_EXTRACT_VECTOR extract the attraction model vector of set-of-snakes
%	Input
%	snk1		snake struct
%	snk2		1D array of snake struct
%	C			segment number of snk2
%	blksize		block size i.e. [nRows nCols nDeps]
%	i			index of snake 1
%	j			index of snake 1
%	options		utility options
%
%	Output
%	Vi			Attraction model of snake 1
%

Vi = zeros(2,3);

% if~ismember(nargin,4:5)
%     error('Invalid inputs to TP_extract_vector_opt!')    
% end

% Get options parameters
defaultoptions = struct('SnakeDebug', 1, 'Figure', 1, ...
        'Segment', false', 'Debug', false, 'Optimize', true);
if ~exist('options','var')
    options = defaultoptions;
else
    options = get_options(defaultoptions, options);
end

%%
offset = zeros(1,3);

%% generate potential map
for k = 1:2
    if ~snkcode(k)
        continue
    end
    if options.Optimize
        if k == 1
            p1 = snk1.vert(1,:);
        else
            p1 = snk1.vert(end,:);
        end
%         [ offset, s ] = TP_find_boundary( snk1, snk2, blksize );
        [offset, blksize] = TP_find_boundary(p1, snk2, blksize );
        stretch_opts.Offset = offset;
%         blksize = s;
    end

    % get start_points and end_points
    start_points = bsxfun(@minus, round(vertcat(snk1.vert)), offset)';
    start_points = start_points([2 1 3],:);
    if options.Optimize
        cond_i = any(start_points<1) ...
                | start_points(1,:)>blksize(1) ...
                | start_points(2,:)>blksize(2) ...
                | start_points(3,:)>blksize(3);
        start_points = start_points(:,~cond_i);
    end
    if isempty(start_points)
        continue
    end
    end_points = bsxfun(@minus, round(vertcat(snk2.vert)), offset)';
    end_points = end_points([2 1 3],:);
    if options.Optimize
        cond_j = any(end_points<1) ...
                | end_points(1,:)>blksize(1) ...
                | end_points(2,:)>blksize(2) ...
                | end_points(3,:)>blksize(3);
        
        end_points = end_points(:,~cond_j);
    end
    if isempty(end_points)
        continue
    end
    
    stretch_opts.Debug = false;
    stretch_i = TP_get_stretch_potential_opt(snk1, blksize, offset);%stretch_opts);
    stretch_j = zeros(blksize);
    num = 0;
    for c = min(C):max(C)
        if isempty(snk2(C==c))
            continue
        end
        stretch_tmp = TP_get_stretch_potential_opt(snk2(C==c), blksize, offset);%stretch_opts);
        stretch_j = stretch_j + stretch_tmp;
        num = num + 1;
    end
    stretch_j = stretch_j ./ num;
    potential_map = (stretch_i + stretch_j)./2;

    % find path between snake i and j
    fast_march_opts.nb_iter_max = Inf;
%     if ~options.Segment
    fast_march_opts.end_points = end_points;
%     end
    [D,~,Q] = perform_fast_marching(1./potential_map, start_points, fast_march_opts);

%     ind_i = sub2ind(blksize,start_points(1,:),start_points(2,:),start_points(3,:));
    ind_j = sub2ind(blksize,end_points(1,:),end_points(2,:),end_points(3,:));

%     if options.Segment
%         Ni = size(snk1.vert, 1);
%         for c = min(C):max(C)
%             if isempty(snk2(C==c))
%                 continue
%             end
%             points = bsxfun(@minus, round(vertcat(snk2(C==c).vert)), offset)';
%             points = points([2 1 3],:);
% 
%             cond = any(points<1) ...
%                     | points(1,:)>blksize(1) ...
%                     | points(2,:)>blksize(2) ...
%                     | points(3,:)>blksize(3);
%             points = points(:,~cond);
% 
%             ind = sub2ind(blksize,points(1,:),points(2,:),points(3,:));
%             [geodist,p] = min(D(ind));
%             closest_points = Q(ind);
%             P_i = closest_points(p);
% 
%             direction = (points(:,p) - start_points(:,P_i))';
%             if norm(direction) ~= 0
%                 direction = direction([2 1 3])./norm(direction);
%             end
%             if isempty(geodist)
%                 continue
%             end
%             E1 = min(0, snk1.E);
%             E2 = min(0, sum([snk2(C==c).E]));
%             Wij = max(0, (E1 * E2) / (1+geodist^2)); % Wij must be greater than zero
%             % Wij = snk1.E * sum([snk2(C==C(gr)).E]) / (1+geodist^2);
%             % Wij = max(0, Wij); % Wij must be greater than zero
%             if Wij == 0
%     %             warning('Wij == 0');
%             end
% 
%             if P_i == 1
%                 Vi(1,:) = Vi(1,:) + Wij .* direction;
%             elseif P_i == Ni
%                 Vi(2,:) = Vi(2,:) + Wij .* direction;
%             end
%             if options.Debug,
%                 Wij
%                 Vi
%                 [D,S,~] = perform_fast_marching(1./potential_map, start_points(:,P_i), options);
%                 gpath = compute_geodesic(D,points(:,p));
%                 if size(gpath,2) == 1
%                     geodesic_opts.method = 'discrete';
%                     gpath = compute_geodesic(D,points(:,p), geodesic_opts);
%                 end
%                 C
%                 figure(c+10)
%                 plot_fast_marching_3d(D,S,gpath,start_points([2 1 3],:),end_points([2 1 3],:));
% 
%                 figure(c+20)
%                 subplot(2,2,1); imagesc(stretch_i(:,:,options.Figure)); colormap gray;
%                 title('stretch i');
%                 subplot(2,2,2); imagesc(stretch_j(:,:,options.Figure)); colormap gray;
%                 title('stretch j');
%                 subplot(2,2,3); imagesc(potential_map(:,:,options.Figure)); colormap gray;
%                 title('potential map');
%                 subplot(2,2,4);
%                 TP_show_path(gpath, start_points, end_points, potential_map(:,:,options.Figure));
%                 pause(1);
%     %             keyboard;
%                 close(c+10); close(c+20);
%             end
%         end
%         return
%     end

    [geodist,p_j] = min(D(ind_j));
    closest_points = Q(ind_j);
    p_i = closest_points(p_j);

    % check validity of the shortest path
    if ~isfinite(geodist) || geodist == 0
    %     warning('geodist is zero, no segment');
        continue
    end

%     if geodist == 0
%     %     warning('geodist is zero, no segment');
%         return
%     end

    % get accumulate direction and weight
    nC = max(length(unique(C)),1);

    direction = (end_points(:,p_j) - start_points(:,p_i))';
    if norm(direction) ~= 0
        direction = direction([2 1 3])./norm(direction);
    end

    E1 = min(0, snk1.E);
    E2 = min(0, sum([snk2.E])/nC);
    % E2 = min(0, sum([snk2(C==C(gr)).E]));
    Wij = max(0, (E1 * E2) / (1+geodist^2)); % Wij must be greater than zero

    if Wij == 0
    %     warning('Wij == 0, no segment');
        continue
    end

%     if ind_i(1) == ind_i(p_i)
    Vi(k,:) = Wij .* direction;
%     elseif ind_i(end) == ind_i(p_i)
%         Vi(2,:) = Wij .* direction;
%     end
end

% %% debug
% if options.Debug,
%     Wij
%     Vi
%     [D,S,~] = perform_fast_marching(1./potential_map, start_points(:,p_i), options);
%     gpath = compute_geodesic(D,end_points(:,p_j));
%     if size(gpath,2) == 1
%         geodesic_opts.method = 'discrete';
%         gpath = compute_geodesic(D,end_points(:,p_j), geodesic_opts);
%     end
%     C
%     figure(j+10)
%     plot_fast_marching_3d(D,S,gpath,start_points([2 1 3],:),end_points([2 1 3],:));
% 
%     figure(j+20)
%     subplot(2,2,1); imagesc(stretch_i(:,:,options.Figure)); colormap gray;
%     title('stretch i');
%     subplot(2,2,2); imagesc(stretch_j(:,:,options.Figure)); colormap gray;
%     title('stretch j');
%     subplot(2,2,3); imagesc(potential_map(:,:,options.Figure)); colormap gray;
%     title('potential map');
%     subplot(2,2,4);
%     TP_show_path(gpath, start_points, end_points, potential_map(:,:,options.Figure));
%     pause(1);
%     keyboard;
%     close(j+10); close(j+20);
% end

end

