function [ V, snk1, done ] = TP_model_set( snk1, snks, snkcode, blksize, C, level, done, options )
%TP_MODEL_SET compute attraction model between snk1 and snks
%   input
%	snk1		a snake struct
%	snks		1D array of snake struct
%	snkcode		binary 2D array telling which snake-end converges
%	blksize		block size i.e. [nRows nCols nDeps]
%	C			snake segment number
%	level		resolution scale
%	done		boolean telling that snake converges or not
%	options		utility options
%
%	Output
%	V			attraction model
%	snk1		updated snake struct
%	done		updated boolean indicating convergence of snake
%

V = zeros(2,3);
if snk1.converge || isempty(snk1.vert)
    return
end

% Get options parameters
defaultoptions = struct('Collide_thr', 6, 'Debug', false, 'C_SNAKE', 0, ...
        'Model', true);

if ~exist('options','var')
    options = defaultoptions;
else
    options = get_options(defaultoptions, options);
end

collide_thr = options.Collide_thr;
C_snk1 = options.C_SNAKE;

%%
grids = arrayfun(@(x) x.grids(level), snks);
% compute snake model
for j = min(grids):max(grids)
%     keyboard;
    if isempty(vertcat(snks(grids == j).vert))
        continue
    end
    if snk1.grids(level) == j
        continue
    end
    % collision detection
    if ~is_inside_boundary( snk1, snks, blksize )
        continue
    end

    if snkcode(1)
        [is_collide, snk_j] = SNK_collide(snk1, snks(grids == j), 1);
        if is_collide
            if ~done(1)
                snk1.collideh = snk1.collideh + 2;
            end
            if snk1.collideh >= collide_thr
                snk1.snkh = [snk1.snkh snk_j];
            end
            done(1) = true;
        end
    else
        is_collide = any(arrayfun(@(x)(any(x.label==snk1.snkh)), snks(grids==j)));
    end

    if is_collide
        continue
    end

    if snkcode(2)
        [is_collide, snk_j] = SNK_collide(snk1, snks(grids == j), 2);
        if is_collide
            if ~done(2)
                snk1.collidet = snk1.collidet + 2;
            end
            if snk1.collidet >= collide_thr
                snk1.snkt = [snk1.snkt snk_j];
            end
            done(2) = true;
        end
    else
        is_collide = any(arrayfun(@(x)(any(x.label==snk1.snkt)), snks(grids==j)));
    end

    % if snakes collide, E_model = 0
    % or if snakes are from the same segment, no attraction model
    if is_collide || ~options.Model || (~isempty(C) &&  any(C_snk1 == C(grids==j)))
        continue
    end

    % find the interaction potential
    extract_set_opts.Debug = options.Debug;
    Vi = TP_extract_vector_set(snk1, snks(grids == j & C ~= C_snk1), snkcode, C(grids == j & C ~= C_snk1), blksize, extract_set_opts);

    V = V + Vi;
end

end

