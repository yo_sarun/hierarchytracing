#include <math.h>
#include "mex.h" /* Always include this */

double round(double val) {
    return floor(val+0.5);
}

void mexFunction(int nlhs, mxArray *plhs[], /* Output variables */
        int nrhs, const mxArray *prhs[]) /* Input variables */
{
    int i, k, point2M;
    int idx[3] = {1, 0, 2};
    double gap, minval, maxval, tmp;
    double *point1, *point2, *blksize, *pmin, *pmax;
    
    // Check for proper number of input and output arguments.
    if (nrhs != 4)
        mexErrMsgTxt("Invalid input arguments!");
    if (nlhs != 2)
        mexErrMsgTxt("Invalid output arguments!");
    
    // read input
    point1 = mxGetPr(prhs[0]);
    point2 = mxGetPr(prhs[1]);
    point2M = mxGetM(prhs[1]); /* Get # of row of point2 */
    gap = *mxGetPr(prhs[2]);
    blksize = mxGetPr(prhs[3]);
 
    // init output
    plhs[0] = mxCreateDoubleMatrix(1, 3, mxREAL);
    pmin = mxGetPr(plhs[0]);
    plhs[1] = mxCreateDoubleMatrix(1, 3, mxREAL);
    pmax = mxGetPr(plhs[1]);
    
    // find pmin, pmax
    for (k=0; k<3; k++) {
        minval = point2[point2M*k];
        for (i=2; i<point2M; i+=2) {
            if (point2[i + point2M*k] < minval) {
                minval = point2[i + point2M*k];
            }
        }
        if (point2M%2 != 0) {
            if (point2[point2M-1 + point2M*k] < minval) {
                minval = point2[point2M-1 + point2M*k];
            }
        }
        
        if (point1[k] > minval) {
            minval = point1[k];
        }
        
        minval = round(minval - gap);
        
        if (minval < 1)
            pmin[k] = 1;
        else
            pmin[k] = minval;
    }
    
    for (k=0; k<3; k++) {
        maxval = point2[point2M*k];
        for (i=2; i<point2M; i+=2) {
            if (point2[i + point2M*k] > maxval) {
                maxval = point2[i + point2M*k];
            }
        }
        if (point2M%2 != 0) {
            if (point2[point2M-1 + point2M*k] > maxval) {
                maxval = point2[point2M-1 + point2M*k];
            }
        }
        
        if (point1[k] < maxval) {
            maxval = point1[k];
        }
        
        maxval = round(maxval + gap);
        
//         if (k==0)
//             tmp = blksize[1];
//         else if (k==1)
//             tmp = blksize[0];
//         else
//             tmp = blksize[2];
        
        if (maxval > blksize[idx[k]])
            pmax[k] = blksize[idx[k]];
        else
            pmax[k] = maxval;
        
    }
    return;
}