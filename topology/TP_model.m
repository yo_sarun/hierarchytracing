function [ V, snk1, done ] = TP_model( snk1, snks, snkcode, blksize, C, done, options )
%TP_MODEL compute attraction model between snk1 and snks
%   input
%	snk1		a snake struct
%	snks		1D array of snake struct
%	snkcode		binary 2D array telling which snake-end converges
%	blksize		block size i.e. [nRows nCols nDeps]
%	C			snake segment number
%	done		boolean telling that snake converges or not
%	options		utility options
%
%	Output
%	V			attraction model
%	snk1		updated snake struct
%	done		updated boolean indicating convergence of snake
%

V = zeros(2,3);
if snk1.converge || isempty(snk1.vert)
    return
end

%% Get options parameters
defaultoptions = struct('Collide_thr', 6, 'Debug', false, 'C_SNAKE', 0, ...
        'Model', true);

if ~exist('options','var')
    options = defaultoptions;
else
    options = get_options(defaultoptions, options);
end

collide_thr = options.Collide_thr;
C_snk1 = options.C_SNAKE;

%%
nsnk = size(snks,2);
for j = 1:nsnk
    if snk1.label == snks(j).label  || isempty(snks(j).vert)
        continue
    end
    
    %%
    if snkcode(1)
        is_collide = SNK_collide(snk1, snks(j), 1);
        if is_collide
            if ~done(1)
                snk1.collideh = snk1.collideh + 2;
            end
            if snk1.collideh >= collide_thr
%                 disp(['snake ' num2str(snk1.label) ' collide at head at snake ' num2str(j)]);
                snk1.snkh = [snk1.snkh snks(j).label];
            end
            done(1) = true;
        end
    else
        is_collide = any(snks(j).label == snk1.snkh);
    end
    
    if is_collide
        continue
    end
    
    if snkcode(2)
        is_collide = SNK_collide(snk1, snks(j), 2);
        if is_collide
            if ~done(2)
                snk1.collidet = snk1.collidet + 2;
            end
            if snk1.collidet >= collide_thr
%                 disp(['snake ' num2str(snk1.label) ' collide at tail at snake ' num2str(j)])
                snk1.snkt = [snk1.snkt snks(j).label];
            end
            done(2) = true;
        end
    else
        is_collide = any(snks(j).label == snk1.snkt);
    end
    
%     for k = 1:2
%         if snkcode(k)
%             is_collide(k) = SNK_collide(snk1, snks(j), k);
%             if is_collide(k)
%                 if k == 1 
%                     if ~done(k)
%                         snk1.collideh = snk1.collideh + 2;
%                     end
%                     if snk1.collideh > collide_thr
% %                         disp(['snake ' num2str(snk1.label) ' collide at head at snake ' num2str(j)]);
%                         snk1.snkh = [snk1.snkh snks(j).label];
%                     end
%                 elseif k == 2
%                     if ~done(k)
%                         snk1.collidet = snk1.collidet + 2;
%                     end
%                     if snk1.collidet > collide_thr
% %                         disp(['snake ' num2str(snk1.label) ' collide at tail at snake ' num2str(j)])
%                         snk1.snkt = [snk1.snkt snks(j).label];
%                     end
%                 end
%                 done(k) = true;
%             end
%         else
%             if k == 1
%                 is_collide(k) = ismember(snks(j).label, snk1.snkh);
%             else
%                 is_collide(k) = ismember(snks(j).label, snk1.snkt);
%             end
%         end
%     end
    
    % if snakes collide, E_model = 0
    % or if snakes are in the same segment, ignore
    if ~options.Model || is_collide || (~isempty(C) && C(j)==C_snk1)
        continue
    end

    % generate potential map and extract vector from it
    Vi = TP_extract_vector_opt(snk1, snks(j), snkcode, blksize);
    V = V + Vi;

end

end

