#include <math.h>
#include "mex.h" /* Always include this */
void mexFunction(int nlhs, mxArray *plhs[], /* Output variables */
        int nrhs, const mxArray *prhs[]) /* Input variables */
{
    int i, j, k;
//     int N;
    int snkpointsM, snkpointsN, pointM, pointN;
    int *blksize;
    double angle, infval, r, cos_theta, mag;
    double *snkpoints, *snkdir, *point, *result;
    double *v;
    
    // Check for proper number of input and output arguments.
    if (nrhs != 6)
        mexErrMsgTxt("Invalid input arguments!");
    if (nlhs != 1)
        mexErrMsgTxt("Invalid output arguments!");
    
    // read input
    snkpoints = mxGetPr(prhs[0]);
    snkpointsM = mxGetM(prhs[0]); /* Get # of row of snkpoints */
    snkpointsN = mxGetN(prhs[0]); /* Get # of column of snkpoints */
    snkdir = mxGetPr(prhs[1]);
    point = mxGetPr(prhs[2]);
    pointM = mxGetM(prhs[2]); /* Get # of row of points */
    pointN = mxGetN(prhs[2]); /* Get # of column of points */
    angle = *mxGetPr(prhs[3]);
//     N = mxGetN(prhs[4]);
    blksize = (int*)mxGetData(prhs[4]);
    infval = *mxGetPr(prhs[5]);
    
//     for (i=0;i<N;i++)
//         mexPrintf("blksize %d\n", blksize[i]);
    
    // init output
//     plhs[0] = mxCreateDoubleMatrix(pointM, 1, mxREAL); /* Create the output matrix */
//     result = mxGetPr(plhs[0]); /* Get the pointer to the data of B */
    plhs[0] = mxCreateNumericArray(snkpointsN, blksize, mxDOUBLE_CLASS, mxREAL);
    result = (double*)mxGetData(plhs[0]);
    
    // generate stretch map
    v = (double *)mxMalloc(sizeof(double)*3);
//     r = (double *)mxMalloc(sizeof(double)*pointM);
//     cos_theta = (double *)mxMalloc(sizeof(double)*pointM);
//     mag = (double *)mxMalloc(sizeof(double)*pointM);
    for (i=0; i<snkpointsM; i++) {
        for (j=0; j<pointM; j++) {
            r = 0;
            for (k=0; k<3; k++) {
                v[k] = point[j + pointM*k] - snkpoints[i + snkpointsM*k];
                r += v[k]*v[k];
            }
            r = sqrt(r);
            
            cos_theta = 0;
            if (r != 0) {
                for (k=0; k<3; k++) {
                    v[k] = v[k] / r;
                    cos_theta = cos_theta + (v[k]*snkdir[i + snkpointsM*k]);
                }
            }
            cos_theta = (cos_theta-angle)/(1-angle);
            
            mag = (r*r)/exp(cos_theta);
            
            if (cos_theta <= 0)
                mag = infval;

            if (i==0 || result[j] > mag)
                result[j] = mag;
        }
    }
    
    //free memory
    mxFree(v);
//     mxFree(r);
//     mxFree(cos_theta);
//     mxFree(mag);
    return;
}