function [imgdir, savefile, ptitles, myCallback] = preprocessVascu2(handles)
% Preprocessing step for vascusynth dataset

addpath('./AMT');
addpath('./CSF');
addpath('./utils');
addpath('./PreprocessLib');
addpath('./oof3response');
addpath(genpath('./CurveLab-2.1.3'));
addpath(genpath('./lib'));

ptitles = cell(6,1);
myCallback = cell(6,1);

%% Read Image
handles.jTextArea.append(['Reading images ...' char(10)]);
handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength);

info = metaImageInfo([handles.pathname handles.filename]);
V = metaImageRead(info);
FNUM = size(V,3);

ptitle = 'Original';
imshow_gui(V, handles.axes1, 1);
ptitles{1} = ptitle;
myCallback{1} = {@imshow_gui, V, handles.axes1};

%% parameters
sigCurv=20;

S_csf = 2;

BG_median_thr = 30;

cellsize = [ceil(size(V,1)/32) ceil(size(V,2)/32)];
blksize = [32 32 FNUM];

thr_val = 50;
region_size = 200;

r_oof = 6;
opts.responsetype=5;
OOF_thr = 3;


%% Gaussian smooth
handles.jTextArea.append(['Gaussian Smooth started...' char(10)]);
h = fspecial('gaussian',[5 5],5);
V_gau = imfilter(V, h);
ptitle = 'Gaussian Smooth';
imshow_gui(V_gau, handles.axes1, 1);
ptitles{2} = ptitle;
myCallback{2} = {@imshow_gui, V_gau, handles.axes1};

%% LoG
handles.jTextArea.append(['Laplacian of Gaussian started...' char(10)]);
pause(1/8);
V_csf = CSF_cos(uint8(V_gau),S_csf);
ptitle = 'LoG';
imshow_gui(V_csf, handles.axes1, 1);
ptitles{3} = ptitle;
myCallback{3} = {@imshow_gui, V_csf, handles.axes1};

%% adjust intensity
handles.jTextArea.append(['Adjust Intensity started...' char(10)]);
V_adj = zeros(size(V));
for fcount = 1:FNUM
    V_adj(:,:,fcount) = imadjust(V_csf(:,:,fcount),[0,0.1],[0,1]);
end
ptitle = 'Image Adjust';
imshow_gui(V_adj, handles.axes1, 1);
ptitles{4} = ptitle;
myCallback{4} = {@imshow_gui, V_adj, handles.axes1};

ptitle = 'Intensity Threshold';
imshow_gui(V_adj>thr_val, handles.axes1, 1);
ptitles{5} = ptitle;
myCallback{5} = {@imshow_gui, V_adj>thr_val, handles.axes1};

%% detect seed points by ridge points
handles.jTextArea.append(['Seed Points Detection started...' char(10)]);
pause(1/8);
[fx,fy,~] = AM_gradient(V_csf);
seedsx = fx(:,1:end-1,:)>0&fx(:,2:end,:)<0&V_adj(:,1:end-1,:) > thr_val;
seedsy = fy(1:end-1,:,:)>0&fy(2:end,:,:)<0&V_adj(1:end-1,:,:) > thr_val;
seeds = seedsx(1:end-1,:,:)&seedsy(:,1:end-1,:);
seeds = padarray(seeds,[1 1],'symmetric','post');

%% Save filtered image and seed points
% get name of save file
savefile = 'vascusynth.mat';

smooth = single(V_adj);
sample_factor = 1;

imgdir = handles.imgdir;
if mkdir(imgdir)
    save ([imgdir savefile], 'V', 'smooth', 'seeds', 'blksize', 'cellsize', 'sample_factor', '-v7.3');
else
    error('ERROR: cannot save preprocessed image stack');
end

%% Show seed points
ptitle = 'Seeds';
imshowoverlay_gui(smooth, seeds, handles.axes1, 1);
ptitles{6} = ptitle;
myCallback{6} = {@imshowoverlay_gui, smooth, seeds, handles.axes1};
handles.jTextArea.append(['Save: ' imgdir savefile char(10)]);
handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength);
end