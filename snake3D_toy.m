% Test tracing snake at voxel level for toy image
% This code is modified from Vector field convolution (VFC) by Bing Li 2005 - 2009.

clear all
close all
clc

addpath('./AMT')
addpath('./frangi/')
addpath('./utils')
addpath('./DRF')
addpath('./snake')
addpath('./block')
addpath('./topology')
addpath('./ba_interp3')
addpath('./oof3response')
addpath(genpath('./toolbox_fast_marching'))
addpath(genpath('./UGM'))

disp('======================================')
disp('Open Snake Curve with DRF at Pixel level')

load 'paramsDRF2.mat'
load 'toy.mat'
%% Get grid image
U = smooth;
seed = seeds;

figure(1);
imshow3D(U);

%% compute GVF and its energy
disp('--------------------------------------------------')
disp('Computing GVF external force field ...')
mu = .2;
GVF_ITER = 10;
normalize = 1;
tic;
U = double(U);
h = fspecial('gaussian',[5 5],5);
f = imfilter(double(U),h);
Fexts = AM_GVF(f, mu, GVF_ITER, 1);    % for toy image
Fext = cell(1,3);
for i = 1:3
    Fext{i} = double(Fexts(:,:,:,i));
end
toc;

%% Train DRF
disp('--------------------------------------------------')
disp('Generate DRF graphical model')
tic;
edgeStruct = BLK_getEdgeStruct(size(U));
toc;

%% Evolve snake
options.FrangiScaleRange = [1,8];
options.Iter_thr = 7;
options.Fcount = fcount;
tic;
[ opensnk, tube, snkcode, S, C ] = find_snake_topo( U, seed, Fext, edgeStruct, result, options );
toc;