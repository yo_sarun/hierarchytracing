function [imgdir, savefile, ptitles, myCallback] = preprocessKim(handles)
% Preprocessing step for OP dataset from DIADEM Challenge
addpath('./AMT');
addpath('./CSF');
addpath('./utils');
addpath('./PreprocessLib');
addpath('./oof3response');
addpath(genpath('./CurveLab-2.1.3'));

ptitles = cell(8,1);
myCallback = cell(8,1);

%% Read Image
file_extension = handles.file_extension;
im_name = [handles.pathname handles.filename];
FNUM = 1;

X = imread(im_name, file_extension);
if ndims(X) == 3
    X = rgb2gray(X(:,:,1:3));
end
X = single(X);
X = X-min(X(:)); X = X/max(X(:)); X = uint8(round(X*255.0));
V(:,:,FNUM)=X;

FNUM = 2;
V(:,:,2) = X;

if ~handles.script
    handles.jTextArea.append(['Reading images finished...' char(10)]);
    handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength);
    ptitle = 'Original';
    imshow_gui(V, handles.axes1, 1);
    ptitles{1} = ptitle;
    myCallback{1} = {@imshow_gui, V, handles.axes1};
else
    figure(1), imshow3D(V);
end

%% parameters
% sigCurv=3;

S_csf = 2;

BG_median_thr = 0;

cellsize = [ceil(size(V,1)/32) ceil(size(V,2)/32)];
blksize = [32 32 FNUM];

thr_val = 50;
region_size = 200;

r_oof = 6;
opts.responsetype=5;
OOF_thr = 3;


%% Adjust Image Intensity
V_adj = zeros(size(V));
for fcount = 1:FNUM
    V_adj(:,:,fcount) = imadjust(V(:,:,fcount),[0,0.3],[0,1]);
end
if ~handles.script
    handles.jTextArea.append(['Adjust Image Intensity finished...' char(10)]);
    ptitle = 'imadjust';
    imshow_gui(V_adj, handles.axes1, 1);
    ptitles{2} = ptitle;
    myCallback{2} = {@imshow_gui, V_adj, handles.axes1};
end
%% Laplacian of Gaussian
V_csf = CSF_cos(uint8(V_adj),S_csf);
if ~handles.script
    handles.jTextArea.append(['Laplacian of Gaussian finished...' char(10)]);
    pause(1/8);
    ptitle = 'LoG';
    imshow_gui(V_csf, handles.axes1, 1);
    ptitles{3} = ptitle;
    myCallback{3} = {@imshow_gui, V_csf, handles.axes1};
else
    figure(2), imshow3D(V_csf);
end

%% heuristic: BG subtraction
V_bg_sub_cell = cell(cellsize);
V_bg_sub_reg_cell = cell(cellsize);
for a = 1:prod(cellsize)
    [i,j] = ind2sub(cellsize,a);
    row = (i-1)*32+1;
    col = (j-1)*32+1;
    rowt = min(row+blksize(1)-1, size(V,1));
    colt = min(col+blksize(1)-1, size(V,2));
    for fcount = 1:FNUM,
        block = double(V_csf(row:rowt,col:colt,fcount));
        bg = median(block(:));
        new_block = block-bg;
        new_block(new_block < BG_median_thr) = 0;
        V_bg_sub_cell{a}(:,:,fcount) = new_block;
    end
end

V_bg_sub = zeros(size(V));
for a = 1:prod(cellsize)
    [i,j] = ind2sub(cellsize,a);
    row = (i-1)*32+1;
    col = (j-1)*32+1;
    rowt = min(row+blksize(1)-1, size(V,1));
    colt = min(col+blksize(1)-1, size(V,2));
    level = graythresh(V_bg_sub_cell{a});
    V_bg_sub(row:rowt,col:colt,:) = V_bg_sub_cell{a};
end

if ~handles.script
    handles.jTextArea.append(['Background Subtraction finished...' char(10)]);
    pause(1/8);
    ptitle = 'BG Sub';
    imshow_gui(V_bg_sub, handles.axes1, 1);
    ptitles{4} = ptitle;
    myCallback{4} = {@imshow_gui, V_bg_sub, handles.axes1};
else
    figure(3), imshow3D(V_bg_sub);
end

%% Small Region Filter
V_thr = Threshold(V_bg_sub,thr_val);
V_reg = EliminateSmallReg(V_bg_sub,region_size);
if ~handles.script
    handles.jTextArea.append(['Small Region Filter finished...' char(10)]);
    pause(1/8);
    ptitle = 'Small Reg.';
    imshow_gui(V_reg, handles.axes1, 1);
    ptitles{5} = ptitle;
    myCallback{5} = {@imshow_gui, V_reg, handles.axes1};
end

%% OOF
if ismatrix(V_reg)
    s = [size(V_reg), 1];
else
    s = size(V_reg);
end

newE = zeros(s+(r_oof+3)*2);
newE(r_oof+4:r_oof+3+s(1), r_oof+4:r_oof+3+s(2), r_oof+4:r_oof+3+s(3)) = V_reg;
Eoof_block = oof3response(newE,1:r_oof,opts);

if ~handles.script
    handles.jTextArea.append(['Optimal Oriented Flux finished...' char(10)]);
    pause(1/8);
    ptitle = 'OOF';
    imshow_gui(Eoof_block, handles.axes1, 1);
    ptitles{6} = ptitle;
    myCallback{6} = {@imshow_gui, Eoof_block, handles.axes1};

    ptitle = 'OOF Threshold';
    imshow_gui(Eoof_block>OOF_thr, handles.axes1, 1);
    ptitles{7} = ptitle;
    myCallback{7} = {@imshow_gui, Eoof_block>OOF_thr, handles.axes1};
else
    figure(4), imshow3D(Eoof_block);
end

%% detect seed points by ridge points
if ismatrix(Eoof_block)
    [fx,fy] = AM_gradient(Eoof_block);
else
    [fx,fy,~] = AM_gradient(Eoof_block);
end
seedsx = fx(:,1:end-1,:)>0&fx(:,2:end,:)<0&Eoof_block(:,1:end-1,:) > OOF_thr;
seedsy = fy(1:end-1,:,:)>0&fy(2:end,:,:)<0&Eoof_block(1:end-1,:,:) > OOF_thr;
seeds = seedsx(1:end-1,:,:)|seedsy(:,1:end-1,:);
seeds = padarray(seeds,[1 1],'symmetric','post');

if ~handles.script
    handles.jTextArea.append(['Seed Points Detection finished...' char(10)]);
    pause(1/8);
end

%% Save filtered image and seed points
% get name of save file
C = regexp(strrep(handles.filename, '\', '/'),'\.','split');
C = C(~cellfun(@(x)isempty(x), C));
savefile = ['n', C{1}, '.mat'];
smooth = single(V_bg_sub);
sample_factor = 1;

imgdir = handles.imgdir;
if mkdir(imgdir)
    save ([imgdir savefile], 'V', 'smooth', 'seeds', 'blksize', 'cellsize', 'sample_factor', '-v7.3');
else
    error('ERROR: cannot save preprocessed image stack');
end

%% Show seed points
if ~handles.script
    ptitle = 'Seeds';
    imshowoverlay_gui(V_bg_sub, seeds, handles.axes1, 1);
    ptitles{8} = ptitle;
    myCallback{8} = {@imshowoverlay_gui, V_bg_sub, seeds, handles.axes1};
    handles.jTextArea.append(['Save: ' imgdir savefile char(10)]);
    handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength);
else
    figure(5), imshow3Doverlay(V_bg_sub, seeds);
end
end