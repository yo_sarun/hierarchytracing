% Function needed for MatLab - Java data format conversion.

function [Original, IM, AM, R, Red_Type, Red_Fac] = Read_Data(filepath)

arr = load(filepath);

Original = arr.Original;
IM = arr.IM;
AM = arr.AM;
R = arr.R;
Red_Type = arr.Red_Type;
Red_Fac = arr.Red_Fac;