% This function performs automated branch merger. AM_merged is the 
% adjacency matrix for the merged branched structure. Loops are avoided
% at every step of the merger. The cost of 20 most likely merger scenarios are
% recalculated for each cluster, taking into account the curvature,
% tortuocity, intensity, and variation of intensity in the merging structure.

function AMlbl_merged=AutomatedMerger(Im,AMlbl,r,Dist,Cos,Offset,MeanI,CVI,MeanI0,Parameters)

Dist_thr=Parameters.Dist_thr;
L=unique(AMlbl(AMlbl>0));

% step back stepsback steps to determine tip1V tip2V vertexes of branch endings 
[tip1V tip2V]=StepBack(AMlbl,Parameters.stepsback*Parameters.pointsperum);

tip1=zeros(1,length(L));
tip2=zeros(1,length(L));
Blengths=zeros(1,length(L));
for i=1:length(L)
    tip1(L(i))=tip1V{L(i)}(1);
    tip2(L(i))=tip2V{L(i)}(1);
    [v ~]=find(AMlbl==L(i));
    Blengths(L(i))=length(unique(v));
end

End_dist=[[Dist(:,:,1),Dist(:,:,2)];[Dist(:,:,3),Dist(:,:,4)]];
End_cos=[[Cos(:,:,1),Cos(:,:,2)];[Cos(:,:,3),Cos(:,:,4)]];
End_Offset=[[Offset(:,:,1),Offset(:,:,2)];[Offset(:,:,3),Offset(:,:,4)]];
End_meanI=[[MeanI(:,:,1),MeanI(:,:,2)];[MeanI(:,:,3),MeanI(:,:,4)]];
End_CVI=[[CVI(:,:,1),CVI(:,:,2)];[CVI(:,:,3),CVI(:,:,4)]];

% Cluster the branch end-points by dist
End_dist_temp=zeros(size(End_dist)); End_dist_temp(End_dist<=Dist_thr)=1;
[ClusterN,ClusterV] = FindClustersAM(spones(End_dist_temp+End_dist_temp'));

% Sort clusters by size: 2,4,3,5,6,7,...
ClusterSize=zeros(1,ClusterN);
for i=1:ClusterN
    ClusterSize(i)=length(ClusterV{i});
end
[ClusterSize,Cluster_ind]=sort(ClusterSize);
ClusterV=ClusterV(Cluster_ind);
Cluster_ind=[find(ClusterSize==2),find(ClusterSize==4),find(ClusterSize==3),find(ClusterSize>4)];
ClusterV=ClusterV(Cluster_ind);
ClusterN=length(ClusterV);

% Calculate the costs of all possible mergers
AMmergerL=sparse(size(Dist,1),size(Dist,1));
X=cell(1,ClusterN);
Xp=cell(1,ClusterN);
for i=1:ClusterN
    disp([num2str(i/ClusterN),', ',num2str(length(ClusterV{i})),'-way merger'])
    TipLabels=ClusterV{i};
    TipLabels(TipLabels>size(Dist,1))=TipLabels(TipLabels>size(Dist,1))-size(Dist,1);
    [Costs MergerAM]=NwayMerger(TipLabels,End_dist(ClusterV{i},ClusterV{i}),End_cos(ClusterV{i},ClusterV{i}),End_meanI(ClusterV{i},ClusterV{i}),End_CVI(ClusterV{i},ClusterV{i}),MeanI0,Parameters);
    
    % calculation of true tortuosity, Tmax, total intensity and length, Itotal and Ltotal, for each merger
    Tmax=inf(min(length(Costs),20),1); %!!!!!!!!!!!!!!!!!!!!!!!!!
    Itotal=zeros(size(Tmax));
    Ltotal=zeros(size(Tmax));
    Imean=zeros(size(Tmax));
    CV=zeros(size(Tmax));
    Kmax=zeros(size(Tmax));
    Kmean=zeros(size(Tmax));
    Free_tips=zeros(size(Tmax));
    ppL=ClusterV{i};
    
    b_length=zeros(1,length(TipLabels));
    r_connect=zeros(numel(cell2mat(tip1V(TipLabels)')),3);
    count=0;
    AM_connect_temp=zeros(size(r_connect,1),size(r_connect,1));
    for k=1:length(TipLabels)
        if ppL(k)<=size(Dist,1)
            tempV=tip1V{TipLabels(k)};
        else
            tempV=tip2V{TipLabels(k)};
        end
        b_length(k)=length(tempV);
        r_connect(count+1:count+b_length(k),:)=r(tempV,:);

        old_label_ind=find(TipLabels(1:k-1)==TipLabels(k),1);
        if isempty(old_label_ind)
            AM_connect_temp(count+1:count+b_length(k),count+1:count+b_length(k))=triu(ones(b_length(k),b_length(k)),1)-triu(ones(b_length(k),b_length(k)),2);
        else % for short branches tip1V and tip2V are overlapping and have to be merged 
            N_same_Vs=b_length(old_label_ind)+b_length(k)-Blengths(TipLabels(k));
            AM_connect_temp(count+1:count+b_length(k)-N_same_Vs+1,count+1:count+b_length(k)-N_same_Vs+1)=triu(ones(b_length(k)-N_same_Vs+1,b_length(k)-N_same_Vs+1),1)-triu(ones(b_length(k)-N_same_Vs+1,b_length(k)-N_same_Vs+1),2);
            AM_connect_temp(sum(b_length(1:old_label_ind)),count+b_length(k)-N_same_Vs+1)=1;
        end
        count=count+b_length(k);
    end
    
    cumb_length=1+cumsum([0,b_length(1:end-1)]);
    Xp{i}=-ones(1,length(Tmax));
    X{i}=zeros(8,length(Tmax));
    for j=1:length(Tmax)      
        AM_connect=AM_connect_temp;
        [ii,jj]=find(MergerAM(:,:,j));
        AM_connect(sub2ind(size(AM_connect),cumb_length(ii),cumb_length(jj)))=1;
        
        if Loops(AM_connect)==0
            [AM_connect1,r_connect1,I_snake]=Snake_All_norm(Im,AM_connect,r_connect,1,0,0,Parameters.pointsperum,Parameters.Nstep,Parameters.alpha,Parameters.betta,Parameters.sig_ips,Parameters.sig_bps,Parameters.sig_tps,0);
            AM_connect1=(AM_connect1>0);
            AM_connect1 = LabelTreesAM(AM_connect1);
            
            Tmax(j)=max(Tortuosity(AM_connect1,r_connect1,0,50));
            
            [it,jt]=find(AM_connect1);
            lll=sum((r_connect1(it,:)-r_connect1(jt,:)).^2,2).^0.5;
            Ltotal(j)=sum(lll)/2;
            Itotal(j)=sum(lll'.*(I_snake(it)+I_snake(jt)))/4;
            Imean(j)=mean(I_snake);
            CV(j)=std(I_snake)/mean(I_snake);
            Free_tips(j)=nnz(sum(MergerAM(:,:,j)+MergerAM(:,:,j)')==0);
            
            [~,Ktotal_temp,Kmean_temp]=Curvature(AM_connect1,r_connect1,1);
            Kmax(j)=max(Kmean_temp); % maximum curvature of connecting trees
            Kmean(j)=sum(Ktotal_temp)/Ltotal(j); % average curvature of all connecting trees
        end
    end
    
    % reorder the merger scenarios based on the new cost
    Cost2=Parameters.Ltotal.*Ltotal+Parameters.Itotal.*Itotal+Parameters.Imean.*Imean+...
        Parameters.Tmax.*Tmax+Parameters.CV.*CV+Parameters.Free_tips.*Free_tips+...
        Parameters.Kmax.*Kmax+Parameters.Kmean.*Kmean;
    [~,sort_ind]=sort(Cost2);
    
    count=1;
    while count<=length(Cost2)
        [pre,post]=find(MergerAM(:,:,sort_ind(count)));
        if isempty(pre)
            count=length(Cost2)+1;
        else
            preL=ClusterV{i}(pre);
            postL=ClusterV{i}(post);
            
            tips_k=zeros(size(preL));
            tips_k(preL<=size(Dist,1) & postL<=size(Dist,1))=1;
            tips_k(preL<=size(Dist,1) & postL>size(Dist,1))=2;
            tips_k(preL>size(Dist,1) & postL<=size(Dist,1))=3;
            tips_k(preL>size(Dist,1) & postL>size(Dist,1))=4;
            
            preL(preL>size(Dist,1))=preL(preL>size(Dist,1))-size(Dist,1);
            postL(postL>size(Dist,1))=postL(postL>size(Dist,1))-size(Dist,1);
            
            VpreL=zeros(size(preL));
            temp_ind=(tips_k==1 | tips_k==2);
            VpreL(temp_ind)=tip1(preL(temp_ind));
            temp_ind=(tips_k==3 | tips_k==4);
            VpreL(temp_ind)=tip2(preL(temp_ind));
            
            VpostL=zeros(size(preL));
            temp_ind=(tips_k==1 | tips_k==3);
            VpostL(temp_ind)=tip1(postL(temp_ind));
            temp_ind=(tips_k==2 | tips_k==4);
            VpostL(temp_ind)=tip2(postL(temp_ind));
            
            temp_ind1=sub2ind(size(Dist(:,:,1)),preL,postL);
            temp_ind2=sub2ind(size(Dist(:,:,1)),postL,preL);
            if length(unique(temp_ind1))<length(temp_ind1) || nnz(AMmergerL(temp_ind1))>0 || nnz(AMmergerL(temp_ind2))>0 
                count=count+1;
            else 
                AMtemp=AMmergerL;
                AMtemp(temp_ind1)=1;
                if nnz(AMtemp+AMtemp')==2*nnz(AMtemp) && Loops(AMtemp)==0
                    AMlbl_temp=AMlbl;
                    temp_ind=(VpreL~=VpostL);
                    AMlbl_temp(sub2ind(size(AMlbl),VpreL(temp_ind),VpostL(temp_ind)))=preL(temp_ind);
                    
%                     preL_temp=preL;
%                     postL_temp=postL;
                    [preL_temp,postL_temp]=find(AMtemp);
                    for ii=1:length(preL_temp)
                        AMlbl_temp(AMlbl_temp==postL_temp(ii))=preL_temp(ii);
                        preL_temp(preL_temp==postL_temp(ii))=preL_temp(ii);
                        postL_temp(postL_temp==postL_temp(ii))=preL_temp(ii);
                    end
                    L_temp=unique([preL_temp,postL_temp]);
                    Loop=0;
                    ii=1;
                    while Loop==0 && ii<=length(L_temp)
                        Loop=Loops(AMlbl_temp==L_temp(ii));
                        ii=ii+1;
                    end
                    if Loop==0
                        AMmergerL=AMtemp;
                        % connect
                        AMlbl(sub2ind(size(AMlbl),VpreL(temp_ind),VpostL(temp_ind)))=preL(temp_ind);
                        count=length(Cost2);
                    end
                    count=count+1;
                else
                    count=count+1;
                end
            end
        end
    end
end

% Create undirected AMlbl
AMlblp=AMlbl';
ind=(AMlbl-AMlblp)<0;
AMlbl(ind)=AMlblp(ind);

% Relabel AMlbl
[preL,postL]=find(AMmergerL);
AMlbl_merged=AMlbl;
for i=1:length(preL)
    AMlbl_merged(AMlbl_merged==postL(i))=preL(i);
    preL(preL==postL(i))=preL(i);
    postL(postL==postL(i))=preL(i);
end

% Reduce the lablels
L=sort(unique(AMlbl_merged(AMlbl_merged>0)));
for i=1:length(L)
    AMlbl_merged(AMlbl_merged==L(i))=i;
end

