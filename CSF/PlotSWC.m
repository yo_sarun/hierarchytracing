% This function plots the tree structure from an SWC format.

function PlotSWC(pth_or_swc)

if exist(pth_or_swc,'file')
    swc=textread(pth_or_swc);
elseif size(pth_or_swc,2)==7
    swc=pth_or_swc;
else
    error('Incorrect path or SWC file format')
end

id=swc(:,1);
pid=swc(:,7);
r=[swc(:,4),swc(:,3),swc(:,5)]+1;

Roots=find(pid==-1);
plot3(r(Roots,2),r(Roots,1),r(Roots,3),'m.','MarkerSize',10)
hold on

for i=[id(pid~=-1)]'
    line([r(pid(i),2),r(i,2)],[r(pid(i),1),r(i,1)],[r(pid(i),3),r(i,3)],'Color','m')
end
