% This function thresholds the stack.

function Im=Threshold(Im,thr_low)

rem_ind=(Im(:)<thr_low);
Im(rem_ind)=0;
