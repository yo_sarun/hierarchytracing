% This function calculates 4 cost components for merging branch pairs. These 
% components are: tip-to-tip distance Dist, cosine of tip orientations,
% average intensity, and variation of intensity along the length of the optimally connecting path.
% MaxDistance is the maximum distance between branches for which merger
% will be considered
% trim is the size of the region near the xy faces of the stack where branch tips are not merged.
% This function also prompts the user to perform semi-automated branch merger.
% The result is used for perceptron learning to determine the coefficients
% in the cost function.

function [Dist,Cos,Offset,MeanI,CVI] = MergingCosts(Im,AMlbl,r,Parameters)

sizeIm=size(Im);
L=unique(AMlbl(AMlbl>0));

% step back stepsback steps to determine tip1V tip2V vertexes of branch endings 
[tip1V tip2V]=StepBack(AMlbl,Parameters.stepsback*Parameters.pointsperum);

tip1=zeros(1,length(L));
tip2=zeros(1,length(L));
for i=1:length(L)
    tip1(L(i))=tip1V{L(i)}(1);
    tip2(L(i))=tip2V{L(i)}(1);
end

% branch end orientations
n1=zeros(length(L),3);
n2=zeros(length(L),3);
for i=1:length(L) 
    n1(i,:)=r(tip1V{L(i)}(end),:)-r(tip1V{L(i)}(1),:);
    n1(i,:)=n1(i,:)./sum(n1(i,:).^2)^0.5;
    
    n2(i,:)=r(tip2V{L(i)}(end),:)-r(tip2V{L(i)}(1),:);
    n2(i,:)=n2(i,:)./sum(n2(i,:).^2)^0.5;
end

% 4 distances and cosines are calculated: tip1-tip1, tip1-tip2, tip2-tip1, tip2-tip2
Dist=inf(length(L),length(L),4); % Dist=inf is the most unfavorable situation for merging
Cos=ones(length(L),length(L),4); % Cos=1 is the most unfavorable situation for merging
MeanI=zeros(length(L),length(L),4); % MeanI=0 is the most unfavorable situation for merging
CVI=inf(length(L),length(L),4); % CVI=inf is the most unfavorable situation for merging
Offset=inf(length(L),length(L),4); % Offset=inf is the most unfavorable situation for merging
%TortuosityI=inf(length(L),length(L),4); % TortuosityI=inf is the most unfavorable situation for merging

for i=1:length(L)
    for j=(i+1):length(L)
        % Distances
        Dist(i,j,1)=sum((r(tip1V{L(i)}(1),:)-r(tip1V{L(j)}(1),:)).^2).^0.5;
        Dist(i,j,2)=sum((r(tip1V{L(i)}(1),:)-r(tip2V{L(j)}(1),:)).^2).^0.5;
        Dist(i,j,3)=sum((r(tip2V{L(i)}(1),:)-r(tip1V{L(j)}(1),:)).^2).^0.5;
        Dist(i,j,4)=sum((r(tip2V{L(i)}(1),:)-r(tip2V{L(j)}(1),:)).^2).^0.5;
        
        % Cosines
        Cos(i,j,1)=sum(n1(i,:).*n1(j,:));
        Cos(i,j,2)=sum(n1(i,:).*n2(j,:));
        Cos(i,j,3)=sum(n2(i,:).*n1(j,:));
        Cos(i,j,4)=sum(n2(i,:).*n2(j,:));
        
        % Branch offsets
        n1n2=cross(n1(i,:),n1(j,:)); n1n2=n1n2./sum(n1n2.^2)^0.5;
        r_parpendicular=dot((r(tip1V{L(i)}(end),:)-r(tip1V{L(j)}(end),:)),n1n2).*n1n2;
        r_parallel=(r(tip1V{L(i)}(end),:)-r(tip1V{L(j)}(end),:))-r_parpendicular;
        Offset(i,j,1)=sum(r_parpendicular.^2)^0.5/(sum(r_parallel.^2)^0.5+10^-5);
        
        n1n2=cross(n1(i,:),n2(j,:)); n1n2=n1n2./sum(n1n2.^2)^0.5;
        r_parpendicular=dot((r(tip1V{L(i)}(end),:)-r(tip2V{L(j)}(end),:)),n1n2).*n1n2;
        r_parallel=(r(tip1V{L(i)}(end),:)-r(tip2V{L(j)}(end),:))-r_parpendicular;
        Offset(i,j,2)=sum(r_parpendicular.^2)^0.5/(sum(r_parallel.^2)^0.5+10^-5);
        
        n1n2=cross(n2(i,:),n1(j,:)); n1n2=n1n2./sum(n1n2.^2)^0.5;
        r_parpendicular=dot((r(tip2V{L(i)}(end),:)-r(tip1V{L(j)}(end),:)),n1n2).*n1n2;
        r_parallel=(r(tip2V{L(i)}(end),:)-r(tip1V{L(j)}(end),:))-r_parpendicular;
        Offset(i,j,3)=sum(r_parpendicular.^2)^0.5/(sum(r_parallel.^2)^0.5+10^-5);
        
        n1n2=cross(n2(i,:),n2(j,:)); n1n2=n1n2./sum(n1n2.^2)^0.5;
        r_parpendicular=dot((r(tip2V{L(i)}(end),:)-r(tip2V{L(j)}(end),:)),n1n2).*n1n2;
        r_parallel=(r(tip2V{L(i)}(end),:)-r(tip2V{L(j)}(end),:))-r_parpendicular;
        Offset(i,j,4)=sum(r_parpendicular.^2)^0.5/(sum(r_parallel.^2)^0.5+10^-5);
    end
end

% distant branch tips or branch tips near the faces of the stack will not be mearged
edge_tip1=(r(tip1(L),1)<=Parameters.trim | r(tip1(L),1)>sizeIm(1)-Parameters.trim | r(tip1(L),2)<=Parameters.trim | r(tip1(L),2)>sizeIm(2)-Parameters.trim);
edge_tip2=(r(tip2(L),1)<=Parameters.trim | r(tip2(L),1)>sizeIm(1)-Parameters.trim | r(tip2(L),2)<=Parameters.trim | r(tip2(L),2)>sizeIm(2)-Parameters.trim);
Dist(edge_tip1,:,1)=inf; Dist(:,edge_tip1,1)=inf;
Dist(edge_tip1,:,2)=inf; Dist(:,edge_tip2,2)=inf;
Dist(edge_tip2,:,3)=inf; Dist(:,edge_tip1,3)=inf;
Dist(edge_tip2,:,4)=inf; Dist(:,edge_tip2,4)=inf;

ind_pass=find(Dist(:)<Parameters.MaxDistance);
[i_pass,j_pass,k_pass]=ind2sub(size(Dist),ind_pass);

AM_merge=sparse([]);
r_merge=zeros(numel(cell2mat(tip1V(i_pass)'))+numel(cell2mat(tip2V(j_pass)')),3);
count=0;
for i=1:length(k_pass)
    if k_pass(i)==1 
        tempV1=tip1V{L(i_pass(i))};
        tempV2=tip1V{L(j_pass(i))};
    elseif k_pass(i)==2 
        tempV1=tip1V{L(i_pass(i))};
        tempV2=tip2V{L(j_pass(i))};
    elseif k_pass(i)==3 
        tempV1=tip2V{L(i_pass(i))};
        tempV2=tip1V{L(j_pass(i))};
    elseif k_pass(i)==4
        tempV1=tip2V{L(i_pass(i))};
        tempV2=tip2V{L(j_pass(i))};
    end
    
    N1=length(tempV1);
    N2=length(tempV2);
    r_merge(count+1:count+N1+N2,:)=r([tempV1,tempV2],:);
    AM_merge(end+1:end+N1,end+1:end+N1)=triu(ones(N1,N1),1)-triu(ones(N1,N1),2);
    AM_merge(end+1:end+N2,end+1:end+N2)=triu(ones(N2,N2),1)-triu(ones(N2,N2),2);
    AM_merge(count+1,count+N1+1)=1;
    count=count+N1+N2;
end

% Intensity along the optimal connections
[AMlbl_merge,~,I_snake]=Snake_All_norm(Im,AM_merge,r_merge,0,0,0,Parameters.pointsperum,Parameters.Nstep,Parameters.alpha,Parameters.betta,Parameters.sig_ips,Parameters.sig_bps,Parameters.sig_tps,1);

AMlbl_merge_direct=triu(AMlbl_merge);
L_new=unique(AMlbl_merge_direct(AMlbl_merge_direct>0));

% Mean intensity and CV
for i=1:length(L_new)
    [e1 e2]=find(AMlbl_merge_direct==L_new(i));
    Ibranch_segs=(I_snake(e1)+I_snake(e2))./2;
    MeanI(ind_pass(L_new(i)))=mean(Ibranch_segs);
    CVI(ind_pass(L_new(i)))=(mean(Ibranch_segs.^2)-mean(Ibranch_segs)^2)^0.5/mean(Ibranch_segs);
end


% % Plot the meargers
% X=[Dist(ind_pass(L_new)),Cos(ind_pass(L_new)),MeanI(ind_pass(L_new)),FanoI(ind_pass(L_new))]';
% disp(['Numbe of putative mergers is ',num2str(length(ind_pass))])
% figure(20)
% imshow(max(Im,[],3));
% hold all; drawnow
% PlotAM_h(AMlbl,r);
% for i=1:length(L_new),i
%     h1=plot3(r_pre(L_new(i),2),r_pre(L_new(i),1),r_pre(L_new(i),3),'r*');
%     h2=plot3(r_post(L_new(i),2),r_post(L_new(i),1),r_post(L_new(i),3),'r*');
%     disp(['D=',num2str(Dist(ind_pass(L_new(i)))), '; Cos=',num2str(Cos(ind_pass(L_new(i)))),'; MeanI=',num2str(MeanI(ind_pass(L_new(i)))),'; FanoI=',num2str(FanoI(ind_pass(L_new(i))))])
%     AM_merge_temp=sparse(size(AMlbl_merge,1),size(AMlbl_merge,2));
%     AM_merge_temp(AMlbl_merge==L_new(i))=1;
%     h3=PlotAM_h(AM_merge_temp,r_merge);
%     user_input = input('Merge Type (2,3,4): ');
%     MergerType(i)=user_input;
%     user_input = input('Merge? (y,n,?): ','s');
%     if user_input=='y'
%         Xp(i)=1;
%     elseif user_input=='n'
%         Xp(i)=-1;
%     else
%         Xp(i)=NaN;
%     end
%     %save('X_Xp_data2','X','Xp','MergerType','Parameters')
%     delete(h1,h2,cell2mat(h3))
% end

%w2=perceptron(X,Xp,MergerType,2,0.1)




