% Main script. Run tracing snake.
clear all
close all
clc

%% set directories
resultdir = '../tracefiles/';
imgdir = [resultdir 'preprocessimage/'];
snkdir = [resultdir 'snake/'];
swcdir = [resultdir 'swc/'];
savedir = [resultdir 'image/'];
imggvf = [resultdir 'gvf/'];
featdir = [resultdir 'feat/'];

mkdir(imgdir);
mkdir(snkdir);
mkdir(swcdir);
mkdir(savedir);
mkdir(imggvf);
mkdir(featdir);

set(0,'DefaultFigureWindowStyle','normal')
warning('off','all')

%% Open parallel
if ~matlabpool('size')
    try
        matlabpool open
    catch
        warning('NO PARALLEL: matlabpool is not available');
    end
end

% %% Learn DRF parameter
% % readSWC
%% CF
% for counter = 1:2
%     if ismac
%         datapath = sprintf('/Users/Yo/Desktop/Cerebellar Climbing Fibers/Image Stacks/CF_%d/',counter);
%     elseif ispc
%         datapath = sprintf('C:/Neuron/CF/CF_%d/',counter);
%     end
%     
%     sample_factor = 4;
% 
%     tokens = regexp(datapath,'/','split');
%     tokens = tokens(~cellfun(@(x)isempty(x), tokens));
%     imgname = ['n', tokens{end}];
% 
%     if exist([imgdir imgname '.mat'], 'file') ~= 2
%         preprocessDIADEM_CF;
%     end
%     close all
%     [V, allsnk, Cs, Ss, blksize, level, tubes] = test_allsnk_topo(...
%             imgdir, imgname, snkdir);
%     [swcfile, fixswcfile] = test_propagate_all_topo(...
%             imgdir, imgname, snkdir, swcdir, sample_factor, ...
%             V, allsnk, Cs, Ss, blksize, level, tubes);
% end
% 
%% NC
% for counter = 1:6
%     if ismac
%         datapath = sprintf('/Users/Yo/Desktop/Neocortical Layer 1 Axons/Subset 1/Image Stacks/%02d/',counter);
%     elseif ispc
%         datapath = sprintf('C:/Neuron/Neocortical Layer 1 Axons/Subset 1/Image Stacks/%02d/',counter);
%     end
%     
%     sample_factor = 1;
% 
%     tokens = regexp(datapath,'/','split');
%     tokens = tokens(~cellfun(@(x)isempty(x), tokens));
%     imgname = ['n', tokens{end}];
% 
%     if exist([imgdir imgname '.mat'], 'file') ~= 2
%         preprocessDIADEM_NL;
%     end
%     close all
%     [V, allsnk, Cs, Ss, blksize, level, tubes] = test_allsnk_topo(...
%             imgdir, imgname, snkdir);
%     [swcfile, fixswcfile] = test_propagate_all_topo(...
%             imgdir, imgname, snkdir, swcdir, sample_factor, ...
%             V, allsnk, Cs, Ss, blksize, level, tubes);
% end
% 
% % stitch;       % stitch image stacks together
% 
%% OP
% for counter = 4:9
%     if ismac
%         datapath = sprintf('/Users/Yo/Desktop/train_data/Image_Stacks/OP_%d/',counter);
%     elseif ispc
%         datapath = sprintf('C:/Neuron/train_data/Image_Stacks/OP_%d/',counter);
%     end
%     
%     sample_factor = 1;
% 
%     tokens = regexp(datapath,'/','split');
%     tokens = tokens(~cellfun(@(x)isempty(x), tokens));
%     imgname = ['n', tokens{end}];
% 
%     if exist([imgdir imgname '.mat'], 'file') ~= 2
%         preprocessDIADEM_OP;
%     end
%     close all
%     [V, allsnk, Cs, Ss, blksize, level, tubes] = test_allsnk_topo(...
%             imgdir, imgname, snkdir);
%     [swcfile, fixswcfile] = test_propagate_all_topo(...
%             imgdir, imgname, snkdir, swcdir, sample_factor, ...
%             V, allsnk, Cs, Ss, blksize, level, tubes);
% end
% 
% 
%% Our dataset
% 
% if ismac
%     datadir = '/Users/Sarun/Desktop/Neuron/';
% elseif ispc
%     datadir = 'C:/Neuron/';
% end
% datapaths = strcat(datadir, {'72413/72413_h0/', ...
%         '72513/72513_h0/', ...
%         '72413/72413_h2/', ...
%         '72413/72413_h05/', ...
%         '72513/72513_h1/', ...
%         '73113_1/Late 1st - early 2nd instar/73113_1_L1E2_0/', ...
%         '73113_1/Late 1st - early 2nd instar/73113_1_L1E2_05/', ...
%         '73113_1/Late 1st - early 2nd instar/73113_1_L1E2_1/', ...
%         '73113_2/CD8 timelapse - late 3rd instar/73113_2_L3_h0/', ...
%         '73113_2/CD8 timelapse - late 3rd instar/73113_2_L3_h115/', ...
%         '73113_2/CD8 timelapse - late 3rd instar/73113_2_L3_h2/'});
% 
% for counter = 1:length(datapaths)
%     datapath = datapaths{counter};
%     sample_factor = 1;
% 
%     tokens = regexp(datapath,'/','split');
%     tokens = tokens(~cellfun(@(x)isempty(x), tokens));
%     imgname = ['n', tokens{end}];
% 
%     if exist([imgdir imgname '.mat'], 'file') ~= 2
%         preprocess;
%     end
%     tic;
%     close all
%     [V, allsnk, Cs, Ss, blksize, level, tubes] = test_allsnk_topo(...
%             imgdir, imgname, snkdir);
%     [swcfile, fixswcfile] = test_propagate_all_topo(...
%             imgdir, imgname, snkdir, swcdir, sample_factor, ...
%             V, allsnk, Cs, Ss, blksize, level, tubes);
%     toc;
% %     render;           % render the tracing result in 3D
% end


%% Our dataset (partial for quantitative result)
% % imgRegion;    % to generate partial image-stack
% datapath = savedir;
% filenames = dir([datapath '*.tif']);
% for counter = 1:length(filenames)
%     filename = filenames(counter).name;
%     sample_factor = 1;
% 
% 	tokens = regexp(filename,'\.','split');
%     tokens = tokens(~cellfun(@(x)isempty(x), tokens));
%     imgname = ['n', tokens{1}];
% 
%     if exist([imgdir imgname '.mat'], 'file') ~= 2
%         preprocess_region;
%     end
%     close all
%     [V, allsnk, Cs, Ss, blksize, level, tubes] = test_allsnk_topo(...
%             imgdir, imgname, snkdir);
%     [swcfile, fixswcfile] = test_propagate_all_topo(...
%             imgdir, imgname, snkdir, swcdir, sample_factor, ...
%             V, allsnk, Cs, Ss, blksize, level, tubes);
% end
% 
% 
%% Nima dataset
% datapaths = {'3-1-14_CNGxWNC_H16 Timelapse', ...
%     'CNGv12xWNC_E3H16_2-8-14', ...
%     'CNGxPNC_E1H15_1-30-14 (0-948)', ...
%     'CNGxPNC_E1H16_2-5-14_(0-430)', ...
%     'CNGxWNC_E1H14_12-27-13_(0-1339)', ...
%     'CNGxWNC_E1H16_12-27-13_(0-1271)'};
% for counter = 1:length(datapaths)
%     if ismac
%         datapath = ['/Users/Yo/Desktop/NeuronTracing/', datapaths{counter}];
%     elseif ispc
%         datapath = ['C:/Neuron/NeuronTracing/', datapaths{counter}];
%     end
%     sample_factor = 1;
% 
%     tokens = regexp(datapath,'/','split');
%     tokens = tokens(~cellfun(@(x)isempty(x), tokens));
%     imgname = ['n', tokens{end}];
% 
%     if exist([imgdir imgname '.mat'], 'file') ~= 2
%         preprocessCCmotor2;
%     end
% 
%     close all
%     [V, allsnk, Cs, Ss, blksize, level, tubes] = test_allsnk_topo(...
%             imgdir, imgname, snkdir);
%     [swcfile, fixswcfile] = test_propagate_all_topo(...
%             imgdir, imgname, snkdir, swcdir, sample_factor, ...
%             V, allsnk, Cs, Ss, blksize, level, tubes);
% end
% 
%% Kim dataset
handles = [];
handles.imgdir = imgdir;
handles.imggvf = imggvf;
handles.snkdir = snkdir;
handles.swcdir = swcdir;
handles.featdir = featdir;
handles.savedir = savedir;
handles.analyze_pathname = handles.imgdir;
handles.script = true;
opt = [];

if ismac
    handles.pathname = '/Users/Yo/Desktop/KimImg/';
elseif ispc
    handles.pathname = 'C:/Neuron/train_data/KimImg/';
end

neurondirs = dir(handles.pathname);
for i = 1:length(neurondirs),
    neurondir = neurondirs(i);
    if neurondir.name(1) == '.'
        continue
    end
    handles.filename = neurondir.name;
    filetokens = regexp(handles.filename,'\.','split');
    handles.file_extension = char(filetokens(end));

    sample_factor = 1;

    tokens = regexp(handles.pathname, '/', 'split');
    tokens = tokens(~cellfun(@(x)isempty(x), tokens));
    imgname = ['n', tokens{end}];

    disp(['Preprocess image volume of : ' handles.filename])
    [imgdir, savefile] = preprocessKim(handles);
    handles.analyze_filename = savefile;

    % end
    close all
    [~, ~, allsnk, tubes, blksize, Ss, Cs, level, V] = ...
            test_allsnk_topo(handles, opt);

    [swcdir, swcfile, ~, ~] = test_propagate_all_topo(...
            handles, V, allsnk, tubes, blksize, Ss, Cs, level, opt);
    close all
end
    
%% close parallel
if matlabpool('size')
    matlabpool close
end