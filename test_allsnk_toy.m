% Trace snake in voxel level for toy image
clear all
close all
clc

load 'toy.mat'
load './paramsDRF2.mat'

addpath('./AMT')
addpath('./frangi/')
addpath('./utils')
addpath('./DRF')
addpath('./snake')
addpath('./block')
addpath('./topology')
addpath('./ba_interp3')
addpath('./oof3response')
addpath(genpath('./UGM'))

disp('======================================')
disp('Open Snake Curve with DRF at Pixel level on Toy Image Stack')
%% Find snake in each block
blksize = [32,32,2];
allsnk = cell(3,3);
tubes = cell(3,3);
Fexts = cell(3,3);
snkcodes = cell(3,3);
Ss = zeros(3,3);
Cs = cell(3,3);
time = zeros(3,3);
looptime = tic;
edgeStruct = BLK_getEdgeStruct(blksize);

options.FrangiScaleRange = [1,8];
options.Fcount = 1;
for i = 1:3
    for j = 1:3
        tic;
        row = ((i-1)*32)+1;
        col = ((j-1)*32)+1;
        seed = seeds(row:row+31,col:col+31,:);
        U = smooth(row:row+31,col:col+31,:);
        
        disp('--------------------------------------------------')
        disp('Computing the GVF external force field ...')
        mu = .2;
        GVF_ITER = 10;
        h = fspecial('gaussian',[5 5],5);
        f = imfilter(double(U),h);
        Fexts = AM_GVF(f, mu, GVF_ITER, 1);
        Fext = cell(1,3);
        for n = 1:3
            Fext{n} = double(Fexts(:,:,:,n));
        end
        [allsnk{i,j}, tubes{i,j}, snkcodes{i,j}, Ss(i,j), Cs{i,j}] = ...
                find_snake_topo(U, seed, Fext, edgeStruct, result, options );
        time(i,j) = toc;
        close all;
    end
end
disp('Done!!');
looptime = toc(looptime);

%% Save result
save 'snkDRF_topo_toy.mat' allsnk tubes time looptime blksize snkcodes Ss Cs '-v7.3'
disp('SAVE')

%% show overlaid image
figure(3), SNK_show(allsnk, blksize, '', 1, V(:,:,1));

%% show 3D image
figure(4), SNK_show(allsnk, blksize, 'copper');
