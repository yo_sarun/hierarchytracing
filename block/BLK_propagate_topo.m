function [ opensnk, tube, S, C] = BLK_propagate_topo( U, allsnk, ...
        blockStruct, blksize, tubes, Fext, ...
        Ss, Cs ,r, c, level, options)
% BLK_PROPAGATE trace snake at block level
%     Inputs
%     U               3D array image stack
%     allsnk          2D cell array of snake struct in the whole image
%     blockStruct     graphical model structure
%     blksize         block size e.g. [nRows nCols nDeps]
%     tubes           2D cell array of the pixel-level probability map of 
%                     the whole image
%     Fext            1-by-3 cell array of GVF of superblock
%     Ss              2D array contains number of segment in each block
%     Cs              2D cell array contains segment number of each snake
%     r               row in cell array where r in [0,nRows-1]
%     c               column in a cell array where c in [0,nCols-1]
%     level           the resolution level
%     options         utility options
%     
%     Outputs
%     opensnk         1D array of snake struct inside the superblock
%     tube            a probability map of superblock
%     S               number of segments in superblock
%     C               segment number of each snake
%

if~ismember(nargin,11:12)
    error('Invalid inputs to BLK_propagate_topo!')    
end

defaultoptions = struct('FrangiScaleRange', [1 8], 'Alpha', 1, 'Beta', 1, 'Tau', .5, ...
        'Fcount', 0, 'Iter_thr', 7, 'Snake_iter', 100, 'LengthThreshold', 0);
% Process inputs
if(~exist('options','var')), 
    options = defaultoptions; 
else
    options = get_options(defaultoptions, options);
end

% disp('Open Snake Curve with DRF at Block Level')
%% Load snakes, Fext, Egvf, Tube
snkgrid = cell(blockStruct.nRows,blockStruct.nCols);
inittube = zeros(size(U));
for i = 1:blockStruct.nRows
    for j = 1:blockStruct.nCols
        row = ((i-1)*blksize(1));
        col = ((j-1)*blksize(2));
        rowt = min(row+blksize(1), size(U,1));
        colt = min(col+blksize(2), size(U,2));
        snkgrid{i,j} = allsnk{r+i,c+j};
        inittube(row+1:rowt, col+1:colt, :) = tubes{r+i,c+j};
        for k = 1:size(snkgrid{i,j},2)
            snklen = size(snkgrid{i,j}(k).vert,1);
            snkgrid{i,j}(k).vert = snkgrid{i,j}(k).vert + ...
                    repmat([col, row, 0],snklen,1);
        end
    end
end

if all(cellfun(@isempty, snkgrid))
    opensnk = [];
    S = 0;
    C = [];
    % combine DRF snake-level and pixel-level into a new probability map
    update_opts.FrangiScaleRange = options.FrangiScaleRange;
    update_opts.Fcount = options.Fcount;
    tube = BLK_update( blockStruct, blksize, U, inittube, update_opts);
    return;
end

S = Ss(r+1:r+blockStruct.nRows,c+1:c+blockStruct.nCols);
Cs = Cs(r+1:r+blockStruct.nRows,c+1:c+blockStruct.nCols);
offset = [0 cumsum(reshape(S,1,numel(S)))];
for i = 1:numel(S)
    Cs{i} = Cs{i} + offset(i);
end
C = [Cs{:}];

%% snake parameters
clearvars -except snkgrid blockStruct blksize inittube Fext U C S level options

SAVE_AVI = 0;           % set to 1 to save the process as .avi movie
SNAKE_ITER1 = options.Snake_iter;
alpha = options.Alpha;
beta = options.Beta;
tau = options.Tau;
RES = 1;
iter_thr = options.Iter_thr;
fcount = options.Fcount;
s = size(U);

%% combine DRF snake-level and pixel-level into a new probability map
update_opts.FrangiScaleRange = options.FrangiScaleRange;
update_opts.Fcount = options.Fcount;
[tube, mask, ev] = BLK_update( blockStruct, blksize, U, inittube, update_opts);
clearvars Egvf inittube

%% initialize snakes
U = U./255;
[fx, fy, fz] = gradient(U);
df = sqrt(fx.^2 + fy.^2 + fz.^2);
cond = cellfun(@(x)isempty(x),snkgrid);
for i = 1:numel(cond)
    if cond(i)
        snkgrid{i} = [];
    end
end
opensnk = [snkgrid{:}];
grids = zeros(size(opensnk));   % the grid that snake resides in
offset = [0, cumsum(reshape(cellfun('length',snkgrid),[1 numel(snkgrid)]))];
for i = 1:numel(snkgrid)
    grids(offset(i)+1:offset(i+1)) = repmat(i,size(snkgrid{i}));
end

nsnk = size(opensnk,2);
for n = 1:nsnk
    opensnk(n).ncon = 0;
    opensnk(n).converge = false;
    opensnk(n).label = n;
    opensnk(n).collideh = 0;
    opensnk(n).collidet = 0;
    opensnk(n).snkh = opensnk(n).snkh + offset(grids(n));
    opensnk(n).snkt = opensnk(n).snkt + offset(grids(n));
    opensnk(n).grids(level) = grids(n);
    opensnk(n) = SNK_get_energy(opensnk(n),alpha,beta,U,df,[]);
end

minE = [opensnk.E];
if fcount > 0, opensnk0 = opensnk; end;
clearvars snkgrid

%% Display initial snake & GVF
if fcount > 0
    disp('Displaying Neuron Mask ...')
    figure(4)
    imshow3D(mask)

    figure(2);
    disp('Displaying Original Image ...')
    subplot(1,3,1);
    imshow(mean(U,3),[]);
    title('preprocessed image');
    
    I = U(:,:,fcount);
    disp('Displaying the external force field ...')
    subplot(1,3,2)
    AC_quiver(Fext{1}(:,:,fcount), Fext{2}(:,:,fcount), I );
    title('GVF normalized field');

    disp('Displaying the Tangent force field ...')
    colmap = jet(nsnk);
    subplot(1,3,3)
    AC_quiver( ev{1}(:,:,fcount), ev{2}(:,:,fcount), I, 'r' );
    hold on
    for i = 1:nsnk
        AC_display(opensnk(i).vert,'open','-o',colmap(i,:));
    end
    hold off
    title('The first principal EV')
    drawnow;
end

%% get initial snake model
predict_options.Figure = 0;
predict_options.Debug = false;
snkcode = ones(nsnk,2);
[opensnk, snkcode, model] = TP_predict_set( opensnk, s, snkcode, S, C, level, predict_options );

%% Evolve snakes

if SAVE_AVI
	mov = avifile(['example_vfc_',num2str(cs),'.avi'],...
            'fps',4,'quality',100,'compression','None');
	frame = getframe(gca);
	mov = addframe(mov,frame);
end

% disp('Deforming the snake ...')

for i=1:SNAKE_ITER1
    for j=1:nsnk
        if opensnk(j).converge
            continue;
        end
        
        opensnk(j) = AC_deform(opensnk(j),...
                alpha,beta,tau,Fext,ev,mask,...
                tube,snkcode(j,:),model(j),'open');
        
        [opensnk(j), needremesh] = SNK_remesh(opensnk(j), RES, s, mask);
        if needremesh || mod(i,5)==0
            opensnk(j) = AC_remesh(opensnk(j), RES, 'equal', 'open');
        end
        % update snake_j's energy
        opensnk(j) = SNK_get_energy(opensnk(j), alpha, beta, U, df, model(j));
        if SNK_is_converge(opensnk(j), minE(j))
            opensnk(j).ncon = opensnk(j).ncon + 1;
        else
            opensnk(j).ncon = 0;
        end
        minE(j) = min(minE(j), opensnk(j).E);
        
%         % display snakes
%         if mod(i,2)==0 && fcount > 0
%             figure(3)
%             imagesc(I), colormap gray;
%             hold on
%             for k = 1:nsnk
%                 h=AC_display(opensnk(k).vert,'open','-o', colmap(k,:));
%             end
%             for k = 1:nsnk
%                 h=AC_display(opensnk0(k).vert,'open','--', colmap(k,:));
%             end
%             hold off
%             title(['Snake at block level iteration ' num2str(i)])
%             drawnow;
%         end
%         if SAVE_AVI
%             frame = getframe(gca);
%             mov = addframe(mov,frame);
%         end
    end
    % update block energy
    if mod(i,8)==0
        [opensnk, snkcode, model] = TP_predict_opt( opensnk, s, snkcode );
    elseif mod(i,2)==0
        [opensnk, snkcode] = TP_predict_opt( opensnk, s, snkcode );
    end
    for j = 1:nsnk
        if opensnk(j).converge
            continue;
        end
        if opensnk(j).ncon > iter_thr || all(snkcode(j,:) == 0) || size(opensnk(j).vert,1) < 3
%             disp(['Snake ', num2str(j), ' Converge at iter ' num2str(i)])
            opensnk(j).converge = true;
        end
    end
    if all([opensnk.converge])
        break;
    end
end
% disp('Done!')
if SAVE_AVI
    h=close(mov);
end

%% Show result snake
if fcount > 0, figure(5), subplot(1,2,1), SNK_show(opensnk); end;
opts_filter.LengthThreshold = options.LengthThreshold; %s(1)/10;
[opensnk, S, C] = BLK_filter_snk(opensnk, size(U), opts_filter);
if fcount > 0, figure(5), subplot(1,2,2), SNK_show(opensnk); end;

end

