function [ newsnk, S, C ] = BLK_filter_snk( opensnk, s, options )
%BLK_FILTER_SNAKE remove short snakes, overlap snakes, find snake segments
%
%     Inputs
%     opensnk        1-by-n array of snake struct
%     s              size of the image i.e. [nRows, nCols, nDeps]
%     options        utility options
%     
%     Outputs
%     newsnk         new 1-by-n array of snake struct
%     S              number of snake segment
%     C              snake segment number
%
if ~ismember(nargin, 2:3)
    error('Invalid inputs to BLK_filter_snk!')
end

defaultoptions = struct('LengthThreshold', 0, 'EnergyThreshold', 100);
% Process inputs
if(~exist('options','var')), 
    options = defaultoptions; 
else
    options = get_options(defaultoptions, options);
end
length_thr = options.LengthThreshold;
energy_thr = options.EnergyThreshold;

%% create snake graph
nsnk = size(opensnk,2);
edge = zeros(length([opensnk.snkh]) + length([opensnk.snkt]), 2);
if isempty(edge)
    adj = sparse(nsnk,nsnk);
else
    cur = 1;
    for i = 1:nsnk
        for j = 1:length(opensnk(i).snkh)
            edge(cur,:) = [i , opensnk(i).snkh(j)];
            cur = cur+1;
        end
        for j = 1:length(opensnk(i).snkt)
            edge(cur,:) = [i , opensnk(i).snkt(j)];
            cur = cur+1;
        end
    end
    edge = edge(1:cur-1,:);
    adj = sparse( [edge(:,1); edge(:,2)], [edge(:,2); edge(:,1)], 1, nsnk, nsnk );
end
% tree = graphminspantree(adj, 'Method', 'Kruskal');
tree = kruskal_mst(adj);

%% remove part of snake that repeat tracing (partially & totally overlap)
visit = false(size(opensnk));
newsnk = [];
C = [];
S = 0;
cur = 1;
for n = 1:nsnk
    if visit(n)
        continue
    end
    S = S + 1;
%     order = graphtraverse(tree, n, 'Directed', false, 'Method', 'BFS');
    order = bfs_traverse(tree, n);
    visit(order) = true;
    if sum([opensnk(order).length]) < length_thr || sum([opensnk(order).E]) >= energy_thr
        continue
    end
    tmpsnk = [];
    snklabel = false(s);
    for i = order
        % make sure snake is inside image
        if isempty(opensnk(i).vert)
            continue
        end
        opensnk(i).vert(opensnk(i).vert<1) = 1;
        opensnk(i).vert(opensnk(i).vert(:,1)>s(2),1) = s(2); 
        opensnk(i).vert(opensnk(i).vert(:,2)>s(1),2) = s(1); 
        opensnk(i).vert(opensnk(i).vert(:,3)>s(3),3) = s(3);
        vert = round(opensnk(i).vert);
        % get nonoverlap snake points
        Idx = sub2ind(size(snklabel), vert(:,2), vert(:,1), vert(:,3));
        % break into snakes if necessary
        opensnk(i).label = cur;
        snks = SNK_break(opensnk(i), ~snklabel(Idx));
        % update connectivity
        for a = 1:length(snks)
            min_ds_h = Inf; min_ds_t = Inf;
            for j = 1:length(tmpsnk)
                ds_h = bsxfun(@minus, tmpsnk(j).vert, snks(a).vert(1,:));
                ds_t = bsxfun(@minus, tmpsnk(j).vert, snks(a).vert(end,:));
                if min_ds_h > min(sqrt(sum(ds_h.^2,2)))
                    min_ds_h = min(sqrt(sum(ds_h.^2,2))); 
                    snks(a).snkh = tmpsnk(j).label;
                end
                if min_ds_t > min(sqrt(sum(ds_t.^2,2)))
                    min_ds_t = min(sqrt(sum(ds_t.^2,2))); 
                    snks(a).snkt = tmpsnk(j).label;
                end
            end
            if min_ds_h < min_ds_t
                snks(a).snkt = [];
            else
                snks(a).snkh = [];
            end
        end
        % update snakelabel & tmpsnk
        tmpsnk = [tmpsnk, snks];
        snklabel = SNK_label(vert,snklabel);
        cur = cur + length(snks);
    end
    newsnk = [newsnk, tmpsnk];
    C = [C, repmat(S, size(tmpsnk))];
end
end

