function [ blockStruct ] = BLK_getStruct( nRows, nCols, nDeps )
%BLK_GETSTRUCT generate blockStruct with 6-neightborhood of block
%size [nRows, nCols, nDeps]
%     Inputs
%     nRows           number of rows
%     nCols           number of columns
%     nDeps           number of depths
%     
%     Outputs
%     blockStruct     graphcial model struct or block struct with 26-neighborhood
%
%% get snake edge Struct
nNodes = nRows*nCols*nDeps;
nStates = 2;

% Add Down Edges
ind = 1:nNodes;
tmp = zeros(1,nCols*nDeps);
tmp(1:nCols:nCols*nDeps) = 1;
exclude = sub2ind([nRows nCols nDeps],...
        repmat(nRows,[1 nCols*nDeps]),...
        repmat(1:nCols, [1 nDeps]),...
        cumsum(tmp)); % No Down edge for last row
ind = setdiff(ind,exclude);
edges = [ind;ind+1];

% Add Right Edges
ind = 1:nNodes;
tmp = zeros(1,nRows*nDeps);
tmp(1:nRows:nRows*nDeps) = 1;
exclude = sub2ind([nRows nCols nDeps],...
        repmat(1:nRows,[1 nDeps]),...
        repmat(nCols,[1 nRows*nDeps]),...
        cumsum(tmp)); % No right edge for last column
ind = setdiff(ind,exclude);
edges = [edges [ind;ind+nRows]];

% Add Back Edges
ind = 1:nNodes;
tmp = zeros(1,nRows*nCols);
tmp(1:nRows:nRows*nCols) = 1;
exclude = sub2ind([nRows nCols nDeps],...
        repmat(1:nRows,[1 nCols]),...
        cumsum(tmp),...
        repmat(nDeps,[1 nRows*nCols])); % No right edge for last column
ind = setdiff(ind,exclude);
edges = [edges [ind;ind+(nRows*nCols)]];

clearvars -except edges nStates nRows nCols nDeps nNodes
if isempty(edges)
    adj = sparse(nNodes,nNodes);
else
    edges = [edges(1,:) edges(2,:); edges(2,:) edges(1,:)];
    adj = sparse(edges(1,:), edges(2,:), ones(1,size(edges,2)));
end
clearvars -except adj nStates nRows nCols nDeps
blockStruct = UGM_makeEdgeStruct(adj,nStates);
clearvars -except blockStruct nRows nCols nDeps
blockStruct.nRows = nRows;
blockStruct.nCols = nCols;
blockStruct.nDeps = nDeps;
end

