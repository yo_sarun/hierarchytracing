function [ tube, mask, ev, blockprob ] = BLK_update( blockStruct, blksize, U, tube, options )
%BLK_UPDATE compute probability map from snake-level and block-level energy
%     Inputs
%     blockStruct     graphical model structure
%     blksize         block size e.g. [nRows nCols nDeps]
%     U               3D array image stack
%     tube            3D array of pixel-level probability map of superblock
%     options         utility options
%     
%     Outputs
%     tube            probability map that combind pixel-and-snake level
%     mask            binary image of neuron
%     ev              eigenvector field of tube i.e. 1-by-3 cell array of 3D array
%     blockprob       probability map at block level
%
%

if~ismember(nargin, 4:5)
    error('Invalid inputs to BLK_update!')    
end

defaultoptions = struct('FrangiScaleRange', [1 8], 'C_EdgeE', 5, 'Fcount', 0);
% Process inputs
if(~exist('options','var')), 
    options = defaultoptions; 
else
    options = get_options(defaultoptions, options);
end

%% get Association Potential
nodeE = zeros(1,blockStruct.nNodes);
for k = 1:blockStruct.nNodes
    nodeE(k) = BLK_get_nodeE(U, tube, k, blockStruct, blksize);
end
nodePot = nodeE';
nodePot = [1-nodePot, nodePot];
nodePot = log(nodePot);

%% get Interaction Potential
edgeEnds = blockStruct.edgeEnds;
n1 = edgeEnds(:,1);
n2 = edgeEnds(:,2);
edgeE = exp(-options.C_EdgeE*abs(nodeE(n1) - nodeE(n2)));

edgePot = zeros(2,2,blockStruct.nEdges);
edgePot(1,1,:) = edgeE;
edgePot(1,2,:) = -edgeE;
edgePot(2,1,:) = -edgeE;
edgePot(2,2,:) = edgeE;

%% inferrence
yDecode = UGM_Decode_GraphCut(nodePot,edgePot,blockStruct);

DRF_infer_GC_c(nodePot,edgePot,edgeEnds,yDecode);
pot = exp(nodePot);
pot = pot./repmat(sum(pot,2),[1 2]);
pot(isnan(pot)) = 1.0;
blockprob = reshape(pot(:,2), blockStruct.nRows, blockStruct.nCols);

for i = 1:blockStruct.nRows
    for j = 1:blockStruct.nCols
        row = ((i-1)*blksize(1));
        col = ((j-1)*blksize(2));
        rowt = min(row+blksize(1), size(tube,1));
        colt = min(col+blksize(2), size(tube,2));
        tube(row+1:rowt,col+1:colt,:) = blockprob(i,j) * ...
                tube(row+1:rowt,col+1:colt,:);
    end
end

mask = tube>0.5;
if options.Fcount > 0
    disp(blockprob);
end

%% Compute Frangi to find ev
if nargout == 3
    ev = cell(1,3);
    clearvars -except tube mask blockprob options
    ms = options.FrangiScaleRange(2);
    s = size(tube);
    frangi_opts.FrangiScaleRange = options.FrangiScaleRange;
    frangi_opts.FrangiScaleRatio = 2;
    frangi_opts.FrangiAlpha = .5;
    frangi_opts.FrangiBeta = .5;
    frangi_opts.FrangiC = 500;
    frangi_opts.BlackWhite = false;
    frangi_opts.verbose = false;
    newtube = zeros(s+(ms+3)*2);
    newtube(ms+4:ms+3+s(1), ms+4:ms+3+s(2), ms+4:ms+3+s(3)) = tube;
    [~,~, Vx,Vy,Vz] = FrangiFilter3D(newtube,frangi_opts);
    Vx = Vx(ms+4:ms+3+s(1), ms+4:ms+3+s(2), ms+4:ms+3+s(3));
    Vy = Vy(ms+4:ms+3+s(1), ms+4:ms+3+s(2), ms+4:ms+3+s(3));
    Vz = Vz(ms+4:ms+3+s(1), ms+4:ms+3+s(2), ms+4:ms+3+s(3));
    ev{1} = double(Vy);
    ev{2} = double(Vx);
    ev{3} = double(Vz);
end

end

