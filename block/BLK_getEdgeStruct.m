function edgeStruct = BLK_getEdgeStruct( s )
% BLK_GETEDGESTRUCT generate edgeStruct of block with size s
%     Inputs
%     s           size of block i.e. [rRows, n
%     
%     Outputs
%     edgeStruct  edgeStruct of graphical model
%

nRows = s(1);
nCols = s(2);
nDeps = s(3);
nNodes = nRows*nCols*nDeps;
nStates = 2;

%% Make edgeStruct
% Add Down Edges
ind = 1:nNodes;
tmp = zeros(1,nCols*nDeps);
tmp(1:nCols:nCols*nDeps) = 1;
exclude = sub2ind([nRows nCols nDeps],...
        repmat(nRows,[1 nCols*nDeps]),...
        repmat(1:nCols, [1 nDeps]),...
        cumsum(tmp)); % No Down edge for last row
ind = setdiff(ind,exclude);
edges = [ind;ind+1];

% Add Right Edges
ind = 1:nNodes;
tmp = zeros(1,nRows*nDeps);
tmp(1:nRows:nRows*nDeps) = 1;
exclude = sub2ind([nRows nCols nDeps],...
        repmat(1:nRows,[1 nDeps]),...
        repmat(nCols,[1 nRows*nDeps]),...
        cumsum(tmp)); % No right edge for last column
ind = setdiff(ind,exclude);
edges = [edges [ind;ind+nRows]];

% Add Back Edges
ind = 1:nNodes;
tmp = zeros(1,nRows*nCols);
tmp(1:nRows:nRows*nCols) = 1;
exclude = sub2ind([nRows nCols nDeps],...
        repmat(1:nRows,[1 nCols]),...
        cumsum(tmp),...
        repmat(nDeps,[1 nRows*nCols])); % No right edge for last column
ind = setdiff(ind,exclude);
edges = [edges [ind;ind+(nRows*nCols)]];

clearvars -except edges nStates
edges = [edges(1,:) edges(2,:); edges(2,:) edges(1,:)];
adj = sparse(edges(1,:), edges(2,:), ones(1,size(edges,2)));

clearvars -except adj nStates
edgeStruct = UGM_makeEdgeStruct(adj,nStates);


end

