function [ result ] = BLK_get_nodeE(U, tube, n, blockStruct, blksize)
%BLK_GET_NODEE get potential of node containing part of neuron
%     Input
%     U             3D array image stack
%     tube          3D array of pixel-level probability map of superblock
%     n             block number
%     blockStruct   DRF graphical model
%     blksize       size of block
%     
%     Output
%     result        potential of node containing part of neuron (association potential)
%

i = mod(n-1,blockStruct.nRows)+1;
j = ceil(single(n)/blockStruct.nRows);

row = ((i-1)*blksize(1));
col = ((j-1)*blksize(2));
rowt = min(row+blksize(1), size(tube,1));
colt = min(col+blksize(2), size(tube,2));
% Egvf = Egvf(row+1:rowt, col+1:colt, :);
% Eblock = Eblock(row+1:rowt, col+1:colt, :);
% 
% r = Eblock./Egvf;
% tmp = mean(r(isfinite(r)));
% 
% if isfinite(tmp)
%     result = min(tmp*2, 1); % cap at 1
% else
%     result = 0;
% end
U = U(row+1:rowt, col+1:colt, :);
mask = tube(row+1:rowt, col+1:colt, :) > 0.5;
% keyboard;
if isnan(mean(U(mask))) || isnan(mean(U(mask)))
    result = 0;
else
    result = max(min((mean(U(mask)) - mean(U(~mask))) / 50,1),0);
end
end

