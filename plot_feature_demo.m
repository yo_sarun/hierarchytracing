% read swc file and compute feature vectors

clear all
close all
clc

imgdir = './img/';
resultdir = './result/';

load './utils/cmap.mat'

Drosimgdir = './img/';
Drosswcdir = './swc/';

addpath('Emmanuel')
addpath('Eve')
addpath('Samantha')
addpath('Yo')
addpath(genpath('lib'))
addpath(genpath('utils'))

% set(0,'DefaultFigureWindowStyle','docked')
set(0,'DefaultFigureWindowStyle','normal')
%%
swcname = dir([Drosswcdir 'yoyon*.swc']);
imgname = dir([imgdir '*-*.mat']);

i = 1;

disp(['Loading: ' Drosswcdir swcname(i).name])
load([imgdir imgname(i).name], 'V')
data = read_swc_file([Drosswcdir swcname(i).name]);

[~, ~, ~, center] = eroding(V,250,10);

%%
data = fix_rootnode(data, V, center);
v = get_feature_vector(data, V, 'soma', center(1:2));

%%
attrs = fieldnames(v);
for attr = attrs'
    str = attr{1};
    disp(str)
    plot_feature(v.(str));
    pause(1);
end