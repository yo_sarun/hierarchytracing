% Old preprocessing step for dataset from Nima


clearvars -except datapath datapaths imgdir imgname snkdir swcdir sample_factor
close all
clc

addpath('./AMT');
addpath('./CSF');
addpath('./utils');
addpath('./PreprocessLib');
addpath('./oof3response');
addpath(genpath('./CurveLab-2.1.3'));

% set(0,'DefaultFigureWindowStyle','normal')
set(0,'DefaultFigureWindowStyle','docked')

%% Read Image
disp('Reading images ...')
currentpath = cd;
% [~, datapath] = uigetfiles;
% datapath = '/Users/Yo/Desktop/NeuronTracing/CNGxPNC_E1H15_1-30-14 (0-948)/';
% datapath = 'C:\Neuron\NeuronTracing\CNGxWNC_E1H16_12-27-13_(0-1271)\';
% datapath = '/Users/Yo/Desktop/NeuronTracing/CNGxWNC_E1H16_12-27-13_(0-1271)/';
% datapath = '/Users/Yo/Desktop/NeuronTracing/3-1-14_CNGxWNC_H16 Timelapse/';
% imgdir = './Yo/';

cd(datapath)
im_names = dir('*.tif');
FNUM = length(im_names);
for fcount = 1:FNUM,
    X = imread(im_names(fcount).name,'tif');
    if ndims(X) == 3
        X = rgb2gray(X);
    end
    X = single(X);
    X = X-min(X(:)); X = X/max(X(:)); X = uint8(round(X*255.0));
    V(:,:,fcount)=X;
end
cd(currentpath)

% get name of save file
C = regexp(strrep(datapath, '\', '/'),'/','split');
C = C(~cellfun(@(x)isempty(x), C));
savefile = ['n', C{end}, '.mat'];
figure(1)
imshow3D(V)

%% sharpen image
V_sharp = double(V);% - mean(V(:));
for fcount = 1:FNUM
    K = V_sharp(:,:,fcount);
%     I = adapthisteq(K./255,'NumTiles', [8 8],'ClipLimit',0.005)*255;
    alpha = 0.2; % Controls shape of filter - Between [0,1]
    f = fspecial('unsharp', alpha); % Create mask
    I2 = imfilter(K, f); % Filter the image
%     I2 = imsharpen(I);
    I3 = medfilt2(I2,[3 3]);
    I4 = imadjust(I3./max(I3(:)));
%     I3 = adapthisteq(I2./max(I2(:)),'NumTiles', [8 8],'ClipLimit',0.005)*255;
    V_sharp(:,:,fcount) = I4;
    
%     figure(31)
%     subplot(2,2,1), imshow(K,[]); title('original image')
%     subplot(2,2,2), imshow(I2,[]); title('sharpeni image')
%     subplot(2,2,3), imshow(I3,[]); title('median filter image')
%     subplot(2,2,4), imshow(I4); title('imadjust image')
%     pause
end
V_sharp(V_sharp<0) = 0;
V_sharp = V_sharp .* 255 ./ max(V_sharp(:));
figure(2)
imshow3D(V_sharp)

%% enhance circular object
V_circle = zeros(size(V));
for fcount = 1:FNUM
    I = V_sharp(:,:,fcount);
    figure(32)
    imshow(I,[])
    Rmin = 5;
    Rmax = 20;
    [centersBright, radiiBright] = imfindcircles(I,[Rmin Rmax],'ObjectPolarity','bright');
    
    viscircles(centersBright, radiiBright,'EdgeColor','b');
    
    [row, col] = size(I);
    hoho = zeros(size(I));
    [X,Y] = meshgrid(1:col,1:row);
    for i = 1:size(centersBright,1)
        distFromCenter = sqrt((X-centersBright(i,1)).^2 + (Y-centersBright(i,2)).^2);
        onPixels = abs(distFromCenter-radiiBright(i)) < 1.5;
        idx = find(onPixels); % THIS is your answer!
        hoho(idx) = 200;
    end
    I_circle = hoho;
    V_circle(:,:,fcount) = I_circle;
%     pause
end
figure(3)
imshow3D(V_circle);


%% parameters
sigCurv=10;

S_csf = 2;

BG_median_thr = 30;

cellsize = [ceil(size(V,1)/32) ceil(size(V,2)/32)];
blksize = [32 32 FNUM];

thr_val = 50;
region_size = 200;

r_oof = 6;
opts.responsetype=5;
OOF_thr = 3;


%% LoG
hoho = V_sharp + V_circle;
% hoho(logical(V_circle)) = max(hoho(:));
% V_csf = CSF_cos(uint8(V_sharp),S_csf);
% V_csf = CSF_cos(uint8(V_adj),S_csf);
% V_csf = CSF_cos(uint8(curvelets),S_csf);
V_csf = CSF_cos(uint8(hoho),2);
% V_csf = CSF_cos(uint8(V_circle),2);
% V_csf_circle = min(double(V_csf)+V_circle,255*ones(size(V)));
figure(4)
% imshow3D(double(V_csf)+V_circle)
imshow3D(V_csf)






% %% Curvelet Preprocessing
% curvelets=zeros(size(V_csf));
% for i=1:FNUM  
%     disp(['Curvelets #',num2str(i),' started']);
%     [curvelets(:,:,i)]=NfdctDemo(V_csf(:,:,i),20);
% end
% figure(2)
% imshow3D(curvelets)

%% heuristic: BG subtraction
V_bg_sub_cell = cell(cellsize);
V_bg_sub_reg_cell = cell(cellsize);
disp('BG subtraction... Start')
for a = 1:prod(cellsize)
    [i,j] = ind2sub(cellsize,a);
    row = (i-1)*32+1;
    col = (j-1)*32+1;
    rowt = min(row+blksize(1)-1, size(V,1));
    colt = min(col+blksize(1)-1, size(V,2));
    for fcount = 1:FNUM,
        block = double(V_csf(row:rowt,col:colt,fcount));
        bg = median(block(:));
        new_block = block-bg;
        new_block(new_block < BG_median_thr) = 0;
        V_bg_sub_cell{a}(:,:,fcount) = new_block;
    end
end
disp('BG subtraction... Finish')

V_bg_sub = zeros(size(V));
for a = 1:prod(cellsize)
    [i,j] = ind2sub(cellsize,a);
    row = (i-1)*32+1;
    col = (j-1)*32+1;
    rowt = min(row+blksize(1)-1, size(V,1));
    colt = min(col+blksize(1)-1, size(V,2));
    level = graythresh(V_bg_sub_cell{a});
    V_bg_sub(row:rowt,col:colt,:) = V_bg_sub_cell{a};
end

figure(5);
imshow3D(V_bg_sub);
% drawnow;
% V_bg_sub = V_csf_circle;

%% Small Region Filter
V_thr = Threshold(V_bg_sub,thr_val);
V_reg = EliminateSmallReg(V_bg_sub,200);
figure(6)
imshow3D(V_reg);

%% OOF
disp('OOF started');
s = size(V_reg);
newE = zeros(s+(r_oof+3)*2);
newE(r_oof+4:r_oof+3+s(1), r_oof+4:r_oof+3+s(2), r_oof+4:r_oof+3+s(3)) = V_reg;
Eoof_block = oof3response(newE,1:r_oof,opts);
disp('OOF finished');

%% detect seed points by ridge points
figure(7)
imshow3D(Eoof_block)
figure(8)
imshow3D(Eoof_block>OOF_thr)

%%
[fx,fy,~] = AM_gradient(Eoof_block);
seedsx = fx(:,1:end-1,:)>0&fx(:,2:end,:)<0&Eoof_block(:,1:end-1,:) > OOF_thr;
seedsy = fy(1:end-1,:,:)>0&fy(2:end,:,:)<0&Eoof_block(1:end-1,:,:) > OOF_thr;
seeds = seedsx(1:end-1,:,:)&seedsy(:,1:end-1,:);

figure(9)
imshow3D(seeds)
seeds = padarray(seeds,[1 1],'symmetric','post');


%% Save filtered image and seed points
smooth = single(V_bg_sub);
if mkdir(imgdir)
    save ([imgdir savefile], 'V', 'smooth', 'seeds', 'blksize', 'cellsize', '-v7.3');
    disp('Save');
else
    error('ERROR: cannot save preprocessed image stack');
end

%% Show seed points
for fcount = 1:FNUM
    figure(10)
    imshow(V_bg_sub(:,:,fcount),[])
    [py, px] = find(seeds(:,:,fcount));
    hold on;
    plot(px,py,'b+')
    hold off;
    title(fcount)
%     pause;
end

