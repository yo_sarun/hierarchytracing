% Test tracing snake at block level for toy image

clear all
close all
clc

addpath('./AMT')
addpath('./frangi/')
addpath('./utils')
addpath('./DRF')
addpath('./snake')
addpath('./block')
addpath('./topology')
addpath('./ba_interp3')
addpath(genpath('./toolbox_fast_marching'))
addpath(genpath('./UGM'))

load 'toy.mat'
load 'snkDRF_topo_toy.mat'

%%
disp('======================================')
disp('Open Snake Curve with DRF at Block level on Toy Image Stack')

r = 0;
c = 0;
row = (r*32)+1;
col = (c*32)+1;
U = double(V);

%%
disp('--------------------------------------------------')
disp('Computing GVF external force field ...')
tic;
mu = .2;
GVF_ITER = 10;
h = fspecial('gaussian',[5 5],5);
f = imfilter(U,h);
Fexts = AM_GVF(f, mu, GVF_ITER, 1);    % for toy image
Fext = cell(1,3);
for i = 1:3
    Fext{i} = double(Fexts(:,:,:,i));
end
toc;
%% get snake edge Struct
disp('Generate Edge Struct for Snake Level ...')
nRows = 3;
nCols = 3;
nDeps = 1;
tic;
blockStruct = BLK_getStruct(nRows, nCols, nDeps);
toc;

%%
level = 1;
options.Fcount = fcount;
options.LengthThreshold = size(U,1)/10;
[bsnk, btube, bSs, bCs] = ...
        BLK_propagate_topo( U, allsnk, blockStruct...
        , blksize, tubes, Fext ...
        , Ss, Cs, r, c, level, options );