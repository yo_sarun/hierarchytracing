% Preprocessing step for CF dataset from DIADEM Challenge for Roysam algorithm

clear all
close all
clc

addpath('./AMT');
addpath('./CSF');
addpath('./utils');
addpath('./PreprocessLib');
addpath('./oof3response');
addpath(genpath('./CurveLab-2.1.3'));

set(0,'DefaultFigureWindowStyle','docked')

%% Read Image
disp('Reading images ...')
currentpath = cd;
% [~, datapath] = uigetfiles;
datapath = '/Users/Yo/Desktop/Cerebellar Climbing Fibers/Image Stacks/CF_2/';

cd(datapath)
im_names = dir('*.tif');
FNUM = length(im_names);
for fcount = 1:FNUM,
    X1 = imread(im_names(fcount).name,'tif');
    X = double(X1);
    if ndims(X) == 3
        L = medfilt2(double(X(:,:,3)),[5 5]);
        f = fspecial('gaussian',[10 10],10);
        Xb = imfilter(L,f);
        tmp = X(:,:,2) + 2*(Xb - X(:,:,1));
        grayIm = min(tmp,255*ones(size(tmp)));
        grayIm(grayIm<0) = 0;
        X = grayIm;
        X = downsample(X',4);
        X = downsample(X',4);
    end
    X = single(X);
    X = X-min(X(:)); X = X/max(X(:)); X = uint8(round(X*255.0));
    V(:,:,fcount)=X;
end
cd(currentpath)

% get name of save file
C = regexp(strrep(datapath, '\', '/'),'/','split');
C = C(~cellfun(@(x)isempty(x), C));
savefile = ['n', C{end}, '.mat'];

if ~isempty(regexp(datapath,'CF'))
    V = imcomplement(V);
end

%%
figure(1)
imshow3D(V)

%%
V_adj = zeros(size(V));
for fcount = 1:size(V,3)
    V_adj(:,:,fcount) = imadjust(V(:,:,fcount),[0.45,1.0],[0,1]);
end
figure(2)
imshow3D(V_adj)

%%
f = fspecial('gaussian',[10 10], 10);
V_con = imfilter(double(V_adj),f);
figure(3)
imshow3D(V_con)


%% Curvelet Preprocessing
curvelets=zeros(size(V));
for i=1:FNUM  
    disp(['Curvelets #',num2str(i),' started']);
    [curvelets(:,:,i)]=NfdctDemo(V_con(:,:,i),3);
end
figure(4)
imshow3D(curvelets)

%%
smooth = curvelets;
smooth(smooth<0) = 0;
smooth = smooth .* 255 ./ max(smooth(:));
figure(5)
imshow3D(smooth)
%%
save (['./Yo/roy' savefile], 'V', 'smooth', '-v7.3');
disp('Save');
%%
im = uint8(round(smooth));
imwrite(im(:,:,1), ['yo', C{end}, '.tif'])
for i = 2:FNUM
    imwrite(im(:,:,i), ['yo', C{end}, '.tif'], 'writemode', 'append')
end
disp('SAVE')