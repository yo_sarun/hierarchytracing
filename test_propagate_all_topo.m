% Trace snake at every scale of block level
function [swcdir, fixswcfile, ptitles, myCallback] = test_propagate_all_topo(...
        handles, V, allsnk, tubes, blksize, Ss, Cs, level, options)

imgfile = [handles.analyze_pathname handles.analyze_filename];
imgname = handles.analyze_filename;
ptitles = cell(1,1);
myCallback = cell(1,1);
cur = 1;

%% compute GVF
C = regexp(imgname,'\.','split');
imgname = C{1};
GVFfile = [handles.imggvf imgname '_GVF.mat'];
if ~(exist(GVFfile, 'file') == 2)
    mu = .2;
    GVF_ITER = 10;
    h = fspecial('gaussian',[5 5],5);
    matObj = matfile(imgfile);
    f = imfilter(double(matObj.smooth),h);
    Fexts = AM_GVF(f, mu, GVF_ITER, 1);
    mkdir(handles.imggvf);
    save(GVFfile, 'Fexts', '-v7.3');
end

%% do on whole image
blockStruct_default = BLK_getStruct(3, 3, 1);
while size(allsnk,1) > 1 || size(allsnk,2) > 1
    if ~handles.script
        handles.jTextArea.append(['Trace Open Snake at Block Level: ' num2str(size(allsnk)) char(10)]);
        handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength);
    else
        disp(['Trace Open Snake at Block Level: ' num2str(size(allsnk))]);
    end
    bsnk = cell(ceil(size(allsnk)/3));
    btube = cell(ceil(size(allsnk)/3));
    bSs = zeros(ceil(size(allsnk)/3));
    bCs = cell(ceil(size(allsnk)/3));
    bsize = size(bsnk);
    blk1 = blksize(1);
    blk2 = blksize(2);
    
    parfor a = 1:numel(bsnk)
        [i,j] = ind2sub(bsize,a);
        i = (i-1)*3;
        j = (j-1)*3;
        nRows = 3;
        if i+3 > size(allsnk,1)
            nRows = size(allsnk,1)-i;
        end
        nCols = 3;
        if j+3 > size(allsnk,2)
            nCols = size(allsnk,2)-j;
        end
        if nRows == 3 && nCols == 3
            blockStruct = blockStruct_default;
        else
            blockStruct = BLK_getStruct(nRows, nCols, 1);
        end
        row = (i*blk1)+1;
        col = (j*blk2)+1;
        rowt = min(row+blk1*nRows-1, size(V,1));
        colt = min(col+blk2*nCols-1, size(V,2));
        matObj = matfile(imgfile);
        U = matObj.smooth(row:rowt, col:colt, :);
        
        % get GVF
        GVFobj = matfile(GVFfile);
        Fext = cell(1,3);
        for n = 1:3
            Fext{n} = double(GVFobj.Fexts(row:rowt,col:colt,:,n));
        end
        
        opt = options;
        if ~isfield(opt, 'LengthThreshold')
            opt.LengthThreshold = size(U,1)/10;%min(size(U,1)/20,20);
        end
        [bsnk{a}, btube{a}, bSs(a), bCs{a}] = ...
                BLK_propagate_topo( U, allsnk, blockStruct...
                , blksize, tubes, Fext ...
                , Ss, Cs, i, j, level, opt );
    end
    
    % replace with new value for new resolution
    blksize = blksize.*[3 3 1];
    allsnk = bsnk;
    tubes = btube;
    Ss = bSs;
    Cs = bCs;
    level = level+1;
    
    % show result
    if handles.script
        h = figure;
        handles.axes1 = cla;
    end
    SNK_show(allsnk, blksize, 'jet', 1, V, ...
            ['Snake Block size ' num2str(size(allsnk))], handles.axes1);
    ptitles{cur,1} = [num2str(size(allsnk,1)) '-by-' num2str(size(allsnk,2))];
    myCallback{cur,1} = {@SNK_show, allsnk, blksize, 'jet', 1, V, ...
            ['Snake Block size ' num2str(size(allsnk))], handles.axes1};  
    cur = cur+1;
    if handles.script
        saveas(h, [handles.savedir imgname '_block'  num2str(size(allsnk,1)) '.png'], 'png');
    end
end

snkdir = handles.snkdir;
savefile = [snkdir 'blkDRF_' imgname '_' num2str(max(size(bsnk))) '.mat'];
if mkdir(snkdir)
    save(savefile, 'allsnk', 'tubes', 'blksize', 'Ss', 'Cs', 'level', '-v7.3');
    if ~handles.script
        handles.jTextArea.append(['Set of Snake file saved to ' savefile char(10)]);
        handles.jTextArea.setCaretPosition(handles.jTextArea.getDocument.getLength);
    else
        disp(['Set of Snake file saved to ' savefile]);
    end
else
    error('ERROR: cannot save block tracing result level %d', max(size(bsnk)));
end

%%
load(imgfile, 'sample_factor');
swcdir = handles.swcdir;
mkdir(swcdir);
swcfile = [swcdir 'my' imgname '.swc'];
fixswcfile = ['yoyo' imgname '.swc'];
export_swc(allsnk{1}, swcfile, V, sample_factor);
fix_swc(swcfile, [swcdir fixswcfile], V, sample_factor);
end