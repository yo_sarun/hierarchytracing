% Get image region from image stack for evaluation

clearvars -except savedir datapath
clc

addpath('./utils');

% set(0,'DefaultFigureWindowStyle','normal')
set(0,'DefaultFigureWindowStyle','docked')

%% Read Image
disp('Reading images ...')
savedir = '../tracefiles/image/';
currentpath = cd;
% [~, datapath] = uigetfiles;
datapath = 'C:/Neuron/72413/72413_h0/';
% datapath = '/Users/Yo/Desktop/Neuron/72513/72513_h0/';

cd(datapath)
im_names = dir('*.tif');
FNUM = length(im_names);
for fcount = 1:FNUM,
    X = imread(im_names(fcount).name,'tif');
    if ndims(X) == 3
        X = rgb2gray(X);
    end
    X = single(X);
    X = X-min(X(:)); X = X/max(X(:)); X = uint8(round(X*255.0));
    V(:,:,fcount)=X;
end
cd(currentpath)

% get name of save file
C = regexp(strrep(datapath, '\', '/'),'/','split');
C = C(~cellfun(@(x)isempty(x), C));
savefile = ['img', C{end}, '.tif'];


%% show selected image region
figure(1), clf, imshow3D(V);
% - 72413_h0
l = 401;
r = 700;
u = 551;
d = 800;
% % - 72513_h0
% l = 451;
% r = 750;
% u = 301;
% d = 500;
U = V(u:d,l:r,:);
figure(2), clf, imshow3D(U);

%% save as image slide
if mkdir([savedir 'img', C{end}]);
    for i = 1:FNUM
        imwrite(U(:,:,i), [savedir 'img', C{end}, '/', sprintf('%02d',i),'.tif']);
    end
    disp('SAVE')
else
    error('ERROR: cannot save image slice');
end

%% save as image stack
imwrite(U(:,:,1), [savedir savefile]);
for i = 2:FNUM
    imwrite(U(:,:,i), [savedir savefile], 'writemode', 'append');
end
disp('SAVE')