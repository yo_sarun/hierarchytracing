function [yDecode, pot] = DRF_infer_GC( theta, nodes, edges, edgeStruct )
% DRF_INFER_CG use graph-cut to infer probability from DRF parameters
%     Inputs
%     theta       DRF parameters
%     nodes       feature vector of each node
%     edges       feature vector of each edge
%     edgeStruct  graphical model of DRF
%     
%     Outputs
%     yDecode     decoding result of model
%     pot         probability of assigning label of model
% 

%% Compute nodePot and edgePot
A = glmval(theta(1:2), nodes, 'logit');
nodePot = [1-A, A];
nodePot = log(nodePot);

I = (theta(3:4)'*[ones(size(edges)), edges]')';
I(I < 0) = 0;
edgePot = zeros(2,2,edgeStruct.nEdges); 
edgePot(1,1,:) = I;
edgePot(1,2,:) = -I;
edgePot(2,1,:) = -I;
edgePot(2,2,:) = I;

%% Evaluate with learned parameters
clearvars -except nodePot edgePot edgeStruct
yDecode = UGM_Decode_GraphCut(nodePot,edgePot,edgeStruct);

if nargout == 2
    edgeEnds = edgeStruct.edgeEnds;    
    DRF_infer_GC_c(nodePot,edgePot,edgeEnds,yDecode);
    pot = exp(nodePot);
    pot = pot./repmat(sum(pot,2),[1 2]); 
end

end