function f = DRF_objfunGC(theta, nodes, edges, ising, label, edgeStruct)
% DRF_OBJFUNGC compute the objective function of DRF using Graph-cut
%	  Input
%	  theta				DRF initial parameters
%	  nodes				association potential
%	  edges				interaction potential
%	  ising				label of edges
%	  label				label of nodes
%	  edgeStruct		graphical model struct
%
%	  Output
%	  f					value of objective function
%
A = glmval(theta(1:2), nodes, 'logit');
I = (theta(3:4)'*[ones(size(edges)), edges]')';

tau = 0.5;
regularize = (theta(3:4)'*theta(3:4))/(2*tau^2);
% regularize = (theta'*theta)/(2*tau^2);
nodes = double(nodes);
edges = double(edges);

ydecode = DRF_infer_GC( theta, nodes, edges, edgeStruct);
maskA = logical(ydecode-1); 
maskI = maskA(edgeStruct.edgeEnds(:,1))==maskA(edgeStruct.edgeEnds(:,2));
logz = sum(log(A(maskA))) + sum(log(1-A(~maskA))) + sum(I(maskI)) + sum(-I(~maskI));
clearvars -except A I label ising logz regularize
A(~label) = 1-A(~label);
I = ising.*I;

pi = sum(log(A)) + sum(I) - logz - regularize;
clearvars -except pi
f = -double(pi);
disp(f);
end

