function f = DRF_objfun(theta, nodes, edges, ising, label)
% DRF_OBJFUNGC compute the objective function of DRF using ICM
%	  Input
%	  theta				DRF initial parameters
%	  nodes				association potential
%	  edges				interaction potential
%	  ising				label of edges
%	  label				label of nodes
%
%	  Output
%	  f					value of objective function
%
A = glmval(theta(1:2), nodes, 'logit');
A(~label) = 1-A(~label);

edgeProb = glmval(theta(3:4), edges, 'logit');
edgeProb(ising==-1) = 1-edgeProb(ising==-1);
K = 0.5;
B = 0.5;
I = B*(K*ising + (1-K)*(2*edgeProb-1));

pi = sum(log(A)) + sum(I);
f = -pi;
end

