function [ BA, BI ] = DRF_init( E, seed, U, display )
% DRF_INIT get initial parameters of A and I potentials
%     Inputs
%     E           3D array GVF energy of each node
%     seed        3D array seed points for foreground samples
%     U           3D array image stack
%     display     showing result for debugging
%     
%     Outputs
%     BA          initial parameters of Association potential
%     BI          initial parameters of Interaction potential
%

if nargin < 4
    display = 0;
end

% pixel level DRF
%% init Association Potential
s = size(E);
fgGVF = E(seed==1);
fgGVF = fgGVF(U(logical(seed))>0.5);
bgGVF = reshape(E(:,:,23),s(1)*s(2),1);
l = [zeros(size(bgGVF,1),1); ones(size(fgGVF,1),1)];
BA = glmfit([bgGVF; fgGVF], l, 'binomial', 'link', 'logit');

% evaluate on all voxels
energy = reshape(E, s(1)*s(2)*s(3), 1);
pihat = glmval(BA, energy, 'logit');
mask = reshape(pihat>0.5, s(1), s(2), s(3));

%% init Interaction Potential
% get sample for depth = 10
label = mask*2-1;
ising = label(:,1:end-1,10).*label(:,2:end,10);
y = (ising+1)/2;
% need to change to Bhattachayya distance
feat = abs(E(:,1:end-1,10)-E(:,2:end,10));

x = reshape(feat, s(1)*(s(2)-1), 1);
y = logical(reshape(y, s(1)*(s(2)-1), 1));
BI = glmfit(x, y, 'binomial', 'link', 'logit');

if display
    pihat = glmval(BI, x, 'logit');
    result = reshape(pihat, s(1), s(2)-1);
    k = 0.5;
    b = 0.5;
    IP = b*(k*ising + (1-k)*(2*result-1));

    figure
    imagesc(IP)
    colormap gray;
end


end