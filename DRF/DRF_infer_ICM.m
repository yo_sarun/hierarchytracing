function [ yDecode, prob ] = DRF_infer_ICM( s, theta, nodes, edges, edgeStruct, display )
% DRF_INFER_ICM use ICM to train DRF parameters based on pseudo-likelihood
%     Inputs
%     s           size of block i.e. [nRows nCols nDeps]
%     theta       DRF parameters
%     nodes       feature vector of each node
%     edges       feature vector of each edge
%     edgeStruct  graphical model of DRF
%     display     showing result for debugging
%     
%     Outputs
%     yDecode     decoding result of model
%     prob        probability of assigning label of model
% 

if nargin < 6
    display = 0;
end

nRows = s(1);
nCols = s(2);
nDeps = s(3);
nStates = 2;
nEdges = edgeStruct.nEdges;
%% Compute nodePot and edgePot
pihat = glmval(theta(1:2), nodes, 'logit');
nodePot = [1-pihat, pihat];
nodePot = double(nodePot);

% Make (non-negative) potential of each edge taking each state combination
K = 0.5;
B = 0.5;
pihat = glmval(theta(3:4), edges, 'logit');
ising = repmat(eye(2),[1 1 edgeStruct.nEdges]);
ising(ising==0) = -1;
edgePot = zeros(2,2,edgeStruct.nEdges);
edgePot(1,1,:) = pihat;
edgePot(1,2,:) = 1-pihat;
edgePot(2,1,:) = 1-pihat;
edgePot(2,2,:) = pihat;
edgePot = exp(B*((K*ising) + ((1-K)*(2*edgePot-1))));
edgePot = double(edgePot);
% for e = 1:nEdges
% 	n1 = edgeStruct.edgeEnds(e,1);
% 	n2 = edgeStruct.edgeEnds(e,2);
% 	for s1 = 1:nStates
% 		for s2 = 1:nStates
% 			edgePot(s1,s2,e) = 1;
% 		end
% 	end
% end
%% Evaluate with learned parameters
tic
fprintf('ICM Decoding with estimated parameters...\n');
[yDecode, pot] = UGM_Decode_ICM(nodePot,edgePot,edgeStruct);
toc

%%
yDecode = logical(reshape(yDecode, nRows, nCols, nDeps));
normpot = pot./repmat(sum(pot,2),[1 2]); 
prob = reshape(normpot(:,2), nRows, nCols, nDeps);
% keyboard;
if display
    figure;
    imshow3D(prob)
end

end