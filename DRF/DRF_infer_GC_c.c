#include <math.h>
#include "mex.h"


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	/* Variables */
	int n1,n2,e,i,nNodes,nEdges,*edgeEnds,*yDecode;
	double *pot,*edgePot;
    
	if(nrhs != 4) 
        mexErrMsgTxt("Wrong number of input arguments.");
    
	/* Inputs */
	pot = mxGetPr(prhs[0]);
	edgePot = mxGetPr(prhs[1]);
	edgeEnds = (int*)mxGetPr(prhs[2]);
    yDecode = (int*)mxGetPr(prhs[3]);
	
	if (!mxIsClass(prhs[2],"int32"))
        mexErrMsgTxt("edgeEnds must be int32");
    
    if (!mxIsClass(prhs[3],"int32"))
        mexErrMsgTxt("yDecode must be int32");
	
	nNodes = mxGetDimensions(prhs[0])[0];
	nEdges = mxGetDimensions(prhs[2])[0];
	
	for(e=0;e<nEdges;e++) {
		n1 = edgeEnds[e]-1;
		n2 = edgeEnds[e+nEdges]-1;
        
        pot[n1] += edgePot[0 + 2*((yDecode[n2]-1) + 2*e)];
        pot[n1+nNodes] += edgePot[1 + 2*((yDecode[n2]-1) + 2*e)];
        
        pot[n2] += edgePot[0 + 2*((yDecode[n1]-1) + 2*e)];
        pot[n2+nNodes] += edgePot[1 + 2*((yDecode[n1]-1) + 2*e)];
	}
}
