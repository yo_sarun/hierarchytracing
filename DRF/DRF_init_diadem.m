function [ BA, BI ] = DRF_init_diadem( E, l, display )
% DRF_INIT get initial parameters of A and I potentials from DIADEM dataset
%     Inputs
%     E           3D array GVF energy of each node
%     l           label correspond to each node
%     display     showing result for debugging
%     
%     Outputs
%     BA          initial parameters of Association potential
%     BI          initial parameters of Interaction potential
%

if nargin < 3
    display = 0;
end

if ~isa(l,'logical')
    l = logical(l);
end

%% init Association Potential
fg = E(l&E>0);
hoho = floor(rand(1000,1)*size(fg,1));
hoho(hoho<1) = 1;
fg = fg(hoho);

bg = E(~l);
hoho = floor(rand(1000,1)*size(bg,1));
hoho(hoho<1) = 1;
bg = bg(hoho);

disp('Initialise Association Potential parameters of DRF ...')
BA = glmfit([fg; bg], [ones(size(fg)); zeros(size(fg))], ...
        'binomial', 'link', 'logit');
 
if display
    % display image
    s = size(E);
    energy = reshape(E, numel(E), 1);
    pihat = glmval(BA, energy, 'logit');
    mask = reshape(pihat>0.5, s);
    figure;
    imshow3D(mask);

    % display histogram
    [y, b] = hist([fg,bg],100);

    % Consider stack for the other type:
    figure(83);
    subplot(1,2,1); bar(b,y,'stacked');
    title('FG vs BG: Sample data');
    
    E_fg = E(mask);
    E_bg = E(~mask);
    [y1, b] = hist(E_fg,20);
    y2 = hist(E_bg,b);
    
    % Consider stack for the other type:
    subplot(1,2,2); bar(b',[y1;y2]','stacked');
    title('FG vs BG: Whole image');
end

%% init Interaction Potential
label = l*2-1;
isingx = label(1:end-1,:,:).*label(2:end,:,:);
isingy = label(:,1:end-1,:).*label(:,2:end,:);
isingz = label(:,:,1:end-1).*label(:,:,2:end);
ising = [reshape(isingx, 1, numel(isingx)), ...
        reshape(isingy, 1, numel(isingy)), ...
        reshape(isingz, 1, numel(isingz))];
y = reshape(logical((ising+1)/2),numel(ising),1);

Ex = abs(E(1:end-1,:,:)-E(2:end,:,:));
Ey = abs(E(:,1:end-1,:)-E(:,2:end,:));
Ez = abs(E(:,:,1:end-1)-E(:,:,2:end));
E = [reshape(Ex, 1, numel(Ex)), ...
        reshape(Ey, 1, numel(Ey)), ...
        reshape(Ez, 1, numel(Ez))];
feat = reshape( E, numel(E), 1 );

class1 = feat(y&feat>0);
hoho = floor(rand(1000,1)*size(class1,1));
hoho(hoho<1) = 1;
class1 = class1(hoho);

class2 = feat(~y);
hoho = floor(rand(1000,1)*size(class2,1));
hoho(hoho<1) = 1;
class2 = class2(hoho);

disp('Initialise Interaction Potential parameters of DRF ...')
BI = glmfit([class1; class2], [ones(size(class1)); zeros(size(class2))], ...
        'binomial', 'link', 'logit');

if display
    s = size(Ey);
    energy = reshape(Ey, numel(Ey), 1);
    pihat = glmval(BI, energy, 'logit');
    result = reshape(pihat>0.5, s);
    figure;
    imshow3D(result), drawnow;
    
    % display histogram
    [y, b] = hist([class1,class2],20);

    % Consider stack for the other type:
    figure(84);
    subplot(1,2,1); bar(b,y,'stacked');
    title('FG vs BG: Sample data');
    E_fg = Ey(result);
    E_bg = Ey(~result);
    [y1, b] = hist(E_fg,20);
    y2 = hist(E_bg,b);
    % Consider stack for the other type:
    subplot(1,2,2); bar(b',[y1;y2]','stacked');
    title('FG vs BG: Whole image');
    drawnow;
end
end