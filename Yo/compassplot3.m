function compassplot3(feature)
%COMPASSPLOT3 plots the dendrite density in a compass graph based on the
%means of each neuron type
%Input = features - cell array of maxtrix values from neurons, Y - neuron
%type (e.g. 1-11), type - type of neuron (title of graph), str - title of subplot
%Output = compass graphs for each neuron type

n = size(feature,2);
wdir = 0:360/n:359;
rdir = wdir * pi/180;
[x,y] = pol2cart(rdir,feature);
h1 = compass(x,y);
set(h1, 'linewidth', 3);

end

