function boxplotfun3( features, Y, type, str )
%BOXPLOTFUN plots the neuron features for each type in a box plot graph
%Input = features - type of neuron feature, Y - type of neuron (eg 1-6), type - type
%of neuron, str - title of graph
%Outputs a box plot

% Defaults for this blog post
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 20;      % Fontsize
lw = 3;      % LineWidth
msz = 8;       % MarkerSize

h=figure;
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'FontSize', fsz, 'LineWidth', alw); %<- Set properties

t1 = 4;
t2 = 9;
idx = Y==t1|Y==t2;
h1 = boxplot(features(idx)', Y(idx));
set(h1,'linewidth',3, 'markersize',20);
type = type([t1 t2]);
set(gca,'Xtick',1:length(type),'XTicklabel',type');
xlabel('neuron type');
ylabel('number of leaf nodes');
% title(str);

set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperPosition', [0 0 20 30]);
saveas(h, ['b_' str], 'png');

end

