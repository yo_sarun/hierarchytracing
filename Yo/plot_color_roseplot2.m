function G = plot_color_roseplot2(data, Y, type, supertitle)
%PLOT_COLOR_ROSEPLOT: plots color roseplots
%Input:data-histogram,Y-features.mat, type-neuron type being plotted,
%supertitle- string
%Output: none
% Defaults for this blog post
width = 3;     % Width in inches
height = 3;    % Height in inches
alw = 0.75;    % AxesLineWidth
fsz = 40;      % Fontsize
lw = 1.5;      % LineWidth
msz = 8;       % MarkerSize

G = figure;
pos = get(gcf, 'Position');
set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
set(gca, 'FontSize', fsz); %<- Set properties

set(G, 'Position', [0, 0, 1500, 1000]);
for i = 8%1:length(type)
    A = data(Y==i);
    
    
    % Calculate the means and standard deviations for each variety
    means = zeros(size(A{1}));
    stds = zeros(size(A{1}));
    for I = 1:size(A{1},1)
        for C = 1:size(A{1},2)
            M = zeros(length(A),1);
            for S = 1:length(A)
                M(S) = A{S}(I,C);
            end
            means(I,C) = mean(M);
            stds (I,C) = std(M);
        end
        
    end
%     subplot(3,4,i);
   color_roseplot2(means);
%    color_roseplot(means);       % fix broken first subplot
   title(type {i});
end

set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperPosition', [0 0 33 30]);
saveas(G,supertitle, 'png')
end