function h = leafnodeplot( V, data )
%LEAFNODEPLOT Summary of this function goes here
%   Detailed explanation goes here
result = leafnode(data);
h = figure;
imshow(max(V,[],3),[])
hold on
for i = find(result)
    plot(data(i,3),data(i,4),'o', 'LineWidth',2,...
        'MarkerEdgeColor','k',...
        'MarkerFaceColor','y',...
        'MarkerSize',10);
end
hold off
end

