function compassplot2( features, Y, type, str )
%COMPASSPLOT plots the dendrite density in a compass graph based on the
%means of each neuron type
%Input = features - cell array of maxtrix values from neurons, Y - neuron
%type (e.g. 1-11), type - type of neuron (title of graph), str - title of subplot
%Output = compass graphs for each neuron type
h = figure;
cur = 1;
for r = [6 10 8 3]%length(type)
    f = features(Y==r);
    n = length(f);

    wdir = 0:45:350;%[0 90 180 270];
    c = features(Y==r);
    knots = mean(vertcat(c{:}));
    rdir = wdir * pi/180;
    [x,y] = pol2cart(rdir,knots);
    subplot(2,2,cur);
    h1 = compass(x,y);
    set(h1, 'linewidth', 3);
    title(type(r));
    cur = cur+1;
end
set(h, 'Position', [0, 0, 800, 600]);
set(gcf, 'PaperUnits', 'centimeters');
set(gcf, 'PaperPosition', [0 0 30 20]);
saveas(h, ['c_' str], 'png');
end

