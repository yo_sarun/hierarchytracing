% Test tracing snake at voxel level
% This code is modified from Vector field convolution (VFC) by Bing Li 2005 - 2009.

clear all
close all
clc

% load './paramsOOF2.mat'

% demoVars = matfile('./Yo/demoVarsV7_3.mat');
% FinalOOFVars = matfile('./Yo/FinalOOFVarsV7_3.mat');
load './paramsDRF2.mat'
% matObj = matfile('./Yo/nCNGxWNC_E1H16_12-27-13_(0-1271).mat');
% matObj = matfile('./Yo/n72413_h0.mat');
% matObj = matfile('./Yo/n72513_h0.mat');
% matObj = matfile('./Yo/nCF_1.mat');
matObj = matfile('../tracefiles/preprocessimage/n72413_h0.mat');

addpath('./AMT')
addpath('./frangi/')
addpath('./utils')
addpath('./DRF')
addpath('./snake')
addpath('./block')
addpath('./topology')
addpath('./ba_interp3')
addpath('./oof3response')
addpath(genpath('./toolbox_fast_marching'))
addpath(genpath('./UGM'))
addpath(genpath('./matlab_bgl'))

set(0,'DefaultFigureWindowStyle','docked')

disp('======================================')
disp('Open Snake Curve with DRF and Neuron Model at Pixel level')
%% Get grid image
fcount = 1;
U = matObj.smooth(850:913,920:951,:);
seed = matObj.seeds(850:913,920:951,:);

% U = matObj.smooth(131:231,480:541,:);
% seed = matObj.seeds(131:231,480:541,:);
% fcount = 1;

% U = matObj.smooth(433:490,414:510,:);
% seed = matObj.seeds(433:490,414:510,:);
% fcount = 10;

% U = matObj.smooth(318:355,678:724,:);
% seed = matObj.seeds(318:355,678:724,:);
% fcount = 12;

% U = matObj.smooth(209:256,271:355,:);
% seed = matObj.seeds(209:256,271:355,:);
% fcount = 4;

% U = smooth(925:964,765:804,:);
% seed = seeds(929:960,769:800,:);
% E1 = E(929:960,769:800,:);
% Fext1 = Fext(929:960,769:800,:, :);
% fcount = 2;

% U = smooth(701:740,221:260,:);
% seed = seeds(705:736,225:256,:);
% fcount = 6;

% U = smooth(737:768,225:256,:);
% seed = seeds(737:768,225:256,:);
% E = OOFOut(737:768,225:256,:);
% fcount = 10;

% fcount = 1;
% U = smooth(225:256,225:256,:);
% seed = seeds(225:256,225:256,:);
% fcount = 1;
% U = smooth(161:192,289:321,:);
% seed = seeds(161:192,289:321,:);
% fcount = 22;
% U = matObj.smooth(705:736,225:256,:);
% seed = matObj.seeds(705:736,225:256,:);



%% compute GVF and its energy
disp('--------------------------------------------------')
disp('Computing GVF external force field ...')
mu = .2;
GVF_ITER = 10;
normalize = 1;
h = fspecial('gaussian',[5 5],5);
f = imfilter(double(U),h);
Fexts = AM_GVF(f, mu, GVF_ITER, normalize);    % for toy image
Fext = cell(1,3);
for i = 1:3
    Fext{i} = double(Fexts(:,:,:,i));
end


%% get DRF graphical moodel
disp('--------------------------------------------------')
disp('Generate DRF graphical model')
tic;
edgeStruct = BLK_getEdgeStruct(size(U));
toc;

%% Evolve snake
options.FrangiScaleRange = [1,1];
options.Iter_thr = 7;
options.Fcount = fcount;
tic;
[ opensnk, tube, snkcode, S, C ] = find_snake_topo( U, seed, Fext, edgeStruct, result, options );
toc;
%% Show result
figure(10), SNK_show(opensnk); 