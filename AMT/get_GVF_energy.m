function energy = get_GVF_energy( f, MU, Fext )
% GET_GVF_ENERGY get GVF energy
%     Inputs
%     f           edge map
%     MU       	  a parameter for weighting edge map and mag of vec field
%     Fext        GVF field
%     
%     Outputs
%     energy      the energy map of GVF field
%

f = single(f);
fmin  = min(f(:));
fmax  = max(f(:));
f = (f-fmin)/(fmax-fmin);           % Normalize f to the range [0,1]

if ndims(f)==2,
    [fx,fy] = AM_gradient(f);       % Calculate the gradient of the edge map
    fz = 0;
else
    [fx,fy,fz] = AM_gradient(f);    % Calculate the gradient of the edge map
end
u = Fext(:,:,:,1); v = Fext(:,:,:,2); w = Fext(:,:,:,3);
SqrMagf = fx.*fx + fy.*fy + fz.*fz;

[ux,uy,uz] = AM_gradient(u);
term1 = MU*((ux.^2) + (uy.^2) + (uz.^2));
[vx,vy,vz] = AM_gradient(v);
term1 = term1 + MU*((vx.^2) + (vy.^2) + (vz.^2));

tmp = (u-fx).*(u-fx) + (v-fy).*(v-fy);

if ndims(f)==3
    [wx,wy,wz] = AM_gradient(w);
    term1 = term1 + MU*((wx.^2) + (wy.^2) + (wz.^2));
    tmp = tmp + (w-fz).*(w-fz);
end
term2 = (SqrMagf.*tmp);

energy = (term2 + term1);

end

