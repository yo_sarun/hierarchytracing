function [ opensnk, tube, snkcode, S, C ] = find_snake_topo( U, seed, Fext, edgeStruct, theta, options )
% find_snake_topo find all snakes in image U
% This code is modified from Vector field convolution by 
% Bing Li 2005 - 2009.
%     Inputs
%     U           3D array image stack
%     seed        3D array represents seed points
%     Fext        Normal force (GVF) i.e. 1-by-3 cell array of 3D array
%     edgeStruct  DRF graphical model
%     theta       DRF parameters
%     options     utility options
%               
%     Outputs
%     opensnk     array of struct variable represents snakes
%     tube        probability map of image U i.e. 3D array
%     snkcode     logical value indicates which end of snakes has collided
%     S           number of snake segment
%     C           snake segment i.e. 1-by-n array


% if~ismember(nargin, 5:6)
%     error('Invalid inputs to find_snake_topo!')    
% end

defaultoptions = struct('FrangiScaleRange', [1 8], 'Alpha', 1, 'Beta', 1, 'Tau', .5, ...
        'Fcount', 0, 'Iter_thr', 7, 'Snake_iter', 100, 'LengthThreshold', 0);
% Process inputs
if(~exist('options','var')), 
    options = defaultoptions; 
else
    options = get_options(defaultoptions, options);
end

%% parameter settings
% disp('Open Snake Curve with DRF at Pixel Level')
% disp('Initializing parameters ...')

SAVE_AVI = 0;           % set to 1 to save the process as .avi movie
alpha = options.Alpha;
beta = options.Beta;
tau = options.Tau;
SNAKE_ITER1 = options.Snake_iter;
iter_thr = options.Iter_thr;
RES = 1;
frangirange = options.FrangiScaleRange;
fcount = options.Fcount;
s = size(U);

% turn on warning for debugging.
if fcount == 0
    warning('off','all');
    set(0,'DefaultFigureWindowStyle','normal')
else
    warning('on','all');
    set(0,'DefaultFigureWindowStyle','docked')
end
%% initialise output variables
opensnk = [];
snkcode = [];
S = 0;
C = [];

%% Use theta trained from Diadem
nodes = reshape(U, numel(U), 1);
if any(isnan(nodes))
    error('Invalid Nodes Feature: Eoof contains Nan')
end
n1 = edgeStruct.edgeEnds(:,1);
n2 = edgeStruct.edgeEnds(:,2);
edges = abs(nodes(n1)-nodes(n2));

% DRF Inference using Graph-Cut
[mask,pot] = DRF_infer_GC( theta, double(nodes), double(edges), edgeStruct);
mask = reshape(mask-1, size(U));
tube = reshape(pot(:,2), size(U));

%% Get Seed points
if isempty(find(seed, 1))
%     disp('Invalid Seed Points: No seed points in the block')
    return;
end

[py, px, pz] = ind2sub(size(seed),find(seed & mask));

% remove seed points withing 5 pixels
ind = false(size(px,1),1);
use = false(size(px,1),1);
for i = 1:size(px)
    if ind(i)
        continue;
    end
    tmp = px<=px(i)+5 & px>=px(i)-5 & ...
            py<=py(i)+5 & py>=py(i)-5 & ...
            pz<=pz(i)+5 & pz>=pz(i)-5;
    ind(tmp) = true;
    use(i) = true;
end
% tmpseed = [px(use), py(use), pz(use)];
% for num = 1:20
%     chg = zeros(size(tmpseed));
%     for i=1:3
%         chg(:,i) = interp3(Fext{i},...
%                 tmpseed(:,1),tmpseed(:,2),tmpseed(:,3),'*linear');
%     end
%     sp = tmpseed+chg;
%     sp(any(isnan(sp),2),:) = tmpseed(any(isnan(sp),2),:);
%     
%     ind = false(size(sp,1),1);
%     use = false(size(sp,1),1);
%     for i = 1:size(sp,1)
%         if ind(i)
%             continue;
%         end
%         tmp = sp(:,1)<=sp(i,1)+5 & sp(:,1)>=sp(i,1)-5 & ...
%                 sp(:,2)<=sp(i,2)+5 & sp(:,2)>=sp(i,2)-5 & ...
%                 sp(:,3)<=sp(i,3)+5 & sp(:,3)>=sp(i,3)-5;
%         ind(tmp) = true;
%         use(i) = true;
%     end
%     tmpseed = sp(use,:);
% end

seedp = round([px(use), py(use), pz(use)]);
seedp(seedp<1) = 1;
seedp(seedp(:,1)>s(2),1) = s(2); 
seedp(seedp(:,2)>s(1),2) = s(1); 
seedp(seedp(:,3)>s(3),3) = s(3);
% sdp = sub2ind(size(mask), seedp(:,2), seedp(:,1), seedp(:,3));
% seedp = seedp(mask(sdp)==1,:);
if isempty(seedp)
%     disp('Invalid Seed Points: No seed points on Neuron')
    return;
end

%% Compute Frangi to find ev
ms = frangirange(2);

opt.FrangiScaleRange = frangirange;
opt.FrangiScaleRatio = 2;
opt.FrangiAlpha = .5;
opt.FrangiBeta = .5;
opt.FrangiC = 500;
opt.BlackWhite = false;
opt.verbose = false;

% pad image
newtube = zeros(size(tube)+(ms+3)*2);
% h = fspecial('gaussian',[5 5],5);
% f = imfilter(double(tube),h);
newtube(ms+4:ms+3+s(1), ms+4:ms+3+s(2), ms+4:ms+3+s(3)) = tube;
[~,~,Vx,Vy,Vz] = FrangiFilter3D(newtube,opt);
Vx = Vx(ms+4:ms+3+s(1), ms+4:ms+3+s(2), ms+4:ms+3+s(3));
Vy = Vy(ms+4:ms+3+s(1), ms+4:ms+3+s(2), ms+4:ms+3+s(3));
Vz = Vz(ms+4:ms+3+s(1), ms+4:ms+3+s(2), ms+4:ms+3+s(3));
ev = cell(1,3);
ev{1} = double(Vy);
ev{2} = double(Vx);
ev{3} = double(Vz);

%% initialize snakes
U = U./255;
[fx, fy, fz] = gradient(U);
df = sqrt(fx.^2 + fy.^2 + fz.^2);
% disp('Initializing the snake ...')
nsnk = size(seedp,1);
% create an array of snake struct
opensnk = struct(...
        'vert',[],...               % points on snake
        'E', 0,...                  % snake energy
        'label', num2cell(1:nsnk),...   % snake label
        'snkh', [],...              % snake connecting at head
        'snkt', [],...              % snake connecting at tail
        'ncon', 0,...               % # iterations that snake converges
        'length', 0,...             % length of snake
        'collideh', 0,...           % # collisions at head end
        'collidet', 0,...           % # collisions at tail end
        'converge', false,...       % boolean showing convergence of snake
        'grids', []);                % grid number for Neuron Model

for i = 1:nsnk
    ind = sub2ind(size(tube),seedp(i,2), seedp(i,1), seedp(i,3));
    % extend seed point into 3-point snake
    opensnk(i).vert = [seedp(i,:) + [ev{1}(ind), ev{2}(ind), ev{3}(ind)];...
            seedp(i,:);...
            seedp(i,:) - [ev{1}(ind), ev{2}(ind), ev{3}(ind)]];
    opensnk(i) = SNK_get_energy(opensnk(i),alpha,beta,U,df,[]);
end
minE = [opensnk.E];

%% get initial snake model
snkcode = ones(nsnk,2);
if fcount > 0
    predict_opts.Debug = true;
    predict_opts.Figure = 1;
else
    predict_opts.Debug = false;
end
[opensnk, snkcode, model] = TP_predict_opt( opensnk, s, snkcode, predict_opts );

%% Display GVF & initial snake
if fcount > 0
    figure(4)
    imshow3D(double(mask))
    
    I = U(:,:,fcount);
    figure(2);
    subplot(1,3,1);
    imshow(mean(U,3),[]);
    title('preprocessed image');
    [py, px] = find(any(seed,3));
    hold on;
    plot(px,py,'b+')
    hold off;
    
    disp('Displaying the external force field ...')
    subplot(1,3,2);
    AC_quiver( Fext{1}(:,:,fcount), Fext{2}(:,:,fcount), I );
    title('normalized GVF field');

    colmap = jet(nsnk);
    subplot(1,3,3)
    AC_quiver( ev{1}(:,:,fcount), ev{2}(:,:,fcount), I, 'r' );
    hold on
    for i = 1:nsnk
        AC_display(opensnk(i).vert,'open','-o',colmap(i,:));
    end
    hold off
    title('The first principal EV')
    drawnow;
    
    Ehist = zeros(SNAKE_ITER1, nsnk);
end

%% Evolve snakes
if SAVE_AVI
	mov = avifile(['example_vfc_',num2str(cs),'.avi'],...
            'fps',4,'quality',100,'compression','None');
	frame = getframe(gca);
	mov = addframe(mov,frame);
end

% disp('Deforming the snake ...')

for i = 1:SNAKE_ITER1
    for j = 1:nsnk
        if opensnk(j).converge
            continue;
        end
        opensnk(j) = AC_deform(opensnk(j),...
                alpha,beta,tau,Fext,ev,mask,...
                tube,snkcode(j,:),model(j),'open');

        [opensnk(j), needremesh] = SNK_remesh(opensnk(j), RES, s, mask);
        if needremesh || mod(i,5)==0
            opensnk(j) = AC_remesh(opensnk(j), RES, 'equal', 'open');
        end
        
        % update snake_j's energy
        opensnk(j) = SNK_get_energy(opensnk(j),alpha,beta,U,df,model(j));
        
        if SNK_is_converge(opensnk(j), minE(j))
            opensnk(j).ncon = opensnk(j).ncon + 1;
        else
            opensnk(j).ncon = 0;
        end
        minE(j) = min(minE(j), opensnk(j).E);

%         if fcount > 0 && mod(i,2)==0,
%             figure(3), subplot(1,2,1);
%             imagesc(U(:,:,fcount)), colormap gray;
%             hold on
%             for k = 1:nsnk
%                 h=AC_display(opensnk(k).vert,'open','-o', colmap(k,:));
%             end
%             hold off
%             title(['Iteration ' num2str(i)])
%             drawnow;
%         end
%         if SAVE_AVI
%             frame = getframe(gca);
%             mov = addframe(mov,frame);
%         end
    end
    % update snake model
    if mod(i,8)==0
        [opensnk, snkcode, model] = TP_predict_opt( opensnk, s, snkcode );
    elseif mod(i,2)==0
        [opensnk, snkcode] = TP_predict_opt( opensnk, s, snkcode );
    end
    if fcount > 0
        figure(3), subplot(1,2,2);
        for j = 1:nsnk
            Ehist(i,j) = opensnk(j).E;
            plot(1:i,Ehist(1:i,j),'Color',colmap(j,:));
            if j == 1, hold on; end
            if j == nsnk, hold off; end
        end
        hehe = (regexp(num2str(1:nsnk),'(\d+)','tokens'));
        legend([hehe{:}]);
        Ehist(i,:)
    end
    
    for j = 1:nsnk
        if opensnk(j).converge
            continue;
        end
        if opensnk(j).ncon > iter_thr || all(snkcode(j,:) == 0)
%             disp(['Snake ', num2str(j), ' Converge at iter ' num2str(i)])
            opensnk(j).converge = true;
        end
    end
    if all([opensnk.converge])
        break;
    end
end
% disp('Done!')
if SAVE_AVI
    h=close(mov);
end

%%
if fcount > 0, figure(5), subplot(1,2,1), SNK_show(opensnk); end;
opts_filter.LengthThreshold = options.LengthThreshold; %s(1)/10;
[opensnk, S, C] = BLK_filter_snk(opensnk, size(U), opts_filter);
if fcount > 0, figure(5), subplot(1,2,2), SNK_show(opensnk); end;

end


