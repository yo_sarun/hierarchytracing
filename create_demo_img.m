addpath('utils')
savedir = 'toyimage/';

%%
demo_img = imread('./toyimage/demo2.png', 'png');
X = rgb2gray(demo_img);
X = downsample(X',2);
X = downsample(X',2);
V = zeros(size(X,1), size(X,2), 20);

V(:,:,10) = X;
se = strel('disk',2);
for i = 9:-1:1
    V(:,:,i) = imerode(V(:,:,i+1),se);
end

for i = 11:20
    V(:,:,i) = imerode(V(:,:,i-1),se);
end

figure, imshow3D(V);

%% save as image slide
if mkdir([savedir 'demo']);
    for i = 1:size(V,3)
        imwrite(V(:,:,i), [savedir 'demo/', sprintf('%02d',i), '.tif']);
    end
    disp('SAVE')
else
    error('ERROR: cannot save image slice');
end