% Check energy involved in snake evolution
clear all
close all
clc

load './paramsDRF2.mat' 'result2'
% load './Yo/V.mat'

% filename = 'n01';
% filename = 'nCF_1';
filename = 'nOP_5';
% filename = 'n72413_h0';
% filename = 'n72513_h0';
% filename = 'n73113_1_L1E2_0';
% filename = 'nCNGv12xWNC_E3H16_2-8-14';
% filename = 'nCNGxPNC_E1H16_2-5-14_(0-430)';
% filename = 'nCNGxPNC_E1H15_1-30-14 (0-948)';
% filename = 'nCNGxWNC_E1H16_12-27-13_(0-1271)';
% filename = 'n3-1-14_CNGxWNC_H16 Timelapse';
loadname = ['./Yo/' filename '.mat'];
load(loadname)%, 'V', 'blksize', 'cellsize', 'seeds')

addpath('./AMT')
addpath('./frangi')
addpath('./utils')
addpath('./DRF')
addpath('./snake')
addpath('./block')
addpath('./topology')
addpath('./ba_interp3')
addpath('./oof3response')
addpath(genpath('./toolbox_fast_marching'))
addpath(genpath('./UGM'))

disp('======================================')
disp('Open Snake Curve with DRF at Pixel level using OOF feature')
%% initialise
tubes = cell(cellsize);
Eoofs = cell(cellsize);
theta = result2;
fcount = 0;

%% Open parallel
if ~matlabpool('size')
    try
        matlabpool open 12
    catch
    end
end
%% Deform snakes in each block
looptime = tic;
parfor a = 1:prod(cellsize)
    [i,j] = ind2sub(cellsize,a);
    disp([i,j])
    row = ((i-1)*32)+1;
    col = ((j-1)*32)+1;
    rowt = min(row+blksize(1)-1, size(V,1));
    colt = min(col+blksize(1)-1, size(V,2));
%     matObj = matfile(loadname);
%     U = matObj.smooth(row:rowt,col:colt,:);
    U = smooth(row:rowt,col:colt,:);
    edgeStruct = BLK_getEdgeStruct(size(U));
    n1 = edgeStruct.edgeEnds(:,1);
    n2 = edgeStruct.edgeEnds(:,2);
    % pad image and find GVF energy/force field in the block
    disp('--------------------------------------------------')
    disp('Computing the GVF external force field ...')
    mu = .2;
    GVF_ITER = 10;
    Fexts = AM_GVF(U, mu, GVF_ITER, 1);
    Fext = cell(1,3);
    for n = 1:3
        Fext{n} = double(Fexts(:,:,:,n));
    end
    %% Display GVF & initial snake
    if fcount > 0
        disp('Displaying the external force field ...')
        I = U(:,:,fcount);
        figure(12);
        subplot(1,3,1)
        AC_quiver( Fext{1}(:,:,fcount), Fext{2}(:,:,fcount), I );
        title('normalized GVF field');
%         pause(1)
    end

    %% Use theta trained from Diadem
%     U = double(U)./255;
%     nodes = reshape(Eoofs{a}, numel(Eoofs{a}), 1);
    nodes = reshape(U, numel(U), 1);
%     n1 = edgeStruct.edgeEnds(:,1);
%     n2 = edgeStruct.edgeEnds(:,2);
    edges = abs(nodes(n1)-nodes(n2));

    if any(isnan(nodes))
        warning('Invalid Nodes Feature: Eoof contains Nan')
        tubes{a} = zeros(size(Eoof));
    else
        %% Inference using Graph-Cut
        disp('--------------------------------------------------')
        disp('Compute Probability Map of DRF using Graph-cut')
        [~,pot] = DRF_infer_GC( theta, double(nodes), double(edges), edgeStruct);
        tubes{a} = reshape(pot(:,2), size(U));
        if fcount > 0
            subplot(1,3,3);
            imshow(tubes{a}(:,:,fcount),[]);
            title('tube');
        end
    end
    
end
%%
tube = zeros(size(V));
% Eoof = zeros(size(V));
for a = 1:prod(cellsize)
    [i,j] = ind2sub(cellsize,a);
    disp([i,j])
    row = ((i-1)*32)+1;
    col = ((j-1)*32)+1;
    rowt = min(row+blksize(1)-1, size(V,1));
    colt = min(col+blksize(1)-1, size(V,2));
    tube(row:rowt,col:colt,:) = tubes{a};
%     Eoof(row:row+31,col:col+31,:) = Eoofs{a};
end
figure
imshow3D(tube);
mask = tube>.5;
figure
imshow3D(mask);
% figure
% imshow3D(Eoof);

figure(7)
for fcount = 1:size(V,3)
    figure(7)
    fcount
    imshow(tube(:,:,fcount),[])
    [py, px] = find(seeds(:,:,fcount));
    hold on;
    plot(px,py,'b+')
    hold off;
    pause(1)
end

disp('Done!!');
looptime = toc(looptime);

%% close parallel
if matlabpool('size')
    matlabpool close
end