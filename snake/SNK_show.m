function str = SNK_show( snks, blksize, cmap, isgroup, V, titl, axes_obj )
%SNK_SHOW show snakes
% 
%     Inputs
%     snks        2D cell array of snake struct
%     blksize     size of the block i.e. [nRows nCols, nDeps]
%     cmap        type of colormap
%     isgroup     boolean whether to group snakes into segment
%     V           3D image stack
%     titl        title of the plot
%
str = '';
if nargin < 2
    blksize = [32,32,23];
end
if nargin < 3
    cmap = 'jet';
end
if nargin < 4
    isgroup = 1;
end
if nargin < 5
    V = [];
end
if nargin < 6
    titl = '';
end

if isgroup
    if iscell(snks)
        group = cell(size(snks));
        ngroup = zeros(size(snks));
        for i = 1:size(snks,1)
            for j = 1:size(snks,2)
                if isempty(snks{i,j})
                    continue;
                end
                [S,C] = find_segment(snks{i,j});
                ngroup(i,j) = S;
                group{i,j} = C;
            end
        end
    else
        [S,C] = find_segment(snks);
    end
end

axes(axes_obj), cla;
if iscell(snks) && isempty(V)
    % show 3D image of all snakes in the cells
    plot3([],[])
    hold on;
    for i = 1:size(snks,1)
        for j = 1:size(snks,2)
            row = ((i-1)*blksize(1));
            col = ((j-1)*blksize(2));
            if strcmp(cmap,'copper')
                colmap = copper(ngroup(i,j));
            elseif strcmp(cmap,'jet')
                colmap = jet(ngroup(i,j));
            end
            for k = 1:size(snks{i,j},2)
                draw_snake( snks{i,j}, [col, row, 0], colmap(group{i,j}(k),:), k);
            end
        end
    end
    hold off;
    grid
    
elseif iscell(snks) && ~isempty(V)
    % show overlaid snake on an image
    imshow(max(V,[],3),[]);
    colmap = jet(max(ngroup(:)));
    hold on;
    for i = 1:size(snks,1)
        for j = 1:size(snks,2)
            row = ((i-1)*blksize(1));
            col = ((j-1)*blksize(2));        
            for k = 1:size(snks{i,j},2)
                draw_snake_over( snks{i,j}, [col, row, 0], colmap(group{i,j}(k),:), k)
            end
        end
    end
    hold off;
else
    % show all snakes in an array of snake struct
    if strcmp(cmap,'copper')
        colmap = copper(S);
    elseif strcmp(cmap,'jet')
        colmap = lines(S);
    end
    hold on;
    for i = 1:size(snks,2)
        draw_snake( snks, zeros(1,3), colmap(C(i),:), i);
    end
    hold off;
end
title(titl);
end


function [S,C] = find_segment(snks)
	nsnks = size(snks,2);
    indH = arrayfun(@(x)~isempty(x.snkh),snks);
    indT = arrayfun(@(x)~isempty(x.snkt),snks);
    edge = zeros(length([snks(indH).snkh]) + length([snks(indT).snkt]), 2);
    cur = 1;
    for n = 1:nsnks
        for m = 1:length(snks(n).snkh)
            edge(cur,:) = [n , snks(n).snkh(m)];
            cur = cur+1;
        end
        for m = 1:length(snks(n).snkt)
            edge(cur,:) = [n , snks(n).snkt(m)];
            cur = cur+1;
        end
    end
    adj = sparse( [edge(:,1); edge(:,2)], ...
            [edge(:,2); edge(:,1)], ...
            ones(size(edge,1)*2,1), nsnks, nsnks );
    [S, C] = connectcomp(adj);
end


function draw_snake( snks, offset, col, k )
    if isempty(snks(k).vert)
        return
    end
    points = snks(k).vert;
	points = points + repmat(offset,size(points,1),1);
    plot3(points(:,1), points(:,2), points(:,3),...
            '-o', 'Color', col);
end


function draw_snake_over( snks, offset, col, k )
    points = bsxfun(@plus, snks(k).vert, offset);
    AC_display(points,'open','-',col);
end
