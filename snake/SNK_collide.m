function [ is_collide, label ] = SNK_collide( snk1, snk2, is_head_end, thr )
% SNK_COLLIDE check whether snk1 collide to snk2
%
%     Inputs
%     snk1          snake struct
%     snk2          another snake struct to check collision with
%     is_head_end   check collision at which end of snk1
%     thr           distance threshold for collision
%     
%     Outputs
%     is_collide    boolean whether snk1 and snk2 collide
%     label         label of snake that snk1 collides with (make sense when
%                   snk2 is segment)
%

% is_collide = false;
% label = 0;
% pos = 0;
% if isempty(snk1) || isempty(snk2) || isempty(snk1.vert) || isempty(vertcat(snk2.vert))
%     return
% end

if nargin < 4
    thr = 2;
end

%%%%%%%%%
% keyboard;
[is_collide, label] = SNK_collide_c(snk1, snk2, is_head_end, thr);
%%%%%%%%%
% N = size(snk1.vert,1);
% 
% if is_head_end == 1
%     ds = bsxfun(@minus, vertcat(snk2.vert), snk1.vert(1,:));
%     if any(sqrt(sum(ds.^2,2)) < thr)
%         is_collide = true;
%     end
% elseif is_head_end == 2
%     ds = bsxfun(@minus, vertcat(snk2.vert), snk1.vert(N,:));
%     if any(sqrt(sum(ds.^2,2)) < thr)
%         is_collide = true;
%     end
% else
%     error('ERROR in SNK_collide: Invalid value of is_head_end')
% end
% 
% % find label of snake that is collided with
% if nargout == 2
%     snk2len = arrayfun(@(x)length(x.vert),snk2);
%     p = find(sqrt(sum(ds.^2,2)) < thr,1,'first');
%     if ~isempty(p)
%         label = snk2(find(p<=cumsum(snk2len),1,'first')).label;
%     end
%     
% %     if is_collide2 ~= is_collide
% %         keyboard;
% %         error('mex SNK_collide_c is wrong');
% %     end
% end
% 
% % if nargout == 3
% % 	pos = find(sqrt(sum(ds.^2,2)) < thr,1,'first');
% % end
% 
% % if is_collide2 ~= is_collide
% %     keyboard;
% %     error('mex SNK_collide_c is wrong');
% % end

end

