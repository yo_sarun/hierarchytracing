function [ snks ] = SNK_break( snk, overlap )
%SNK_BREAK find snake fragments that do not overlap
% 
%     Inputs
%     snk             a snake struct
%     overlap         logical array indicate whether snake point overlap
%     
%     Outputs
%     snks            1D array of snake fragments
%
interval = find([overlap(1); xor(overlap(1:end-1), overlap(2:end)); overlap(end)]);
interval(2:2:end) = interval(2:2:end)-1;
cur = 1;
for i = 1:2:length(interval)
    % get fragment of snake
    if interval(i) == interval(i+1)
        if interval(i) == size(snk.vert,1)
            interval(i) = interval(i) - 1;
        else
            interval(i+1) = interval(i+1) + 1;
        end
    end
    snks(cur) = snk;
    snks(cur).snkh = [];
    snks(cur).snkt = [];
    snks(cur).label = snks(cur).label + cur - 1;
    snks(cur).vert = snks(cur).vert(interval(i):interval(i+1),:);
    % remesh snake points
    len = find_curvature(snks(cur).vert, 0);
    snks(cur).length = sum(len);
    M = max(round(snks(cur).length),3);
    dif = diff(snks(cur).vert);
    snks(cur).vert(sum(dif,2)==0,:) = [];  % Ensure  strict monotonic order.
    dif(sum(dif,2)==0,:) = [];  % consistent to cursnk.vert
    d = sqrt(sum(dif.^2,2));  % distance between adjacent points
    D = sum(d);
    di = linspace(0,D,M);
    dcum = [0;cumsum(d)];
    vert = zeros(M,3);
    for k=1:3,
        vert(:,k) = interp1(dcum,snks(cur).vert(:,k),di)';
    end
    snks(cur).vert = vert;
    cur = cur + 1;
end
if ~exist('snks','var')
    snks = [];
end
end

