#include <math.h>
#include "mex.h" /* Always include this */
void mexFunction(int nlhs, mxArray *plhs[], /* Output variables */
        int nrhs, const mxArray *prhs[]) /* Input variables */
{
    mxArray *snk1vert, *snk2vert;
    mxLogical *is_collide;
    double *p1, *p2, *label;
    double ds, thr;
    int i, j, k;
    int M1, N1, M2, N2, is_head_end, m1, m2, cur1;
    
    M1 = mxGetM(prhs[0]); /* Get the dimensions of A */
    N1 = mxGetN(prhs[0]);
    M2 = mxGetM(prhs[1]); /* Get the dimensions of A */
    N2 = mxGetN(prhs[1]);
    is_head_end = *mxGetPr(prhs[2]);
    thr = *mxGetPr(prhs[3]);
    
    plhs[0] = mxCreateLogicalScalar(0);
    is_collide = mxGetLogicals(plhs[0]);
    plhs[1] = mxCreateDoubleScalar(0);
    label = (double*)mxGetData(plhs[0]);
    
//     if (M1 != 1 || N1 != 1)
//         mexErrMsgTxt("Invalid snk1 input arguments!");
//     if (M2 != 1 || N2 < 1)
//         mexErrMsgTxt("Invalid snk2 input arguments!");
    
    snk1vert = mxGetField(prhs[0], 0, "vert");
    m1 = mxGetM(snk1vert); /* Get the dimensions of A */
    p1 = mxGetPr(snk1vert);
//     mexPrintf("Hello, world %f, %d, %d ,%d ,%d, %d, %d\n", p1[0], M1, N1, M2, N2, is_head_end, m1);
    
    if (is_head_end == 1){
        cur1 = 0;
    } else if (is_head_end == 2) {
        cur1 = m1 - 1;
    } else {
        mexErrMsgTxt("Invalid input arguments!");
    }
    
    for (i=0; i<N2; i++) {
        snk2vert = mxGetField(prhs[1], i, "vert");
        m2 = mxGetM(snk2vert); /* Get the dimensions of A */
//         n2 = mxGetN(snk2vert);
        p2 = mxGetPr(snk2vert);
        for (j=0; j<m2; j+=2) {
            ds = 0;
            for (k=0; k<3; k++) {
                ds += (p1[cur1 + m1*k] - p2[j + m2*k]) * (p1[cur1 + m1*k] - p2[j + m2*k]);
//                 mexPrintf("a (%d, %d) at %d: %f ,%f - %d %d\n", cur1, j, k, p1[cur1 + m1*k], p2[j + m2*k], m2, n2);
            }
            if (sqrt(ds) < thr) {
                *is_collide = 1;
                plhs[1] = mxGetField(prhs[1], i, "label");
//                 mexPrintf("exit");
                return;
            }
        }
        if (m2%2 != 1) {
            ds = 0;
            for (k=0; k<3; k++) {
                ds += (p1[cur1 + m1*k] - p2[m2-1 + m2*k]) * (p1[cur1 + m1*k] - p2[m2-1 + m2*k]);
            }
            if (sqrt(ds) < thr) {
                *is_collide = 1;
                plhs[1] = mxGetField(prhs[1], i, "label");
                return;
            }
        }
    }
    
//         N = size(snk1.vert,1);
// 
//     if is_head_end == 1
//         ds = bsxfun(@minus, vertcat(snk2.vert), snk1.vert(1,:));
//         if any(sqrt(sum(ds.^2,2)) < thr)
//             is_collide = true;
//         end
//     elseif is_head_end == 2
//         ds = bsxfun(@minus, vertcat(snk2.vert), snk1.vert(N,:));
//         if any(sqrt(sum(ds.^2,2)) < thr)
//             is_collide = true;
//         end
//     else
//         error('ERROR in SNK_collide: Invalid value of is_head_end')
//     end
    
    
    return;
}