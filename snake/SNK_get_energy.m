function [ snk ] = SNK_get_energy( snk, alpha, beta, Egvf, Prob, model, display)
% SNK_GET_ENERGY get energy of the snake
%     Inputs
%     snk         struct snake variable
%     alpha       alpha parameter for elasticity
%     beta        beta parameter for stiffness
%     Egvf        normal energy (U) i.e. 3D array
%     Prob        tangent energy (dU) i.e. 3D array
%     model       attraction model i.e. 1-by-n cell array
%     display     logical variable showing result for debugging
%     
%     Outputs
%     snk         struct snake variable with updated energy
%

if nargin < 7
    display = 0;
end
if isempty(snk.vert)
    snk.length = 0;
    snk.E = 0;
    return
end
s = size(Egvf);
snk.vert(snk.vert<1) = 1;
snk.vert(snk.vert(:,1)>s(2),1) = s(2); 
snk.vert(snk.vert(:,2)>s(1),2) = s(1); 
snk.vert(snk.vert(:,3)>s(3),3) = s(3);
[len, curve] = find_curvature(snk.vert, 0);

p = round(snk.vert);
ind = sub2ind(size(Egvf), p(:,2), p(:,1), p(:,3));
Enormal = sum(Egvf(ind));
Etangent = sum(Prob(ind));
if ~isempty(model)
    Emodel = max(~isempty(snk.snkh),model.confidenceH) + ...
            max(~isempty(snk.snkt),model.confidenceT);
else
    Emodel = 0;
end

snk.length = sum(len);
snk.E = alpha*snk.length + beta*curve - 8*Enormal + Etangent - 0.1*Emodel;

if display
    fprintf('Snake %.2d: len = %.2f, curve = %.2f, Prob = %.2f, GVF = %.2f => E = %.2f\n',...
            snk.label, sum(len), curve, sum(Etangent), sum(Enormal), snk.E);
end

end

