function [ snk, exitflag ] = SNK_remesh( snk, RES, s, mask )
%SNK_REMESH adding point the snake's end if meet criteria
% 
%     Inputs
%     snk         snake struct
%     RES         threshold distance between each snake point
%     s           image size i.e. [nRows nCols nDeps]
%     mask        3D binary image of Neuron
%     
%     Outputs
%     snk         remeshed snake struct
%     exitflag    boolean indicate whether snake lies within the block
%

% keep all snake points inside the image
hoho = snk.vert;
snk.vert(snk.vert<1) = 1;
snk.vert(snk.vert(:,1)>s(2),1) = s(2); 
snk.vert(snk.vert(:,2)>s(1),2) = s(1); 
snk.vert(snk.vert(:,3)>s(3),3) = s(3);

if any(any(hoho ~= snk.vert))
    exitflag = 1;
else
    exitflag = 0;
end

% snake must have at least 3 points
if size(snk.vert,1) < 3
    return
end

% half of number of points must be on Neuron
p = round(snk.vert);
if sum(mask(sub2ind(s, p(:,2), p(:,1),p(:,3)))) <= (size(snk.vert,1) / 2)
    return
end

%% Extend head-end of snake
ds1 = snk.vert(1,:) - snk.vert(2,:);
magEexp = sqrt(sum(ds1.^2,2));
ds1 = ds1./magEexp;
% expand only when end point exceed certain length and it is on Neuron
if magEexp >= RES+0.5 && (snk.length / size(snk.vert,1)) > 0.75
    snk.vert = [snk.vert(1,:); snk.vert];
    snk.vert(2,:) = snk.vert(3,:) + ds1*RES;
end

%% Extend tail-end of snake
dsN = snk.vert(end,:) - snk.vert(end-1,:);
magEexp = sqrt(sum(dsN.^2,2));
dsN = dsN./magEexp;
% expand only when end point exceed certain length and it is on Neuron
if magEexp >= RES+0.5 && (snk.length / size(snk.vert,1)) > 0.75
    snk.vert = [snk.vert; snk.vert(end,:)];
    snk.vert(end-1,:) = snk.vert(end-2,:) + dsN*RES;
end

end

