function [ snklabel ] = SNK_label( vert, snklabel )
% SNK_LABEL label snake points 'vert' in label map 'snklabel' by true value
%     Inputs
%     vert        a list of points on the snake
%     snklabel    a label map of snakes in a block
%     
%     Outputs
%     snklabel    a new label map with vert labelled by val
%
for i = 1:size(vert,1)
	% generate all neighbors of a vertex
    [xx, yy, zz] = meshgrid(vert(i,1)+(-1:1), vert(i,2)+(-1:1), vert(i,3)+(-1:1));
    neighbors = round([reshape(xx,[27 1]), reshape(yy,[27 1]), reshape(zz,[27 1])]);
	% exclude pixels lying outside image
	IdxIn = neighbors(:,1)>=1 & neighbors(:,1)<=size(snklabel,2) ...
            & neighbors(:,2)>=1 & neighbors(:,2)<=size(snklabel,1) ...
            & neighbors(:,3)>=1 & neighbors(:,3)<=size(snklabel,3);
	idx = sub2ind(size(snklabel),...
            neighbors(IdxIn,2), neighbors(IdxIn,1), neighbors(IdxIn,3));
	snklabel(idx) = true;
end
end

