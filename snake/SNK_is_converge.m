function [ result ] = SNK_is_converge( snk, minE)
% SNK_IS_CONVERGE check convergence of snk
%
%     Inputs
%     snk         snake struct
%     minE        the minimum energy of all snake so far
%     
%     Outputs
%     result      true if snake converges, otherwise false
%

result = false;
if snk.E >= (minE - 0.01)
    result = true; 
end

end

