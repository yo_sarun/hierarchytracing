function [ snk, flag ] = SNK_overlap( snk, dessnk, is_head_end, options )
%SNK_OVERLAP check for snake-overlap and self-intersect (self-overlap)
%
%     Inputs
%     snk           snake struct
%     dessnk        another snake struct that is checked the overlap with
% 	  is_head_end   boolean s.t. 1 for head-end, otherwise tail-end
%     options       utility options
%     
%     Outputs
%     snk           snake struct after remove overlapped points
%     flag          integer indicate type of overlap
%                       0 - no overlap
%                       1 - partial overlap
%                       2 - fully overlap
%
if ~ismember(nargin, 3:4)
    error('Invalid inputs to SNK_overlap!')
end

defaultoptions = struct('Threshold', 1, 'RemoveShortSnake', 1);
% Process inputs
if(~exist('options','var')), 
    options = defaultoptions; 
else
    options = get_options(defaultoptions, options);
end

thr = options.Threshold;
rm_short_snk = options.RemoveShortSnake;
flag = 0;

%% check and remove overlap part of snake snk 
nsnk = size(snk.vert,1);
if nsnk == 0 || size(dessnk.vert,1) == 0
    return
end

if is_head_end == 1
    loop = 2:nsnk;
else
    loop = nsnk-1:-1:1;
end
% check for snake-overlap
for i = loop
    try
    ds = bsxfun(@minus, dessnk.vert, snk.vert(i,:));
    catch
        keyboard;
    end
    if all(sqrt(sum(ds.^2,2)) >= thr)
        break;
    end
end
% remove overlapped points
if is_head_end == 1
    snk.vert = snk.vert(i-1:end,:);
else
    snk.vert = snk.vert(1:i+1,:);
end
% check type of overlap
if nsnk ~= size(snk.vert,1)
    flag = 1;
end
if size(snk.vert,1) < 3
    if rm_short_snk
        snk.vert = [];
    else
%         keyboard;
        dif = diff(snk.vert);
        d = sqrt(sum(dif.^2,2));  % distance between adjacent points
        di = linspace(0,d,3);
        dcum = [0;cumsum(d)];
        vert = zeros(3,3);
        for i=1:3,
            vert(:,i) = interp1(dcum,snk.vert(:,i),di)';
        end
        % update vert
        snk.vert = vert;
    end
    snk.converge = 1;
    flag = 2;
end
end

