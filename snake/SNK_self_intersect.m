function [ snk ] = SNK_self_intersect( snk, res )
%SNK_SELF_INTERSECT check whether snake self-intersect
%
%     Inputs
%     snk     snake struct
%     res     threshold distance between adjacent snake points
%     
%     Outputs
%     snk     snake after remove self-intersect points
%

if isempty(snk) || isempty(snk.vert)
    return
end

if nargin < 2
    res = 1;
end

%% check for self-intersect from head
ds = bsxfun(@minus, snk.vert, snk.vert(1,:));
cond = sqrt(sum(ds.^2,2)) <= res;
if ~issorted(flipud(cond))
    % remove self-overlapped part
%     warning('Snake %d self-intersect', snk.label);
    n = find(cond==0, 1, 'first');
    snk.vert = snk.vert(n:end,:);
    snk.converge = 1;
end

%% check for self-intersect from tail
ds = bsxfun(@minus, snk.vert, snk.vert(end,:));
cond = sqrt(sum(ds.^2,2)) <= res;
if ~issorted(cond)
    % remove self-overlapped part
%     warning('Snake %d self-intersect', snk.label);
    n = find(cond==0, 1, 'last');
    snk.vert = snk.vert(1:n,:);
    snk.converge = 1;
end

%% check size of snake
if size(snk.vert,1) < 3
    snk.vert = [];
end

end

