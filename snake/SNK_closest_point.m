function [ point ] = SNK_closest_point( endpoint, snk )
%SNK_CLOSEST_POINT find a snake point on snk that is closest to endpoint
%     Inputs
%     endpoint    an end point
%     snk         a snake struct
%     
%     Outputs
%     point       a point on snk that is the closest to endpoint
%

point = [];
if isempty(snk)
    return
end
cur = 1;
for i = 1:size(snk,2)
    if isempty(snk(i).vert)
        continue;
    end
    p = snk(i).vert;
    d = bsxfun(@minus, p, endpoint);
%     d = p - repmat(endpoint, size(p,1), 1);
    [~,pos] = min(sqrt(sum(d.^2,2)));
    point(cur,:) = p(pos,:);
    cur = cur+1;
end

end

